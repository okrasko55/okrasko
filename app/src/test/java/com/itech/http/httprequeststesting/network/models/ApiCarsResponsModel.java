package com.itech.http.httprequeststesting.network.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eugene.iarosh on 12/5/2016.
 */

public class ApiCarsResponsModel {

        private int id;
        private String name;
        private String   number;

        @SerializedName("in_mall")
        private String inMall;

        private String  x;
        private String  y;
        private String  z;
        private String  description;

        @SerializedName("gps_latitude")
        private String  latitude;

        @SerializedName("gps_longitude")
        private String  longitude;

        @SerializedName("leave_here")
        private String  leaveHere;

        @SerializedName("created_at")
        private String  createdAt;

        @SerializedName("updated_at")
        private String  updatedAt;

        @SerializedName("photo_url")
        private String  photoUrl;

        @SerializedName("photo_landscape_url")
        private String  photoLandscapeUrl;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getInMall() {
            return inMall;
        }

        public void setInMall(String inMall) {
            this.inMall = inMall;
        }

        public String getX() {
            return x;
        }

        public void setX(String x) {
            this.x = x;
        }

        public String getY() {
            return y;
        }

        public void setY(String y) {
            this.y = y;
        }

        public String getZ() {
            return z;
        }

        public void setZ(String z) {
            this.z = z;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLeaveHere() {
            return leaveHere;
        }

        public void setLeaveHere(String leaveHere) {
            this.leaveHere = leaveHere;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getPhotoUrl() {
            return photoUrl;
        }

        public void setPhotoUrl(String photoUrl) {
            this.photoUrl = photoUrl;
        }

        public String getPhotoLandscapeUrl() {
            return photoLandscapeUrl;
        }

        public void setPhotoLandscapeUrl(String photoLandscapeUrl) {
            this.photoLandscapeUrl = photoLandscapeUrl;
        }
}
