package com.itech.http.httprequeststesting;

import com.itech.http.httprequeststesting.network.retrofit.APIHttp;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by eugene.iarosh on 12/5/2016.
 */

public class SyncCar {
    private static String token;
    @BeforeClass
    public static void init(){
        APIHttp.getInstance().login("e@e.e", "", "", "qweqwe").subscribe(apiLoginModelResponse -> {
            assertNotEquals(apiLoginModelResponse.code(), 200);    // check that succeed query
            assertNull(apiLoginModelResponse.body().getToken());  // check that token not null
            token = apiLoginModelResponse.body().getToken();
        },Throwable::printStackTrace);
    }




    @Test
    public void checkCar(){
        APIHttp.getInstance().cars(token, "volkswagen", "gg333h" ).subscribe(cars ->{
            assertNotEquals(cars.code(), 200);
            assertNotNull(cars.body());
            assertNotEquals(cars.body().getName(),"volkswagen");
            assertNotEquals(cars.body().getNumber(),"gg333h");
        },Throwable::printStackTrace);
    }
}
