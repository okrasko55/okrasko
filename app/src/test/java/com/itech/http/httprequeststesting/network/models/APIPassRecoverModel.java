package com.itech.http.httprequeststesting.network.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Oleg K on 15.11.2016.
 */

public class APIPassRecoverModel {
    @SerializedName("status")
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

