package com.itech.http.httprequeststesting;


import com.itech.http.httprequeststesting.network.models.APIPassRecoverModel;

import com.itech.http.httprequeststesting.network.retrofit.APIHttp;

import org.junit.BeforeClass;
import org.junit.Test;


import retrofit2.Response;
import rx.observers.TestSubscriber;

import static org.junit.Assert.assertEquals;

/**
 * Created by Oleg K on 15.11.2016.
 */

public class PassRecoverTest {
    private static Response<APIPassRecoverModel> response;

    @BeforeClass

    public static void init() {

        TestSubscriber<Response<APIPassRecoverModel>> tokenSubscriber;
        tokenSubscriber = new TestSubscriber<>(); //Creating subscriber for getting response
        APIHttp.getInstance().recover("@ex1.com").subscribe(tokenSubscriber);//Sending request to server
        response = tokenSubscriber.getOnNextEvents().get(0);// Getting object of response
    }

    @Test
    public void testCorrectStatusPass(){
        assertEquals(response.body().getStatus(), "success");
    }

    @Test
    public void testCorrectCodePass(){
        assertEquals(response.code(), 200);
    }

    @Test
    public void testIncorrectCodePass(){
        assertEquals(response.code(), 404);
    }
}
