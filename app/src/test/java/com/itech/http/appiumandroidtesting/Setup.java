package com.itech.http.appiumandroidtesting;

import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;

/**
 * Created by Oleg K on 09.02.2017.
 */

public class Setup {
    private static AndroidDriver wd;
    public static String app = "D:/in_nav_client-1.0.2.8-20170203-debug-prod-a838655.apk";
    public static String appPackage = "com.smartnavigationsystems.mallapp.mallapplication";
    public static String activity = "activity.SplashActivity_";
    public static String device = "Android";
    public static String deviceName = "0316038a4a723001";
    public static String platformVersion = "6.0.1";
    public static String platformName = "Android";

    public static AndroidDriver setup() throws MalformedURLException {
        DesiredCapabilities dc = new DesiredCapabilities();
        dc.setCapability("device", device);
        dc.setCapability("deviceName", deviceName);
        dc.setCapability("platformVersion", platformVersion);
        dc.setCapability("platformName", platformName);
        dc.setCapability("app", app);
        dc.setCapability("app-package", appPackage);
        dc.setCapability("app-activity", activity);

        wd = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), dc);
        wd.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        return wd;
    }
    public static void tearDown(){
        if (wd != null){
            wd.quit();
        }
    }
}
