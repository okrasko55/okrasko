package com.itech.http.appiumandroidtesting;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;

import java.net.MalformedURLException;

import io.appium.java_client.android.AndroidDriver;

/**
 * Created by Oleg K on 09.02.2017.
 */

public class OlegsTests {
    private static AndroidDriver wd;
    private static final String BASEPATH = "com.smartnavigationsystems.mallapp.mallapplication:id/";

    @BeforeClass
    public static void appiumUnit(){
        try {
            wd = Setup.setup();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void loginApp() {
        wd.findElement(By.id(BASEPATH + "welcome_button_sign_in")).click();
        wd.findElement(By.id(BASEPATH + "sing_select_type_button_email")).click();
        wd.findElement(By.id(BASEPATH + "login_email_layout")).sendKeys("a@a.aa");
        wd.hideKeyboard();
        wd.findElement(By.id(BASEPATH + "login_password_layout"));
        wd.hideKeyboard();
        wd.findElement(By.id(BASEPATH + "login_log_in")).click();
    }
}
