package com.itech.http.sockettests.api;

/**
 * Created by eugene.iarosh on 3/30/2017.
 */

public final class BuildConfig {
    public static final boolean DEBUG = Boolean.parseBoolean("true");
    public static final String APPLICATION_ID = "com.smartnavigationsystems.ourschoolbus";
    public static final String BUILD_TYPE = "debug";
    public static final String FLAVOR = "dev";
    public static final int VERSION_CODE = 4;
    public static final String VERSION_NAME = "2.1";
    // Fields from product flavor: dev
    public static final String GOOGLE_API_HOST = "https://maps.googleapis.com";

    // DEV serever
    /*public static final String HOST = "http://bus.itcraftlab.com";
    public static final String SOCKET_ADDRESS = "http://dev.traccar.itcraft.i-it.pro:9092";
    public static final String TRACCAR_API_HOST = "http://dev.traccar.itcraft.i-it.pro:5055";*/


    //Live server
    public static final String HOST = "http://ourschoolbus.ae";
    public static final String SOCKET_ADDRESS = "http://traccar.itcraft.i-it.pro:9092";
    public static final String TRACCAR_API_HOST = "http://traccar.itcraft.i-it.pro:5055";
    // Fields from default config.
    public static final String GIT_COMMIT = "27db156";
}
