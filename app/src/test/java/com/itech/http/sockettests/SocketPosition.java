package com.itech.http.sockettests;

import com.itech.http.sockettests.api.API;
import com.itech.http.sockettests.api.ITraccarAPI;
import com.itech.http.sockettests.api.RestClient;
import com.itech.http.sockettests.socketsettings.Constants;
import com.itech.http.sockettests.socketsettings.SocketManager;
import com.itech.http.sockettests.socketsettings.Timer;

import org.junit.Before;
import org.junit.Test;

import io.socket.client.IO;
import io.socket.client.Socket;
import rx.schedulers.Schedulers;

import static com.itech.http.sockettests.api.BuildConfig.TRACCAR_API_HOST;

/**
 * Created by eugene.iarosh on 2/22/2017.
 */

public class SocketPosition implements SocketManager.TrackerSocketListener {
    private SocketManager socketManager;
    private Socket socket;
    private static final String TOKEN = "Hm4eqswi9fuvBhXyEtlBC3pnK6e0YmfUMoYQqbm7KOMSkq0XTQx3hdWfXIy6";
    private Timer timer;
    private RestClient client;

    @Before
    public void init() {
        timer = new Timer();
        System.setProperty("http.proxyHost", "localhost");
        System.setProperty("http.proxyPort", "8888");
        client = new RestClient(TRACCAR_API_HOST);
    }

    @Test
    public void firstTest() throws Exception {
        socket = IO.socket(Constants.BUS_SERVER_URL);
        socketManager = new SocketManager(TOKEN, this, socket);
        socketManager.socketConnect();
        API api = new API();
        timer.start();
        while (!timer.expired(8)) {
            ITraccarAPI.setRequest("qwe1", 1468503421, 50.006561, 36.224900, 0.0f, 0.0f, 107.7, client);
        }
        socketManager.socketDisconnect();
    }

    @Override
    public void onAuthSuccessfully() {
    }
}
