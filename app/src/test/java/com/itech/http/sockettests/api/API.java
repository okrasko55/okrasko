package com.itech.http.sockettests.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by eugene.iarosh on 3/29/2017.
 */

public class API {

    private ITraccarAPI iTraccarAPI;
    private IErrorsControllerModule errorsCheckerModule;

    public API() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient
                .Builder()
                .addInterceptor(chain -> {
                    Request original = chain.request();
                    Request request = original.newBuilder()
                            .header("Accept-Language", Locale.getDefault().getLanguage())
                            .header("Accept", "application/json")
                            .method(original.method(), original.body())
                            .build();

                    return chain.proceed(request);
                })
                .addInterceptor(interceptor)
                .connectTimeout(30, TimeUnit.SECONDS)
                .build();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit traccarApiRetrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BuildConfig.TRACCAR_API_HOST)
                .client(client)
                .build();

        iTraccarAPI = traccarApiRetrofit.create(ITraccarAPI.class);
    }

    public void sentLocationToTraccar(String trackerId, long timestamp, double lat, double lng, float speed, float bearing, double altitude) {
        iTraccarAPI.sentLocationToTraccar(trackerId, timestamp, lat, lng, speed, bearing, altitude);
    }


    private Response errorController(Response response) {
        if (errorsCheckerModule != null) {
            return errorsCheckerModule.responseControl(response);
        }
        return response;
    }
}
