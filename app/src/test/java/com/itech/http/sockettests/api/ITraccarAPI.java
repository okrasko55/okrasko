package com.itech.http.sockettests.api;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import rx.Observable;
import rx.observers.TestSubscriber;

/**
 * Created by eugene.iarosh on 3/29/2017.
 */

public interface ITraccarAPI {
    @GET("/")
    @Headers("Accept: application/json")
    Observable<Response<Object>> sentLocationToTraccar(@Query("id") String trackerId,
                                                       @Query("timestamp") long timestamp,
                                                       @Query("lat") double lat,
                                                       @Query("lon") double lng,
                                                       @Query("speed") float speed,
                                                       @Query("bearing") float bearing,
                                                       @Query("altitude") double altitude);

    public static void setRequest (String trackerId, long timestamp, double lat, double lng, float speed, float bearing, double altitude, RestClient client) {

        TestSubscriber<Object> subscriber = new TestSubscriber<>();
        client.getApiService().sentLocationToTraccar(trackerId,timestamp,lat,lng, speed, bearing, altitude).subscribe(subscriber);
    }
}
