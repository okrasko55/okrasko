package com.itech.http.sockettests.api;

import retrofit2.Response;

/**
 * Created by eugene.iarosh on 3/29/2017.
 */

public interface IErrorsControllerModule {
    Response responseControl(Response response);
}
