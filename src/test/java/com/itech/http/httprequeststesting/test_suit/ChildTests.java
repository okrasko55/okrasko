package com.itech.http.httprequeststesting.test_suit;

import com.itech.http.httprequeststesting.DB.DbSessionFactory;
import com.itech.http.httprequeststesting.DB.tables.ChildDataSet;
import com.itech.http.httprequeststesting.UserCredentials.UserData;
import com.itech.http.httprequeststesting.network.models.APIChildSyncModel;
import com.itech.http.httprequeststesting.network.models.APILoginModel;
import com.itech.http.httprequeststesting.network.retrofit.APIHttp;
import com.itech.http.httprequeststesting.utils.Utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import retrofit2.Response;
import rx.observers.TestSubscriber;

import static org.junit.Assert.assertEquals;

/**
 * Created by Alyona on 14.11.2016.
 */

public class ChildTests {
    private static Transaction tx;
    private static SessionFactory sessionFactory;
    private static Session session;
    private static ChildDataSet childDataSet;
    private static String userToken;
    private static String childName = "New test user";

    @BeforeClass
    public static void dbSetUp() {
        //login with user and get token and id
        UserData userData = new UserData();
        APILoginModel apiLoginModel = userData.getUserLoginModelData("t1@i.ua", "qwe123");
        userToken = apiLoginModel.getToken();
        String parentId = apiLoginModel.getId();

        //get current date time
        long currentLongDateTime =  Utils.getCurrentDateTime();

        //Create instance of ChildDataSet
        childDataSet = new ChildDataSet(Integer.valueOf(parentId), childName, "123456", currentLongDateTime, currentLongDateTime, 2, "");

        //save childDataSet to DB
        session = DbSessionFactory.getSessionFactory().openSession();
        tx = session.beginTransaction();
        session.save(childDataSet);
        tx.commit();
        session.close();
    }


    //Test #1: sync request for getting a list of children returns successful result with code=200
    @Test
    public void testChildSyncReturnsCorrectCode() throws ParseException {
        TestSubscriber<Response<ArrayList<APIChildSyncModel>>> childSyncSubscriber = new TestSubscriber<>();

        //get current time in needed format and cast it to String
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date resultDate = new Date();
        String sCurrectTime =  sdf.format(resultDate).toString();

        //send childSync request with tiken and since parameters
        APIHttp.getInstance().childSync(userToken,sCurrectTime).subscribe(childSyncSubscriber);
        Response<ArrayList<APIChildSyncModel>> response = childSyncSubscriber.getOnNextEvents().get(0);

        //check that code in response equals 200
        assertEquals(200, response.code());
    }


    //Test#2 sync request Since NOW(current time) returns 0 size of children
    @Test
    public void testChildSyncIsEmptySinceCurrentTime() throws ParseException {
        TestSubscriber<Response<ArrayList<APIChildSyncModel>>> childSyncSubscriber = new TestSubscriber<>();

        //get current time in needed format and cast it to String
        Calendar cal = Calendar.getInstance();
        Date resultDate = new Date();
        cal.setTime(resultDate);
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sCurrectTime = format1.format(cal.getTime()).toString();


        APIHttp.getInstance().childSync(userToken, sCurrectTime).subscribe(childSyncSubscriber);
        Response<ArrayList<APIChildSyncModel>> response = childSyncSubscriber.getOnNextEvents().get(0);

        Assert.assertEquals(response.body().size(), 0);
    }


    //Test#3: sync request returns that the last child in array is added TODAY
    @Test
    public void testChildSyncReturnsCorrectDateForLastChild() throws ParseException {

        TestSubscriber<Response<ArrayList<APIChildSyncModel>>> childSyncSubscriber = new TestSubscriber<>();
        APIHttp.getInstance().childSync(userToken,"2016-11-11 10:10:10").subscribe(childSyncSubscriber);
        Response<ArrayList<APIChildSyncModel>> response = childSyncSubscriber.getOnNextEvents().get(0);

        String updatedAt = response.body().get(response.body().size()-1).getUpdatedAt();
        DateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        Date date = format.parse(updatedAt);


        Calendar updatedCalendarDay = new GregorianCalendar();
        updatedCalendarDay.setTime(date);

        Calendar currectCalendarDay = new GregorianCalendar() ;
        currectCalendarDay.setTime(new Date());


        Boolean isEqualDates = false;
        if(currectCalendarDay.get(Calendar.YEAR) == updatedCalendarDay.get(Calendar.YEAR)
                && currectCalendarDay.get(Calendar.MONTH) == updatedCalendarDay.get(Calendar.MONTH)
                    && currectCalendarDay.get(Calendar.DATE) == updatedCalendarDay.get(Calendar.DATE)){
            isEqualDates = true;
        }

        Assert.assertTrue(isEqualDates);
    }


    //Test#4 sync request returns correct name for the last added child
    @Test
    public void testChildSyncReturnsCorrectChildName() throws ParseException {
        TestSubscriber<Response<ArrayList<APIChildSyncModel>>> childSyncSubscriber = new TestSubscriber<>();

        APIHttp.getInstance().childSync(userToken, "2016-11-11 10:10:10").subscribe(childSyncSubscriber);
        Response<ArrayList<APIChildSyncModel>> response = childSyncSubscriber.getOnNextEvents().get(0);
        String nameFromRequest  = response.body().get(response.body().size()-1).getName();

        Assert.assertEquals(childName, nameFromRequest);
    }

    //Test#5 sync request returns correct name for the last added child
    @Test
    public void testChildSyncReturnsErrorWithIncorrectToken() throws ParseException, IOException {
        TestSubscriber<Response<ArrayList<APIChildSyncModel>>> childSyncSubscriber = new TestSubscriber<>();

        APIHttp.getInstance().childSync("***", "2016-11-11 10:10:10").subscribe(childSyncSubscriber);
        Response<ArrayList<APIChildSyncModel>> response = childSyncSubscriber.getOnNextEvents().get(0);

        Assert.assertEquals("Unauthorized",response.message());
    }


    @AfterClass
    public static void dbClear() {
        //remove childDataSet to DB
        sessionFactory = DbSessionFactory.getSessionFactory();
        Session session = sessionFactory.openSession();
        tx = session.beginTransaction();
        session.delete(childDataSet);
        tx.commit();
        session.close();
        DbSessionFactory.close();
    }

}
