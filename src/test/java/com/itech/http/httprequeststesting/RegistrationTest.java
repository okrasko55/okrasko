package com.itech.http.httprequeststesting;


import com.google.gson.Gson;
import com.google.gson.internal.ObjectConstructor;
import com.itech.http.httprequeststesting.network.models.APIErrorsModel;
import com.itech.http.httprequeststesting.network.models.APILoginModel;
import com.itech.http.httprequeststesting.network.models.APIRegistrationModel;
import com.itech.http.httprequeststesting.network.retrofit.APIHttp;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Random;

import retrofit2.Response;
import rx.observers.TestSubscriber;

import static org.junit.Assert.assertEquals;

/**
 * Created by Alyona on 17.10.2016.
 */

public class RegistrationTest {

    private TestSubscriber<Response<APIRegistrationModel>> registrationSubscriber;
    private APIRegistrationModel  apiRegistrationModel;
    private Response<APIRegistrationModel> response;
    private APIErrorsModel apiErrorsModel;


    @Test
    public void testRegistrationWithExistingUserReturnsErrorCodeResult(){
        assertEquals(422, getResponse("firstName", "lastName", "e@e.e", "",  "", "qweqwe", "" ,"").code());

    }

    @Test
    public void testRegistrationWithNotExistingUserReturnsSuccessCodeResult(){
        Random r = new Random();
        int random = r.nextInt(1231237);
        assertEquals(200, getResponse("firstName", "lastName", "testuser"+random+"@i.ua", "",  "", "qwe123", "" ,"").code());

    }


    private Response<APIRegistrationModel> getResponse(String firstName, String lastName, String email,
                                                       String phone_code,  String phone_value, String password,
                                                       String photo , String photo_landscape ){



        registrationSubscriber = new TestSubscriber<>(); //Creating subscriber for getting response
        APIHttp.getInstance().registration(firstName,lastName,email, phone_code,phone_value, password,photo,photo_landscape).subscribe(registrationSubscriber); //Sending request to server
        response = registrationSubscriber.getOnNextEvents().get(0); // Getting object of response

        apiErrorsModel = new APIErrorsModel();
        if (response.code() != 200) {
            try {
                apiErrorsModel = new Gson().fromJson(response.errorBody().string(), APIErrorsModel.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return response;

<<<<<<< HEAD
        //test master
=======
>>>>>>> 8539c414b431f8ed8adf0890482fde0b04c82bfa

    }

}
