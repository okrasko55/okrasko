package com.itech.http.httprequeststesting;

import com.itech.http.httprequeststesting.network.models.APILoginModel;
import com.itech.http.httprequeststesting.network.retrofit.APIHttp;

import org.junit.BeforeClass;
import org.junit.Test;

import retrofit2.Response;
import rx.observers.TestSubscriber;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class LoginResponseTest {
<<<<<<< HEAD
    private static APILoginModel userResponse;
    private static Response<APILoginModel> response;

    @BeforeClass

    public static void init(){

        TestSubscriber<Response<APILoginModel>> tokenSubscriber;
        tokenSubscriber = new TestSubscriber<>(); //Creating subscriber for getting response
        APIHttp.getInstance().login("barbinkh@gmail.com", "", "", "qweqweqwe").subscribe(tokenSubscriber); //Sending request to server
        response = tokenSubscriber.getOnNextEvents().get(0); // Getting object of response
        userResponse = response.body(); //Getting response as model
=======
    private TestSubscriber<Response<APILoginModel>> loginSubscriber;
    private APILoginModel apiLoginModel;
    private Response<APILoginModel> response;

    @Before
    public void sendLoginRequest() {
        loginSubscriber = new TestSubscriber<>(); //Creating subscriber for getting response
        APIHttp.getInstance().login("e@e.e", "", "", "qweqwe").subscribe(loginSubscriber); //Sending request to server
        response = loginSubscriber.getOnNextEvents().get(0); // Getting object of response
        apiLoginModel = response.body(); //Getting response as model
>>>>>>> eef3a42c4380aa361df946b62578b6539bd11767
    }

    @Test
    public void testLoginRequestErrors() throws Exception {
        TestSubscriber<Response<APILoginModel>> loginSubscriber = new TestSubscriber<>();
        APIHttp.getInstance().login("e@e.e", "", "", "qweqwe").subscribe(apiLoginModelResponse -> {
            assertEquals(apiLoginModelResponse.code(), 404);
        }, Throwable::printStackTrace);
        loginSubscriber.assertNoErrors(); //Checking errors
        assertEquals(response.code(), 200); // Checking status of response
    }

    @Test
    public void testCorrectEmailPass() {
        assertEquals(userResponse.getFirstName(), "Konstantin"); // Checking first name
    }

    void response(APILoginModel apiLoginModel) {
        apiLoginModel.getFirstName();
    }
}