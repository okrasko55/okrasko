package com.itech.http.httprequeststesting;

import com.itech.http.httprequeststesting.UserCredentials.UserData;
import com.itech.http.httprequeststesting.network.models.APILoginModel;
import com.itech.http.httprequeststesting.network.models.APITokenModel;
import com.itech.http.httprequeststesting.network.models.user.UserResponse;
import com.itech.http.httprequeststesting.network.retrofit.APIHttp;

import org.junit.Before;
import org.junit.Test;

import retrofit2.Response;
import rx.observers.TestSubscriber;

import static org.junit.Assert.assertEquals;

/**
 * Created by Alyona on 10.10.2016.
 */

public class TokenTest {
    TestSubscriber<Response<APITokenModel>> tokenSubscriber;
    APITokenModel apiTokenModel;
    Response<APITokenModel> response;
    String userToken;

    @Before
    public void sendLoginRequest() {
        //Creating subscriber for getting response
        tokenSubscriber = new TestSubscriber<>();

        //login with user and get token
        UserData userData = new UserData();
        APILoginModel apiLoginModel = userData.getUserLoginModelData("barbinkh@gmail.com", "qweqweqwe");
        userToken = apiLoginModel.getToken();


        APIHttp.getInstance().token(userToken).subscribe(tokenSubscriber); //Sending request to server
        response = tokenSubscriber.getOnNextEvents().get(0); // Getting object of response
        apiTokenModel = response.body(); //Getting response as model
    }

    @Test
    public void testTokenRequestErrors() throws Exception {
        tokenSubscriber.assertNoErrors(); //Checking errors
        assertEquals(response.code(), 200); // Checking status of response
    }

    @Test
    public void testNameByToken() {
        assertEquals(apiTokenModel.getFirstName(), "Konstantin"); // Checking first name
    }


}
