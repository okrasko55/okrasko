package com.itech.http.httprequeststesting;

import com.google.gson.Gson;
import com.itech.http.httprequeststesting.network.models.APIErrorsModel;
import com.itech.http.httprequeststesting.network.models.APILoginModel;
import com.itech.http.httprequeststesting.network.retrofit.APIHttp;

import org.junit.Test;

import java.io.IOException;

import retrofit2.Response;
import rx.observers.TestSubscriber;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class LoginRequestTest {
    private TestSubscriber<Response<APILoginModel>> loginSubscriber;
    private Response<APILoginModel> response;
    private APIErrorsModel apiErrorsModel;

    private Response<APILoginModel> getResponse(String email, String code, String phone, String password) {
        loginSubscriber = new TestSubscriber<>(); //Creating subscriber for getting response
        APIHttp.getInstance().login(email, code, phone, password).subscribe(loginSubscriber);
        response = loginSubscriber.getOnNextEvents().get(0); // Getting object of response
        apiErrorsModel = new APIErrorsModel();
        if (response.code() != 200) {
            try {
                apiErrorsModel = new Gson().fromJson(response.errorBody().string(), APIErrorsModel.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return response;
    }


    @Test
    public void testCorrectEmailPass() {

        assertEquals(200, getResponse("e@e.e", "", "", "qweqwe").code());
    }

    @Test
    public void testCorrectPhonePass() {
        assertEquals(200, getResponse("", "971", "1234566667", "a").code());
    }

    @Test
    public void testIncorrectEmailPass() {
        assertEquals(422, getResponse("t_test@t.c", "", "", "qweqweqwe").code());
    }

    @Test
    public void testInCorrectPhonePass() {
        assertEquals(422, getResponse("", "971", "123456666", "a").code());
    }

    @Test
    public void testInCorrectFormatEmailPass() {
        getResponse("t@t", "", "", "qweqweqwe");
        assertEquals("EMAIL_FORMAT", apiErrorsModel.getErrors().get("email").get(0));
    }
}