package com.itech.http.httprequeststesting.requestcontrol;

import com.itech.http.httprequeststesting.network.models.venues.VenueSyncObjectList;
import com.itech.http.httprequeststesting.network.retrofit.APIHttp;

import retrofit2.Response;
import rx.observers.TestSubscriber;

/**
 * Created by Igor on 14.11.2016.
 */

public class SyncController {

    public TestSubscriber<Response<VenueSyncObjectList>> GetSyncObjectBySinceDate(String token, String since) {
        TestSubscriber<Response<VenueSyncObjectList>> syncSubscriber = new TestSubscriber<>();
        APIHttp.getInstance().getVenuesSyncList(token, since).subscribe(syncSubscriber);
        return syncSubscriber;
    }
}
