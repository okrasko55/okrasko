package com.itech.http.httprequeststesting;

import com.itech.http.httprequeststesting.DB.DbSessionFactory;
import com.itech.http.httprequeststesting.DB.tables.MallsDataSet;
import com.itech.http.httprequeststesting.Logger.Log4jConfigure;
import com.itech.http.httprequeststesting.network.models.APILoginModel;
import com.itech.http.httprequeststesting.network.models.venues.VenueSyncObjectList;
import com.itech.http.httprequeststesting.network.retrofit.APIHttp;
import com.itech.http.httprequeststesting.requestcontrol.SyncController;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import retrofit2.Response;
import rx.observers.TestSubscriber;

/**
 * Created by Igor on 04.11.2016.
 */

public class SyncTest {
    private static APILoginModel userResponse;
    private Transaction tx;
    private Session session;
    private MallsDataSet mallsDataSet;
    private static SyncController syncController;
    private int[] idList = new int[]{29, 30};

    @BeforeClass

    public static void init(){
        Response<APILoginModel> response;
        TestSubscriber<Response<APILoginModel>> tokenSubscriber;
        Log4jConfigure.configureSimpleConsole();
        syncController = new SyncController();
        tokenSubscriber = new TestSubscriber<>(); //Creating subscriber for getting response
        APIHttp.getInstance().login("barbinkh@gmail.com", "", "", "qweqweqwe").subscribe(tokenSubscriber); //Sending request to server
        response = tokenSubscriber.getOnNextEvents().get(0); // Getting object of response
        userResponse = response.body(); //Getting response as model
    }

    @Test
    public void syncBaseTest() {
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        Calendar calendar = new GregorianCalendar();
        calendar.add(Calendar.MONTH, -5);
        syncController.GetSyncObjectBySinceDate(userResponse.getToken(), sdf.format(calendar.getTime()));
    }

    @Test
    public void syncAfterUpdateTest() throws ParseException {
        VenueSyncObjectList venueSyncObjectList;

        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        Calendar calendar = new GregorianCalendar();
        Date updatedAt = calendar.getTime();
        int updatedAtInt = (int) (updatedAt.getTime() / 1000);

        session = DbSessionFactory.getSessionFactory().openSession();
        tx = session.beginTransaction();
        for (int i : idList) {
            mallsDataSet = session.load(MallsDataSet.class, i);
            mallsDataSet.setUpdatedAt(Long.valueOf(updatedAtInt));
            session.update(mallsDataSet);
        }
        tx.commit();
        session.close();

        calendar.add(Calendar.HOUR, -4);
        calendar.add(Calendar.MINUTE, -5);

        venueSyncObjectList = syncController.GetSyncObjectBySinceDate(userResponse.getToken(), sdf.format(calendar.getTime())).getOnNextEvents().get(0).body();

        for (int i = 0; i < idList.length; i++) {
            Assert.assertEquals(String.valueOf(idList[i]), venueSyncObjectList.getUpdate().get(i).getId());
        }

    }
}
