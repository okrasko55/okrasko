package com.itech.http.httprequeststesting.utils;

import java.util.Date;

/**
 * Created by Alyona on 15.11.2016.
 */

public class Utils {

    //get in Long format current date time
    public static long getCurrentDateTime() {
        Date date = new Date();
        long mills = date.getTime() / 1000;
        return mills;
    }


}
