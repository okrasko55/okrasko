package com.itech.http.httprequeststesting.UserCredentials;

import com.itech.http.httprequeststesting.network.models.APILoginModel;
import com.itech.http.httprequeststesting.network.retrofit.APIHttp;

import retrofit2.Response;
import rx.observers.TestSubscriber;

/**
 * Created by Alyona on 15.11.2016.
 */

public class UserData {

    private TestSubscriber<Response<APILoginModel>> loginSubscriber;
    private APILoginModel apiLoginModel;
    private Response<APILoginModel> response;

    public APILoginModel getUserLoginModelData(String login, String password){

        loginSubscriber = new TestSubscriber<>(); //Creating subscriber for getting response
        APIHttp.getInstance().login(login,"","", password).subscribe(loginSubscriber); //Sending request to server
        response = loginSubscriber.getOnNextEvents().get(0); // Getting object of response
        apiLoginModel = response.body(); //Getting response as model
        return apiLoginModel;
    }
}
