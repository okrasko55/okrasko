package com.itech.http.httprequeststesting.sockets.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Igor on 14.04.2017.
 */

public class IndoorCoordinate {

    @SerializedName("x")
    long x;

    @SerializedName("y")
    long y;

    @SerializedName("z")
    long z;

    public long getZ() {
        return z;
    }

    public long getX() {
        return x;
    }

    public long getY() {
        return y;
    }

    public void setZ(long z) {
        this.z = z;
    }

    public void setY(long y) {
        this.y = y;
    }

    public void setX(long x) {
        this.x = x;
    }

    @Override
    public String toString() {
        return "{ x=" + x  +" y=" +y + " z=" + z + " }";
    }
}
