package com.itech.http.httprequeststesting.sockets.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Igor on 14.04.2017.
 */

public class BaseResponse{

    public static String ERROR_USER_NOT_FOUND = "USER_NOT_FOUND";
    public static String ERROR_TOKEN_NOT_FOUND = "TOKEN_NOT_FOUND";
    public static String ERROR_DB_ERROR = "DB_ERROR";
    public static String ERROR_AUTH_ERROR = "AUTH_ERROR";
    public static String ERROR_NO_DATA = "NO_DATA";
    public static String ERROR_SQL = "SQL";

    @SerializedName("success")
    boolean success;

    @SerializedName(value = "error", alternate = "errors")
    String error;


    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    @Override
    public String toString() {
        return "{ success =" + success + ", error = " + error + " }";
    }
}
