package com.itech.http.httprequeststesting.sockets.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Igor on 14.04.2017.
 */

public class ObjectIndoorResponse  extends BaseResponse {

    @SerializedName("data")
    ObjectIndoorLocation location;

    public ObjectIndoorLocation getLocation() {
        return location;
    }

    public void setLocation(ObjectIndoorLocation location) {
        this.location = location;
    }
}
