package com.itech.http.httprequeststesting.sockets;

import com.itech.http.appiumandroidtesting.core.Timer;
import com.itech.http.httprequeststesting.DB.DataAccessObject;
import com.itech.http.httprequeststesting.DB.tables.TokenDataSet;
import com.itech.http.httprequeststesting.restapi.utils.TimeUtils;
import com.itech.http.httprequeststesting.restapi.utils.UsersContainer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Random;

/**
 * Created by inevzorov on 3/24/17.
 */

public class SocketTest {
    private static String token;
    private TokenDataSet tokenDataSet;
    private UsersContainer usersContainer;
    private SocketManager userSocket;
    private SocketManager childSocket;


    @Before
    public void init() throws Exception {
        Random random = new Random();
        usersContainer = new UsersContainer();
        token = "77a2aaaa3f6e1c8f1200fbf9ee876b6440e" + random.nextInt(10000);
        tokenDataSet = new TokenDataSet(token, usersContainer.getUser().getId(), TimeUtils.getBeginningTimeStamp() + 31536000);
        DataAccessObject.addObject(tokenDataSet);
        userSocket = new SocketManager(token);
        userSocket.socketConnect();
        childSocket = new SocketManager(usersContainer.getChildList().get(0).getToken(), true);
        childSocket.socketConnect();
      /*  RequestSocket requestSocket = new RequestSocket(new RequestSocket.Options());
        requestSocket.create();*/
    }

    @After
    public void tearDown() throws Exception {
        usersContainer.clear();
    }

    @Test
    public void setUp() throws Exception {

        Timer timer = new Timer();
        timer.start();
        while (!timer.expired(60)) {
            childSocket.sendChildGpsLocation(10, 20);
            Thread.sleep(1000);
        }
    }


}
