package com.itech.http.httprequeststesting.sockets;

import com.google.gson.Gson;
import com.itech.http.httprequeststesting.restapi.BuildConfig;
import com.itech.http.httprequeststesting.restapi.utils.TimeUtils;
import com.itech.http.httprequeststesting.sockets.model.BaseResponse;
import com.itech.http.httprequeststesting.sockets.model.ChildCoordinatesObject;
import com.itech.http.httprequeststesting.sockets.model.EndPointCoordinatesObject;
import com.itech.http.httprequeststesting.sockets.model.GpsCoordinate;
import com.itech.http.httprequeststesting.sockets.model.IndoorCoordinate;
import com.itech.http.httprequeststesting.sockets.model.ObjectGpsLocation;
import com.itech.http.httprequeststesting.sockets.model.ObjectGpsResponse;
import com.itech.http.httprequeststesting.sockets.model.ObjectIndoorLocation;
import com.itech.http.httprequeststesting.sockets.model.ObjectIndoorResponse;

import org.json.JSONObject;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by inevzorov on 3/30/17.
 */

public class SocketManager {

    private Socket socket;
    private String token;
    private boolean socketConnected;
    private boolean socketAuthorized;
    private boolean isChild;


    {
        try {
            socket = IO.socket(BuildConfig.SOCKET_ADDRESS + "/" + BuildConfig.SOCKET_VERSION);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public SocketManager(String token) {
        this.token = token;
    }

    public SocketManager(String token, boolean isChild) {
        this.token = token;
        this.isChild = isChild;
    }

    private void subscribeOnSocketEvents() {
        //authorize(TOKEN);
        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                System.out.println("Socket connection event");
                socketConnected = true;
                authorize(token);

                //socket.close();
            }
        });
        socket.on(Socket.EVENT_ERROR, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                System.out.println("Socket EVENT_ERROR event" + String.valueOf(args[0]));
                socketConnected = false;

                //socket.close();
            }
        });

        socket.on(Socket.EVENT_MESSAGE, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                System.out.println("Socket EVENT_MESSAGE event" + String.valueOf(args[0]));

                //socket.close();
            }
        });
        socket.on(Socket.EVENT_DISCONNECT, args -> {
            System.out.println("Socket disconnect event" + String.valueOf(args[0]));
            socketConnected = false;
            socketAuthorized = false;
        });


        socket.on("token", tokenListener);
        socket.on("child_token", tokenListener);
        socket.on("child_indoor_location", indoorCoordinatesChildListener);
        socket.on("child_gps_location", coordinatesGpsChildListener);
    }

    private void authorize(String token) {
        System.out.println("Try to authorize");
        if (isChild == true){
            socket.emit("child_token", token);
        }else {
            socket.emit("token", token);
        }
        socketAuthorized = true;
    }

    private Emitter.Listener tokenListener = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            //Gson gson = new Gson();
            //BaseResponse auth = gson.fromJson(String.valueOf(args[0]), BaseResponse.class);
            System.out.println(String.valueOf(args[0]));
           /* if (auth == null){
                return;
            }
*/
          /*  Log.i("TAG", "Socket tokenListener " + BuildConfig.SOCKET_ADDRESS  + " " + auth.toString());
            socketAuthorized = auth.isSuccess();

            if (auth.isSuccess()){
                if (hasNotSendIndoorLocation) {
                    sendIndoorLocation(notSendX, notSendY, notSendZ, notSendVenueId);
                }
                if (hasNotSendGpsLocation) {
                    sendGpsLocation(latitude, longitude);
                }
            }*/
        }
    };

    private Emitter.Listener gpsCoordinatesListener = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            if (args.length > 0) {

                Gson gson = new Gson();
                BaseResponse gpsResp = gson.fromJson(String.valueOf(args[0]), BaseResponse.class);
                if (gpsResp == null) {
                    return;
                }
                System.out.println("Socket " + SocketConfig.DEV_SOCKET_ADDRESS + " gpsCoordinatesListener = " + gpsResp.toString());
            }
        }
    };

    private Emitter.Listener coordinatesGpsChildListener = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            if (args.length == 0){
                return;
            }
            System.out.println("Socket CHILD GPS LOCATION " + String.valueOf(args[0]));

            Gson gson = new Gson();
            ObjectGpsResponse response = gson.fromJson(String.valueOf(args[0]), ObjectGpsResponse.class);

            if (response == null) {
                return;
            }

            if (response.isSuccess() && response.getGpsLocation() != null){
                ObjectGpsLocation locationData = response.getGpsLocation();
                GpsCoordinate gpsCoordinate = locationData.getGpsCoordinate();
                if (gpsCoordinate != null) {
                    EndPointCoordinatesObject childCoordinatesObject = new EndPointCoordinatesObject();
                    childCoordinatesObject.setPk(locationData.getObjectId());
                    childCoordinatesObject.setLatitude(String.valueOf(gpsCoordinate.getLat()));
                    childCoordinatesObject.setLongitude(String.valueOf(gpsCoordinate.getLng()));
                    System.out.println(String.valueOf(gpsCoordinate.getLat()));
                    System.out.println(String.valueOf(gpsCoordinate.getLng()));

                }
            }
        }
    };

    private Emitter.Listener indoorCoordinatesChildListener = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            if (args.length == 0) {
                return;
            }

            System.out.println("Socket child Indoor location " + String.valueOf(args[0]));

            Gson gson = new Gson();
            ObjectIndoorResponse response = gson.fromJson(String.valueOf(args[0]), ObjectIndoorResponse.class);

            if (response == null) {
                return;
            }


            if (response.isSuccess() && response.getLocation() != null) {
                ObjectIndoorLocation locationData = response.getLocation();
                IndoorCoordinate indoorCoordinate = locationData.getIndoorCoordinate();
                if (indoorCoordinate != null) {
                    ChildCoordinatesObject childCoordinatesObject = new ChildCoordinatesObject();

                    childCoordinatesObject.setChild_id(String.valueOf(locationData.getId()));
                    childCoordinatesObject.setMall_id(String.valueOf(locationData.getVenueId()));
                    childCoordinatesObject.setUpdated_at(TimeUtils.getCurrentTimeStamp());
                    childCoordinatesObject.setX(String.valueOf(indoorCoordinate.getX()));
                    childCoordinatesObject.setY(String.valueOf(indoorCoordinate.getY()));
                    childCoordinatesObject.setZ(String.valueOf(indoorCoordinate.getZ()));
                    System.out.println(String.valueOf(indoorCoordinate.getX()));
                    System.out.println(String.valueOf(indoorCoordinate.getY()));
                    System.out.println(String.valueOf(indoorCoordinate.getZ()));

                }
            }
        }
    };

    public void sendGpsLocation(double latitude, double longitude) {
        if (socket != null) {
            if (socketAuthorized) {
                JSONObject json = ParamsBuilder.prepareSendGpsObject(latitude, longitude);
                if (json != null) {
                    System.out.println("Socket " + SocketConfig.DEV_SOCKET_ADDRESS + " emit " + ParamsBuilder.SOCKET_EVENT_GPS_COORDINATES + " " + json.toString());
                    socket.emit(ParamsBuilder.SOCKET_EVENT_GPS_COORDINATES, json);
                }
            }
        }
    }
    public void sendChildGpsLocation(double latitude, double longitude) {
        if (socket != null) {
            if (socketAuthorized) {
                JSONObject json = ParamsBuilder.prepareSendGpsObject(latitude, longitude);
                if (json != null) {
                    System.out.println("Socket " + SocketConfig.DEV_SOCKET_ADDRESS + " emit " + ParamsBuilder.SOCKET_EVENT_CHILD_GPS_COORDINATES + " " + json.toString());
                    socket.emit(ParamsBuilder.SOCKET_EVENT_CHILD_GPS_COORDINATES, json);
                }
            }
        }
    }



    public void socketConnect() {
        if (socket != null) {
            subscribeOnSocketEvents();
            socket.open();
        }
    }

    public void socketDisconnect() {
        if (socket != null) {
            socket.close();
            socket.off(); //unsubscribe from socket events
        }
    }


}
