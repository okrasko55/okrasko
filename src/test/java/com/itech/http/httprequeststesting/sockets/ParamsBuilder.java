package com.itech.http.httprequeststesting.sockets;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Igor on 18.04.2017.
 */

public class ParamsBuilder {


    private static final String VERSION_2 = "v2";

    private static final String EVENT_TOKEN = "token";
    private static final String SOCKET_EVENT_COORDINATES = "coordinates";
    private static final String SOCKET_EVENT_INDOOR_COORDINATES = "indoor_coordinates";

    public static final String SOCKET_EVENT_GPS_COORDINATES = "gps_coordinates";

    public static final String SOCKET_EVENT_CHILD_GPS_COORDINATES = "child_gps_coordinates";

    private static final String SOCKET_EVENT_CHILD_GPS_V2 = "child_gps_location";

    private static final String SOCKET_EVENT_CHILD_INDOOR_V2 = "child_indoor_location";

    private static final String SOCKET_EVENT_FRIEND_INDOOR_V2 = "friend_indoor_location";

    private static final String SOCKET_EVENT_FRIEND_GPS_V2 = "friend_gps_location";

    private static final String SOCKET_EVENT_CHILD_DELETE = "child_delete";


    public static String GPS_LAT_V1 = "gps_latitude";
    public static String GPS_LNG_V1 = "gps_longitude";

    public static String GPS_LAT_V2 = "lat";
    public static String GPS_LNG_V2 = "lng";

    public static String INDOOR_X = "x";
    public static String INDOOR_Y = "y";
    public static String INDOOR_Z = "z";
    public static String INDOOR_IN_MALL = "in_mall";

    public static JSONObject prepareSendIndoorObject(long x, long y, long z, long venueId){
        JSONObject json = new JSONObject();
        try {
            json.put(INDOOR_X, x);
            json.put(INDOOR_Y, y);
            json.put(INDOOR_Z, z);
            json.put(INDOOR_IN_MALL, venueId);
            return json;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static JSONObject prepareSendGpsObject(double lat, double lng){
        JSONObject json = new JSONObject();
        try {
            json.put( GPS_LAT_V2, lat);
            json.put( GPS_LNG_V2, lng);
            return json;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
