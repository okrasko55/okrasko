package com.itech.http.httprequeststesting.sockets.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Igor on 18.04.2017.
 */
public class GpsCoordinate {

    @SerializedName(value = "lat", alternate = "gps_latitude")
    double lat;

    @SerializedName(value = "lng", alternate = "gps_longitude")
    double lng;

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
