package com.itech.http.httprequeststesting.sockets.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Igor on 14.04.2017.
 */

public class ObjectIndoorLocation {

    @SerializedName(value = "friend_id", alternate = "child_id")
    long id;

    @SerializedName("mall_id")
    long venueId;

    @SerializedName("coordinates")
    IndoorCoordinate indoorCoordinate;

    @SerializedName("updated_at")
    long updatedAt;

    public long getVenueId() {
        return venueId;
    }

    public IndoorCoordinate getIndoorCoordinate() {
        return indoorCoordinate;
    }

    public long getId() {
        return id;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setVenueId(long venueId) {
        this.venueId = venueId;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setIndoorCoordinate(IndoorCoordinate indoorCoordinate) {
        this.indoorCoordinate = indoorCoordinate;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "{ id=" + id  +" venueId=" +venueId + " coordinates=" + indoorCoordinate.toString() + " }";
    }

}
