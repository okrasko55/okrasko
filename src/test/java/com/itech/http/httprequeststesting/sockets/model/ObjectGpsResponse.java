package com.itech.http.httprequeststesting.sockets.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Igor on 18.04.2017.
 */

public class ObjectGpsResponse extends BaseResponse {

    @SerializedName("data")
    private ObjectGpsLocation gpsLocation;

    public ObjectGpsLocation getGpsLocation() {
        return gpsLocation;
    }

    public void setGpsLocation(ObjectGpsLocation gpsLocation) {
        this.gpsLocation = gpsLocation;
    }

}
