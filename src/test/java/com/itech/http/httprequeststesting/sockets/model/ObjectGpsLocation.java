package com.itech.http.httprequeststesting.sockets.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Igor on 18.04.2017.
 */

public class ObjectGpsLocation {

    @SerializedName(value = "friend_id", alternate = "child_id")
    long objectId;

    @SerializedName("coordinates")
    GpsCoordinate gpsCoordinate;

    @SerializedName("updated_at")
    long updatedAt;

    public GpsCoordinate getGpsCoordinate() {
        return gpsCoordinate;
    }

    public long getObjectId() {
        return objectId;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    public void setGpsCoordinate(GpsCoordinate gpsCoordinate) {
        this.gpsCoordinate = gpsCoordinate;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }
}