package com.itech.http.httprequeststesting.sockets.model;

/**
 * Created by Igor on 14.04.2017.
 */

public class EndPointCoordinatesObject {

    private Long pk;
    private String latitude;
    private String longitude;


    public Long getPk() {
        return pk;
    }

    public void setPk(Long pk) {

        this.pk = pk;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}