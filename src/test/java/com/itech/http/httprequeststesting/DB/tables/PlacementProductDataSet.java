package com.itech.http.httprequeststesting.DB.tables;

import com.google.gson.annotations.SerializedName;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Igor on 15.03.2017.
 */

@Entity
@Table(name = "placement_product")
public class PlacementProductDataSet {

    public PlacementProductDataSet() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @SerializedName("placement_id")
    @Column(name = "placement_id")
    private Integer placementId;

    @SerializedName("product_id")
    @Column(name = "product_id")
    private Integer productId;

    @SerializedName("created_at")
    @Column(name = "created_at")
    private Long createdAt;

    @SerializedName("updated_at")
    @Column(name = "updated_at")
    private Long updatedAt;

    public PlacementProductDataSet(Integer placementId, Integer productId, Long createdAt, Long updatedAt) {
        this.placementId = placementId;
        this.productId = productId;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPlacementId() {
        return placementId;
    }

    public void setPlacementId(Integer placementId) {
        this.placementId = placementId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }
}
