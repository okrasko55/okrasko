package com.itech.http.httprequeststesting.DB;

import com.itech.http.httprequeststesting.DB.DbSessionFactory;
import com.itech.http.httprequeststesting.DB.tables.MallsDataSet;

import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

/**
 * Created by Igor on 28.02.2017.
 */

public class DataAccessObject {

    private static Transaction tx;
    private static Session session;

    public static <T> void addObject(T object) {
        session = DbSessionFactory.getSessionFactory().openSession();
        tx = session.beginTransaction();
        session.save(object);
        tx.commit();
        session.close();
    }

    public static <T> void update(T object) {
        session = DbSessionFactory.getSessionFactory().openSession();
        tx = session.beginTransaction();
        session.update(object);
        tx.commit();
        session.close();
    }

    public static <T> void delete(T object) {
        session = DbSessionFactory.getSessionFactory().openSession();
        tx = session.beginTransaction();
        session.delete(object);
        tx.commit();
        session.close();
    }

    public static <T> void deleteList(List<T> objects) {
        session = DbSessionFactory.getSessionFactory().openSession();
        tx = session.beginTransaction();
        for (T object : objects) {
            session.delete(object);
        }
        tx.commit();
        session.close();
    }

    public static <T> Object get(Class<T> tClass, Integer id) {
        session = DbSessionFactory.getSessionFactory().openSession();
        Object dataSet = session.get(tClass, id);
        tx = session.beginTransaction();
        session.save(dataSet);
        tx.commit();
        session.close();
        return dataSet;
    }

    public static <T> List<Integer> getByField(Class<T> tClass, String fieldName, String value)  {
        session = DbSessionFactory.getSessionFactory().openSession();
        String SQL_QUERY = "select table.id from " + tClass.getName() + " table where " + fieldName + " = " + value;
        //String SQL_QUERY = "select cds.id from ChildDataSet cds where parent_id = 9";
        return  (List<Integer>) session.createQuery(SQL_QUERY).list();
    }

}
