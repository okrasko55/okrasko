package com.itech.http.httprequeststesting.DB.tables;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Igor on 15.03.2017.
 */

@Entity
@Table(name = "shop")
public class FacilityDataSet extends Facility {
    public FacilityDataSet() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @SerializedName("mall_id")
    @Column(name = "mall_id")
    private Integer mallId;



    @SerializedName("created_at")
    @Column(name = "created_at")
    private Long createdAt;

    @SerializedName("updated_at")
    @Column(name = "updated_at")
    private Long updatedAt;

    @Column(name = "walls")
    private String walls;

    @Column(name = "level")
    private Integer level;

    @Column(name = "image")
    private String image;

    @Column(name = "vertexes")
    private String vertexes;

    @Column(name = "color")
    private String color;

    @SerializedName("map_image")
    @Column(name = "map_image")
    private String mapImage;

    @Column(name = "width")
    private Integer width;

    @Column(name = "height")
    private Integer height;

    @SerializedName("map_offset_x")
    @Column(name = "map_offset_x")
    private Integer mapOffsetX;

    @SerializedName("map_offset_y")
    @Column(name = "map_offset_y")
    private Integer mapOffsetY;

    @SerializedName("wall_size")
    @Column(name = "wall_size")
    private Float wallSize;

    @SerializedName("deleted_at")
    @Column(name = "deleted_at")
    private Long deletedAt;

    @SerializedName("px_walls")
    @Column(name = "px_walls")
    private String pxWalls;

    public FacilityDataSet(String name, String description, Integer mallId, Long createdAt, Long updatedAt,
                           String walls, Integer level, String image, String vertexes, String color,
                           String mapImage, Integer width, Integer height, Integer mapOffsetX,
                           Integer mapOffsetY, Float wallSize, Long deletedAt, String pxWalls) {
        this.name = name;
        this.description = description;
        this.mallId = mallId;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.walls = walls;
        this.level = level;
        this.image = image;
        this.vertexes = vertexes;
        this.color = color;
        this.mapImage = mapImage;
        this.width = width;
        this.height = height;
        this.mapOffsetX = mapOffsetX;
        this.mapOffsetY = mapOffsetY;
        this.wallSize = wallSize;
        this.deletedAt = deletedAt;
        this.pxWalls = pxWalls;

        setCategoriesList(new ArrayList<>());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getMallId() {
        return mallId;
    }

    public void setMallId(Integer mallId) {
        this.mallId = mallId;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getWalls() {
        return walls;
    }

    public void setWalls(String walls) {
        this.walls = walls;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getVertexes() {
        return vertexes;
    }

    public void setVertexes(String vertexes) {
        this.vertexes = vertexes;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMapImage() {
        return mapImage;
    }

    public void setMapImage(String map_Image) {
        this.mapImage = map_Image;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getMapOffsetX() {
        return mapOffsetX;
    }

    public void setMapOffsetX(Integer mapOffsetX) {
        this.mapOffsetX = mapOffsetX;
    }

    public Integer getMapOffsetY() {
        return mapOffsetY;
    }

    public void setMapOffsetY(Integer mapOffsetY) {
        this.mapOffsetY = mapOffsetY;
    }

    public Float getWallSize() {
        return wallSize;
    }

    public void setWallSize(Float wallSize) {
        this.wallSize = wallSize;
    }

    public Long getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Long deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getPxWalls() {
        return pxWalls;
    }

    public void setPxWalls(String pxWalls) {
        this.pxWalls = pxWalls;
    }
}
