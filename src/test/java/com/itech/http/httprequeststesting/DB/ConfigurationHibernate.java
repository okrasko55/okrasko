package com.itech.http.httprequeststesting.DB;

import com.itech.http.httprequeststesting.DB.tables.AdminDataSet;
import com.itech.http.httprequeststesting.DB.tables.CategoryDataSet;
import com.itech.http.httprequeststesting.DB.tables.CategoryTypeDataSet;
import com.itech.http.httprequeststesting.DB.tables.ChildDataSet;
import com.itech.http.httprequeststesting.DB.tables.FacilityCategoryDataSet;
import com.itech.http.httprequeststesting.DB.tables.FacilityDataSet;
import com.itech.http.httprequeststesting.DB.tables.FloorDataSet;
import com.itech.http.httprequeststesting.DB.tables.InvitationsDataSet;
import com.itech.http.httprequeststesting.DB.tables.MallsDataSet;
import com.itech.http.httprequeststesting.DB.tables.PlacementCategoryDataSet;
import com.itech.http.httprequeststesting.DB.tables.PlacementDataSet;
import com.itech.http.httprequeststesting.DB.tables.PlacementProductDataSet;
import com.itech.http.httprequeststesting.DB.tables.ProductCategoryDataSet;
import com.itech.http.httprequeststesting.DB.tables.ProductDataSet;
import com.itech.http.httprequeststesting.DB.tables.ProductFacilityDataSet;
import com.itech.http.httprequeststesting.DB.tables.TokenDataSet;
import com.itech.http.httprequeststesting.DB.tables.TypeDataSet;
import com.itech.http.httprequeststesting.DB.tables.UserDataSet;

import org.hibernate.cfg.Configuration;

/**
 * Created by Igor on 17.10.2016.
 */

public class ConfigurationHibernate {
    private Configuration configuration;

    public ConfigurationHibernate() {
        configuration = new Configuration();
        configuration.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
        configuration.setProperty("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
        configuration.setProperty("hibernate.connection.url", "jdbc:mysql://dev-server-db-smartnav.ctu3suawihof.eu-central-1.rds.amazonaws.com:3306/mall_dev");
        configuration.setProperty("hibernate.connection.username", "root");
        configuration.setProperty("hibernate.connection.password", "Neimcot7OtDud");
        configuration.setProperty("hibernate.show_sql", "true");
        configuration.setProperty("hibernate.hbm2ddl.auto", "update");
        configuration.addAnnotatedClass(InvitationsDataSet.class);
        configuration.addAnnotatedClass(ChildDataSet.class);
        configuration.addAnnotatedClass(MallsDataSet.class);
        configuration.addAnnotatedClass(AdminDataSet.class);
        configuration.addAnnotatedClass(UserDataSet.class);
        configuration.addAnnotatedClass(FloorDataSet.class);
        configuration.addAnnotatedClass(TypeDataSet.class);
        configuration.addAnnotatedClass(CategoryDataSet.class);
        configuration.addAnnotatedClass(CategoryTypeDataSet.class);
        configuration.addAnnotatedClass(FacilityDataSet.class);
        configuration.addAnnotatedClass(TokenDataSet.class);
        configuration.addAnnotatedClass(PlacementDataSet.class);
        configuration.addAnnotatedClass(ProductDataSet.class);
        configuration.addAnnotatedClass(PlacementProductDataSet.class);
        configuration.addAnnotatedClass(PlacementCategoryDataSet.class);
        configuration.addAnnotatedClass(ProductCategoryDataSet.class);
        configuration.addAnnotatedClass(ProductFacilityDataSet.class);
        configuration.addAnnotatedClass(FacilityCategoryDataSet.class);
    }

    public Configuration getConfiguration() {
        return configuration;
    }
}

