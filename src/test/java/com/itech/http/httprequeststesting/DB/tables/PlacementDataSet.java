package com.itech.http.httprequeststesting.DB.tables;

import com.google.gson.annotations.SerializedName;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by inevzorov on 3/24/17.
 */

@Entity
@Table(name = "placement")

public class PlacementDataSet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @SerializedName("shop_id")
    @Column(name = "shop_id")
    private Integer shopId;

    @Column(name = "name")
    private String name;

    @Column(name = "walls")
    private String walls;

    @SerializedName("px_walls")
    @Column(name = "px_walls")
    private String pxWalls;

    @SerializedName("created_at")
    @Column(name = "created_at")
    private Long createdAt;

    @SerializedName("updated_at")
    @Column(name = "updated_at")
    private Long updatedAt;

    @Column(name = "color")
    private String color;

    @SerializedName("deleted_at")
    @Column(name = "deleted_at")
    private Long deletedAt;

    @SerializedName("px_wall_height")
    @Column(name = "px_wall_height")
    private Float pxWallHeight;

    @Column(name = "x")
    private Integer x;

    @Column(name = "y")
    private Integer y;

    public PlacementDataSet(Integer shopId, String name, String walls, String pxWalls, Long createdAt, Long updatedAt, String color, Long deletedAt, Float pxWallHeight, Integer x, Integer y) {
        this.shopId = shopId;
        this.name = name;
        this.walls = walls;
        this.pxWalls = pxWalls;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.color = color;
        this.deletedAt = deletedAt;
        this.pxWallHeight = pxWallHeight;
        this.x = x;
        this.y = y;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWalls() {
        return walls;
    }

    public void setWalls(String walls) {
        this.walls = walls;
    }

    public String getPxWalls() {
        return pxWalls;
    }

    public void setPxWalls(String pxWalls) {
        this.pxWalls = pxWalls;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Long getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Long deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Float getPxWallHeight() {
        return pxWallHeight;
    }

    public void setPxWallHeight(Float pxWallHeight) {
        this.pxWallHeight = pxWallHeight;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }
}
