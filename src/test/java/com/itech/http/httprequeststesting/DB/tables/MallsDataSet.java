package com.itech.http.httprequeststesting.DB.tables;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Igor on 07.11.2016.
 */
@Entity
@Table(name = "mall")
public class MallsDataSet extends  Venue {

    public MallsDataSet() {
    }

    public void setId(int id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @SerializedName("name")
    @Column(name = "name")
    private String mallName;


    @Column(name = "description")
    private String description;

    @SerializedName("created_at")
    @Column(name = "created_at")
    private Long createdAt;

    @SerializedName("updated_at")
    @Column(name = "updated_at")
    private Long updatedAt;

    @SerializedName("deleted_at")
    @Column(name = "deleted_at")
    private Long deletedAt;

    @SerializedName("indoors_api_key")
    @Column(name = "indoors_api_key")
    private String indoorsApiKey;

    @SerializedName("indoors_building_id")
    @Column(name = "indoors_building_id")
    private String indoorsBuildingId;

    @Column(name = "longitude")
    private Double longitude;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "image")
    private String image;

    @SerializedName("indooratlas_api_key")
    @Column(name = "indooratlas_api_key")
    private String indoorAtlasApiKey;

    @SerializedName("indooratlas_secret")
    @Column(name = "indooratlas_secret")
    private String indoorAtlasSecret;

    @SerializedName("indooratlas_venue_id")
    @Column(name = "indooratlas_venue_id")
    private String indoorAtlasVenueId;

    @SerializedName("gps_corners")
    @Column(name = "gps_corners")
    private String gpsCorners;

    @SerializedName("area_id")
    @Column(name = "area_id")
    private Integer areaId;

    @Column(name = "access")
    private Integer access;

    @SerializedName("access_updated")
    @Column(name = "access_updated")
    private Long accessUpdated;

    @SerializedName("subscription_id")
    @Column(name = "subscription_id")
    private Integer subscriptionId;



    public int getId() {
        return id;
    }

    public String getMallName() {
        return mallName;
    }

    public void setMallName(String mallName) {
        this.mallName = mallName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Long getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Long deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getIndoorsApiKey() {
        return indoorsApiKey;
    }

    public void setIndoorsApiKey(String indoorsApiKey) {
        this.indoorsApiKey = indoorsApiKey;
    }

    public String getIndoorsBuildingId() {
        return indoorsBuildingId;
    }

    public void setIndoorsBuildingId(String indoorsBuildingId) {
        this.indoorsBuildingId = indoorsBuildingId;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIndoorAtlasApiKey() {
        return indoorAtlasApiKey;
    }

    public void setIndoorAtlasApiKey(String indoorAtlasApiKey) {
        this.indoorAtlasApiKey = indoorAtlasApiKey;
    }

    public String getIndoorAtlasSecret() {
        return indoorAtlasSecret;
    }

    public void setIndoorAtlasSecret(String indoorAtlasSecret) {
        this.indoorAtlasSecret = indoorAtlasSecret;
    }

    public String getIndoorAtlasVenueId() {
        return indoorAtlasVenueId;
    }

    public void setIndoorAtlasVenueId(String indoorAtlasVenueId) {
        this.indoorAtlasVenueId = indoorAtlasVenueId;
    }

    public String getGpsCorners() {
        return gpsCorners;
    }

    public void setGpsCorners(String gpsCorners) {
        this.gpsCorners = gpsCorners;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public Integer getAccess() {
        return access;
    }

    public void setAccess(Integer access) {
        this.access = access;
    }

    public Long getAccessUpdated() {
        return accessUpdated;
    }

    public void setAccessUpdated(Long accessUpdated) {
        this.accessUpdated = accessUpdated;
    }

    public Integer getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(Integer subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public MallsDataSet(String mallName, String description, Long createdAt, Long updatedAt,
                        String indoorsApiKey, String indoorsBuildingId, Double longitude,
                        Double latitude, String image, String indoorAtlasApiKey, String indoorAtlasSecret,
                        String indoorAtlasVenueId, String gpsCorners, Long deletedAt, Integer areaId, Integer access,
                        Long accessUpdated, Integer subscriptionId) {

        this.mallName = mallName;
        this.description = description;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.deletedAt = deletedAt;
        this.indoorsApiKey = indoorsApiKey;
        this.indoorsBuildingId = indoorsBuildingId;
        this.longitude = longitude;
        this.latitude = latitude;
        this.image = image;
        this.indoorAtlasApiKey = indoorAtlasApiKey;
        this.indoorAtlasSecret = indoorAtlasSecret;
        this.indoorAtlasVenueId = indoorAtlasVenueId;
        this.gpsCorners = gpsCorners;
        this.areaId = areaId;
        this.access = access;
        this.accessUpdated = accessUpdated;
        this.subscriptionId = subscriptionId;
    }

    /*public List<FloorDataSet> getFloorsList() {
        return super.getFloorsList();
    }

    public void setFloorsList(List<FloorDataSet> floorsList) {
        super.setFloorsList(floorsList);
    }

    public List<TypeDataSet> getTypesList() {
        return super.getTypesList();
    }

    public void setTypesList(List<TypeDataSet> typesList) {
        super.setTypesList(typesList);
    }

    public List<CategoryDataSet> getCategoriesList() {
        return super.getCategoriesList();
    }

    public void setCategoriesList(List<CategoryDataSet> categoriesList) {
        super.setCategoriesList(categoriesList);
    }

    public List<CategoryTypeDataSet> getCategoriesType() {
        return super.getCategoriesType();
    }

    public void setCategoriesType(List<CategoryTypeDataSet> categoriesType) {
        super.setCategoriesType(categoriesType);
    }

    public List<FacilityDataSet> getFacilitiesList() {
        return super.facilitiesList;
    }

    public void setFacilitiesList(List<FacilityDataSet> facilitiesList) {
        this.facilitiesList = facilitiesList;
    }*/
}
