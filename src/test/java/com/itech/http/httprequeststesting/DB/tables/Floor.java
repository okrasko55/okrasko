package com.itech.http.httprequeststesting.DB.tables;

import java.util.List;

/**
 * Created by Igor on 10.04.2017.
 */

public abstract class Floor {
    private List<FacilityDataSet> facilitiesList;

    public List<FacilityDataSet> getFacilitiesList() {
        return facilitiesList;
    }

    public void setFacilitiesList(List<FacilityDataSet> facilitiesList) {
        this.facilitiesList = facilitiesList;
    }
}
