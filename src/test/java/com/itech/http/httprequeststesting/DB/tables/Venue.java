package com.itech.http.httprequeststesting.DB.tables;

import java.util.List;

/**
 * Created by Igor on 07.04.2017.
 */

public abstract class Venue {
    private List<FloorDataSet> floorsList;

    private List<TypeDataSet> typesList;

    private List<CategoryDataSet> categoriesList;

    private List<FacilityDataSet> facilitiesList;

    private List<ProductDataSet> productsList;

    public List<FacilityDataSet> getFacilitiesList() {
        return facilitiesList;
    }

    public void setFacilitiesList(List<FacilityDataSet> facilitiesList) {
        this.facilitiesList = facilitiesList;
    }

    public List<FloorDataSet> getFloorsList() {
        return floorsList;
    }

    public void setFloorsList(List<FloorDataSet> floorsList) {
        this.floorsList = floorsList;
    }

    public List<TypeDataSet> getTypesList() {
        return typesList;
    }

    public void setTypesList(List<TypeDataSet> typesList) {
        this.typesList = typesList;
    }


    public List<ProductDataSet> getProductsList() {
        return productsList;
    }

    public void setProductsList(List<ProductDataSet> productsList) {
        this.productsList = productsList;
    }

    public List<CategoryDataSet> getCategoriesList() {
        return categoriesList;
    }

    public void setCategoriesList(List<CategoryDataSet> categoriesList) {
        this.categoriesList = categoriesList;
    }
}
