package com.itech.http.httprequeststesting.DB.tables;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Alyona on 14.11.2016.
 */

@Entity
@Table(name = "child")
public class ChildDataSet {
    public ChildDataSet() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "parent_id")
    private Integer parentId;

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @Column(name = "created_at")
    private long createdAt;

    @Column(name = "updated_at")
    private long updatedAt;

    @Column(name = "photo")
    private String photo;

    @Column(name = "deleted_at")
    private String deletedAt;

    @Column(name = "x")
    private Integer x;

    @Column(name = "y")
    private Integer y;

    @Column(name = "z")
    private Integer z;

    @Column(name = "gps_latitude")
    private String gpsLatitude;

    @Column(name = "gps_longitude")
    private String gpsLongitude;

    @Column(name = "in_mall")
    private Integer inMall;

    @Column(name = "in_mall_time")
    private Long inMallTime;

    @Column(name = "photo_landscape")
    private String photoLandscape;

    @Column(name = "token")
    private String token;

    public ChildDataSet(Integer parentId, String name, String code, long createdAt, long updatedAt, Integer inMall , String token) {

        this.parentId = parentId;
        this.name = name;
        this.code = code;
        this.inMall = inMall;
        this.createdAt = createdAt;//System.currentTimeMillis();
        this.updatedAt = updatedAt;//System.currentTimeMillis();
        this.token = token;
        /* long createdAt, long updatedAt,
                        String deletedAt, Integer x, Integer y, Integer z, String token, */
    /*    this.createdAt = createdAt;//System.currentTimeMillis();
        this.updatedAt = updatedAt;//System.currentTimeMillis();
        this.deletedAt = deletedAt;
        this.x = x;
        this.y = y;
        this.z = z; */



    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public Integer getZ() {
        return z;
    }

    public void setZ(Integer z) {
        this.z = z;
    }

    public String getGpsLatitude() {
        return gpsLatitude;
    }

    public void setGpsLatitude(String gpsLatitude) {
        this.gpsLatitude = gpsLatitude;
    }

    public String getGpsLongitude() {
        return gpsLongitude;
    }

    public void setGpsLongitude(String gpsLongitude) {
        this.gpsLongitude = gpsLongitude;
    }

    public Integer getInMall() {
        return inMall;
    }

    public void setInMall(Integer inMall) {
        this.inMall = inMall;
    }

    public Long getInMallTime() {
        return inMallTime;
    }

    public void setInMallTime(Long inMallTime) {
        this.inMallTime = inMallTime;
    }

    public String getPhotoLandscape() {
        return photoLandscape;
    }

    public void setPhotoLandscape(String photoLandscape) {
        this.photoLandscape = photoLandscape;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


}
