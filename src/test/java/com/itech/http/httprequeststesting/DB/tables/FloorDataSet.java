package com.itech.http.httprequeststesting.DB.tables;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Администратор on 13.03.2017.
 */
@Entity
@Table(name = "floor")
public class FloorDataSet extends Floor{
    public FloorDataSet() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @SerializedName("level")
    @Column(name = "level")
    private Integer floorLevel;

    @Column(name = "width")
    private Integer width;

    @Column(name = "height")
    private Integer height;

    @SerializedName("image_name")
    @Column(name = "image_name")
    private String imageName;

    @SerializedName("mall_id")
    @Column(name = "mall_id")
    private Integer mallId;

    @Column(name = "type")
    private String type;

    @SerializedName("created_at")
    @Column(name = "created_at")
    private Long createdAt;

    @SerializedName("updated_at")
    @Column(name = "updated_at")
    private Long updatedAt;

    @SerializedName("mmwidth")
    @Column(name = "mmwidth")
    private Integer mmWidth;

    @SerializedName("mmheight")
    @Column(name = "mmheight")
    private Integer mmHeight;

    @Column(name = "walls")
    private String walls;

    @SerializedName("step_width")
    @Column(name = "step_width")
    private Integer stepWidth;

    @SerializedName("step_height")
    @Column(name = "step_height")
    private Integer stepHeight;

    @SerializedName("indooratlas_floorplanid")
    @Column(name = "indooratlas_floorplanid")
    private String indooratlasFloorplanId;

    @SerializedName("indooratlas_floorid")
    @Column(name = "indooratlas_floorid")
    private String indooratlasFloorId;

    @SerializedName("road_data")
    @Column(name = "road_data")
    private String roadData;

    @Column(name = "road")
    private String road;

    @SerializedName("deleted_at")
    @Column(name = "deleted_at")
    private Long deletedAt;

    @Column(name = "perimeter")
    private String perimeter;

    @SerializedName("statistic_image")
    @Column(name = "statistic_image")
    private String statisticImage;

    @SerializedName("px_perimeter")
    @Column(name = "px_perimeter")
    private String pxPerimeter;

    @SerializedName("px_walls")
    @Column(name = "px_walls")
    private String pxWalls;

    @SerializedName("angle_to_north")
    @Column(name = "angle_to_north")
    private Double angleToNorth;

    @Column(name = "doors")
    private String doors;

    @Column(name = "exits")
    private String exits;

    @SerializedName("px_wall_height")
    @Column(name = "px_wall_height")
    private Integer pxWallHeight;

    @SerializedName("mm_wall_height")
    @Column(name = "mm_wall_height")
    private Integer mmWallHeight;



    public FloorDataSet(Integer floorLevel, Integer width, Integer height, String imageName,
                        Integer mallId, String type, Long createdAt, Long updatedAt,
                        Integer mmWidth, Integer mmHeight, String walls, Integer stepWidth,
                        Integer stepHeight, String indooratlasFloorplanId, String indooratlasFloorId,
                        String roadData, String road, Long deletedAt, String perimeter,
                        String statisticImage, String pxPerimeter, String pxWalls, Double angleToNorth,
                        String doors, String exits, Integer pxWallHeight, Integer mmWallHeight) {
        this.floorLevel = floorLevel;
        this.width = width;
        this.height = height;
        this.imageName = imageName;
        this.mallId = mallId;
        this.type = type;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.mmWidth = mmWidth;
        this.mmHeight = mmHeight;
        this.walls = walls;
        this.stepWidth = stepWidth;
        this.stepHeight = stepHeight;
        this.indooratlasFloorplanId = indooratlasFloorplanId;
        this.indooratlasFloorId = indooratlasFloorId;
        this.roadData = roadData;
        this.road = road;
        this.deletedAt = deletedAt;
        this.perimeter = perimeter;
        this.statisticImage = statisticImage;
        this.pxPerimeter = pxPerimeter;
        this.pxWalls = pxWalls;
        this.angleToNorth = angleToNorth;
        this.doors = doors;
        this.exits = exits;
        this.pxWallHeight = pxWallHeight;
        this.mmWallHeight = mmWallHeight;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFloorLevel() {
        return floorLevel;
    }

    public void setFloorLevel(Integer floorLevel) {
        this.floorLevel = floorLevel;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public Integer getMallId() {
        return mallId;
    }

    public void setMallId(Integer mallId) {
        this.mallId = mallId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getMmWidth() {
        return mmWidth;
    }

    public void setMmWidth(Integer mmWidth) {
        this.mmWidth = mmWidth;
    }

    public Integer getMmHeight() {
        return mmHeight;
    }

    public void setMmHeight(Integer mmHeight) {
        this.mmHeight = mmHeight;
    }

    public String getWalls() {
        return walls;
    }

    public void setWalls(String walls) {
        this.walls = walls;
    }

    public Integer getStepWidth() {
        return stepWidth;
    }

    public void setStepWidth(Integer stepWidth) {
        this.stepWidth = stepWidth;
    }

    public Integer getStepHeight() {
        return stepHeight;
    }

    public void setStepHeight(Integer stepHeight) {
        this.stepHeight = stepHeight;
    }

    public String getIndooratlasFloorplanId() {
        return indooratlasFloorplanId;
    }

    public void setIndooratlasFloorplanId(String indooratlasFloorplanId) {
        this.indooratlasFloorplanId = indooratlasFloorplanId;
    }

    public String getIndooratlasFloorId() {
        return indooratlasFloorId;
    }

    public void setIndooratlasFloorId(String indooratlasFloorId) {
        this.indooratlasFloorId = indooratlasFloorId;
    }

    public String getRoadData() {
        return roadData;
    }

    public void setRoadData(String roadData) {
        this.roadData = roadData;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public Long getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Long deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getPerimeter() {
        return perimeter;
    }

    public void setPerimeter(String perimeter) {
        this.perimeter = perimeter;
    }

    public String getStatisticImage() {
        return statisticImage;
    }

    public void setStatisticImage(String statisticImage) {
        this.statisticImage = statisticImage;
    }

    public String getPxPerimeter() {
        return pxPerimeter;
    }

    public void setPxPerimeter(String pxPerimeter) {
        this.pxPerimeter = pxPerimeter;
    }

    public String getPxWalls() {
        return pxWalls;
    }

    public void setPxWalls(String pxWalls) {
        this.pxWalls = pxWalls;
    }

    public Double getAngleToNorth() {
        return angleToNorth;
    }

    public void setAngleToNorth(Double angleToNorth) {
        this.angleToNorth = angleToNorth;
    }

    public String getDoors() {
        return doors;
    }

    public void setDoors(String doors) {
        this.doors = doors;
    }

    public String getExits() {
        return exits;
    }

    public void setExits(String exits) {
        this.exits = exits;
    }

    public Integer getPxWallHeight() {
        return pxWallHeight;
    }

    public void setPxWallHeight(Integer pxWallHeight) {
        this.pxWallHeight = pxWallHeight;
    }

    public Integer getMmWallHeight() {
        return mmWallHeight;
    }

    public void setMmWallHeight(Integer mmWallHeight) {
        this.mmWallHeight = mmWallHeight;
    }
}
