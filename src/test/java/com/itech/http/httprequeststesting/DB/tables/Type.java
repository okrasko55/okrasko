package com.itech.http.httprequeststesting.DB.tables;

import java.util.List;

/**
 * Created by Igor on 10.04.2017.
 */

public abstract class Type {
    private List<CategoryDataSet> categoriesList;

    public List<CategoryDataSet> getCategoriesList() {
        return categoriesList;
    }

    public void setCategoriesList(List<CategoryDataSet> categoriesList) {
        this.categoriesList = categoriesList;
    }
}
