package com.itech.http.httprequeststesting.DB.tables;

import com.google.gson.annotations.SerializedName;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Igor on 14.03.2017.
 */

@Entity
@Table(name = "category")
public class CategoryDataSet {
    public CategoryDataSet() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @SerializedName("created_at")
    @Column(name = "created_at")
    private Long createdAt;

    @SerializedName("updated_at")
    @Column(name = "updated_at")
    private Long updatedAt;

    @Column(name = "image")
    private String image;

    @SerializedName("mall_id")
    @Column(name = "mall_id")
    private Integer mallId;

    @SerializedName("shop_id")
    @Column(name = "shop_id")
    private String shopId;

    @Column(name = "status")
    private String status;

    @SerializedName("deleted_at")
    @Column(name = "deleted_at")
    private Long deletedAt;

    @SerializedName("copy_id")
    @Column(name = "copy_id")
    private Integer copyId;

    public CategoryDataSet(String name, String description, Long createdAt, Long updatedAt, String image,
                           Integer mallId, String shopId, String status, Long deletedAt, Integer copyId) {
        this.name = name;
        this.description = description;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.image = image;
        this.mallId = mallId;
        this.shopId = shopId;
        this.status = status;
        this.deletedAt = deletedAt;
        this.copyId = copyId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getMallId() {
        return mallId;
    }

    public void setMallId(Integer mallId) {
        this.mallId = mallId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Long deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Integer getCopyId() {
        return copyId;
    }

    public void setCopyId(Integer copyId) {
        this.copyId = copyId;
    }
}
