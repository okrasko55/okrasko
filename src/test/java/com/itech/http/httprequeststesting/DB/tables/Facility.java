package com.itech.http.httprequeststesting.DB.tables;

import java.util.List;

/**
 * Created by Igor on 10.04.2017.
 */

public abstract class Facility {
    private List<CategoryDataSet> categoriesList;

    private List<PlacementDataSet> placementsList;

    private List<ProductDataSet> productsList;

    public List<CategoryDataSet> getCategoriesList() {
        return categoriesList;
    }

    public void setCategoriesList(List<CategoryDataSet> categoriesList) {
        this.categoriesList = categoriesList;
    }

    public List<PlacementDataSet> getPlacementsList() {
        return placementsList;
    }

    public void setPlacementsList(List<PlacementDataSet> placementsList) {
        this.placementsList = placementsList;
    }

    public List<ProductDataSet> getProductsList() {
        return productsList;
    }

    public void setProductsList(List<ProductDataSet> productsList) {
        this.productsList = productsList;
    }
}
