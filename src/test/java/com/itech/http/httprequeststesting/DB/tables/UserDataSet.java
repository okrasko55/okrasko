package com.itech.http.httprequeststesting.DB.tables;

import com.google.gson.annotations.SerializedName;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Igor on 02.03.2017.
 */
@Entity
@Table(name = "user")
public class UserDataSet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @SerializedName("first_name")
    @Column(name = "first_name")
    private String firstName;

    @SerializedName("last_name")
    @Column(name = "last_name")
    private String lastName;

    @Column(name = "auth_key")
    private String auth_key;

    @Column(name = "phone")
    private String phone;

    @Column(name = "role")
    private Integer role;

    @SerializedName("password_hash")
    @Column(name = "password_hash")
    private String passwordHash;

    @SerializedName("password_reset_token")
    @Column(name = "password_reset_token")
    private String passwordResetToken;

    @Column(name = "email")
    private String email;

    @Column(name = "status")
    private String status;

    @SerializedName("created_at")
    @Column(name = "created_at")
    private Long createdAt;

    @SerializedName("updated_at")
    @Column(name = "updated_at")
    private Long updatedUt;

    @Column(name = "photo")
    private String photo;

    @Column(name = "x")
    private String x;

    @Column(name = "y")
    private String y;

    @Column(name = "z")
    private String z;

    @SerializedName("gps_latitude")
    @Column(name = "gps_latitude")
    private Double latitude;

    @SerializedName("gps_longitude")
    @Column(name = "gps_longitude")
    private Double longitude;

    @SerializedName("in_mall")
    @Column(name = "in_mall")
    private Integer inMall;

    @SerializedName("in_mall_time")
    @Column(name = "in_mall_time")
    private Long inMallTime;

    @SerializedName("location_access")
    @Column(name = "location_access")
    private Integer locationAccess;

    @SerializedName("profile_visible")
    @Column(name = "profile_visible")
    private Integer profileVisible;

    @SerializedName("deleted_at")
    @Column(name = "deleted_at")
    private Long deletedAt;

    @SerializedName("voice_nav")
    @Column(name = "voice_nav")
    private Integer voiceNav;

    @SerializedName("photo_landscape")
    @Column(name = "photo_landscape")
    private String photoLandscape;

    @SerializedName("send_push")
    @Column(name = "send_push")
    private Integer sendPush;

    @SerializedName("device_token")
    @Column(name = "device_token")
    private String deviceToken;

    @SerializedName("device_service")
    @Column(name = "device_service")
    private String deviceService;

    @SerializedName("accept_ads")
    @Column(name = "accept_ads")
    private Integer acceptAds;

    @SerializedName("background_active")
    @Column(name = "background_active")
    private Integer backgroundActive;

    public UserDataSet() {
    }

    public UserDataSet(String firstName, String lastName, String auth_key, String phone, Integer role, String passwordHash, String passwordResetToken, String email, String status, Long createdAt, Long updatedUt, String photo, String x, String y, String z, Double latitude, Double longitude, Integer inMall, Long inMallTime, Integer locationAccess, Integer profileVisible, Long deletedAt, Integer voiceNav, String photoLandscape, Integer sendPush, String deviceToken, String deviceService, Integer acceptAds, Integer backgroundActive) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.auth_key = auth_key;
        this.phone = phone;
        this.role = role;
        this.passwordHash = passwordHash;
        this.passwordResetToken = passwordResetToken;
        this.email = email;
        this.status = status;
        this.createdAt = createdAt;
        this.updatedUt = updatedUt;
        this.photo = photo;
        this.x = x;
        this.y = y;
        this.z = z;
        this.latitude = latitude;
        this.longitude = longitude;
        this.inMall = inMall;
        this.inMallTime = inMallTime;
        this.locationAccess = locationAccess;
        this.profileVisible = profileVisible;
        this.deletedAt = deletedAt;
        this.voiceNav = voiceNav;
        this.photoLandscape = photoLandscape;
        this.sendPush = sendPush;
        this.deviceToken = deviceToken;
        this.deviceService = deviceService;
        this.acceptAds = acceptAds;
        this.backgroundActive = backgroundActive;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAuth_key() {
        return auth_key;
    }

    public void setAuth_key(String auth_key) {
        this.auth_key = auth_key;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getPasswordResetToken() {
        return passwordResetToken;
    }

    public void setPasswordResetToken(String passwordResetToken) {
        this.passwordResetToken = passwordResetToken;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedUt() {
        return updatedUt;
    }

    public void setUpdatedUt(Long updatedUt) {
        this.updatedUt = updatedUt;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public String getY() {
        return y;
    }

    public void setY(String y) {
        this.y = y;
    }

    public String getZ() {
        return z;
    }

    public void setZ(String z) {
        this.z = z;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Integer getInMall() {
        return inMall;
    }

    public void setInMall(Integer inMall) {
        this.inMall = inMall;
    }

    public Long getInMallTime() {
        return inMallTime;
    }

    public void setInMallTime(Long inMallTime) {
        this.inMallTime = inMallTime;
    }

    public Integer getLocationAccess() {
        return locationAccess;
    }

    public void setLocationAccess(Integer locationAccess) {
        this.locationAccess = locationAccess;
    }

    public Integer getProfileVisible() {
        return profileVisible;
    }

    public void setProfileVisible(Integer profileVisible) {
        this.profileVisible = profileVisible;
    }

    public Long getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Long deletedAt) {
        this.deletedAt = deletedAt;
    }

    public Integer getVoiceNav() {
        return voiceNav;
    }

    public void setVoiceNav(Integer voiceNav) {
        this.voiceNav = voiceNav;
    }

    public String getPhotoLandscape() {
        return photoLandscape;
    }

    public void setPhotoLandscape(String photoLandscape) {
        this.photoLandscape = photoLandscape;
    }

    public Integer getSendPush() {
        return sendPush;
    }

    public void setSendPush(Integer sendPush) {
        this.sendPush = sendPush;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getDeviceService() {
        return deviceService;
    }

    public void setDeviceService(String deviceService) {
        this.deviceService = deviceService;
    }

    public Integer getAcceptAds() {
        return acceptAds;
    }

    public void setAcceptAds(Integer acceptAds) {
        this.acceptAds = acceptAds;
    }

    public Integer getBackgroundActive() {
        return backgroundActive;
    }

    public void setBackgroundActive(Integer backgroundActive) {
        this.backgroundActive = backgroundActive;
    }
}
