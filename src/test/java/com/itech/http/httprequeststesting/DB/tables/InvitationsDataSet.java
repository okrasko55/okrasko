package com.itech.http.httprequeststesting.DB.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Igor on 17.10.2016.
 */

@Entity
@Table(name = "invitations")
public class InvitationsDataSet implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "from_invite_id")
    private Integer fromInviteId;

    @Column(name = "to_invite_id")
    private Integer toInviteId;

    @Column(name = "to_invite_email")
    private String toInviteEmail;

    @Column(name = "mall_id")
    private Integer mallId;

    @Column(name = "status")
    private Integer status;

    @Column(name = "created_at")
    private Long createdAt;

    @Column(name = "updated_at")
    private Long updatedAt;

    @Column(name = "deleted_at")
    private Long deletedAt;

    @Column(name = "token")
    private String token;

    public InvitationsDataSet() {
    }

    public InvitationsDataSet(Integer fromInviteId, Integer toInviteId, String toInviteEmail, Integer mallId,
                              Integer status, Long createdAt, Long updatedAt, Long deletedAt, String token) {
        this.fromInviteId = fromInviteId;
        this.toInviteId = toInviteId;
        this.toInviteEmail = toInviteEmail;
        this.mallId = mallId;
        this.status = status;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.deletedAt = deletedAt;
        this.token = token;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFromInviteId() {
        return fromInviteId;
    }

    public void setFromInviteId(Integer fromInviteId) {
        this.fromInviteId = fromInviteId;
    }

    public Integer getToInviteId() {
        return toInviteId;
    }

    public void setToInviteId(Integer toInviteId) {
        this.toInviteId = toInviteId;
    }

    public String getToInviteEmail() {
        return toInviteEmail;
    }

    public void setToInviteEmail(String toInviteEmail) {
        this.toInviteEmail = toInviteEmail;
    }

    public Integer getMallId() {
        return mallId;
    }

    public void setMallId(Integer mallId) {
        this.mallId = mallId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Long getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Long deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
