package com.itech.http.httprequeststesting.DB.tables;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Igor on 28.02.2017.
 */
@Entity
@Table(name = "admin")
public class AdminDataSet implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column (name = "user_id")
    private Integer userId;

    @Column (name = "type")
    private String type;

    @Column (name = "mall_id")
    private Integer mallId;

    @Column (name = "shop_id")
    private Integer shopId;

    @Column (name = "created_at")
    private Long createdAt;

    @Column (name = "updated_at")
    private Long updatedAt;

    @Column (name = "deleted_at")
    private Long deletedAt;

    public AdminDataSet() {
    }

    public AdminDataSet(Integer userId, String type, Integer mallId, Integer shopId, Long createdAt, Long updatedAt, Long deletedAt) {

        this.userId = userId;
        this.type = type;
        this.mallId = mallId;
        this.shopId = shopId;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.deletedAt = deletedAt;
    }

    public Integer getId() {
        return id;
    }

    public Integer getUserId() {
        return userId;
    }

    public String getType() {
        return type;
    }

    public Integer getMallId() {
        return mallId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public Long getDeletedAt() {
        return deletedAt;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setMallId(Integer mallId) {
        this.mallId = mallId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setDeletedAt(Long deletedAt) {
        this.deletedAt = deletedAt;
    }
}
