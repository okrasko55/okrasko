package com.itech.http.httprequeststesting.restapi.model.venues;



import com.google.gson.annotations.SerializedName;

import com.itech.http.httprequeststesting.restapi.model.BaseResponse;
import com.itech.http.httprequeststesting.restapi.model.venues.floor.Entry;
import com.itech.http.httprequeststesting.restapi.model.venues.floor.Floor;
import com.itech.http.httprequeststesting.restapi.model.venues.floor.Vertex;
import com.itech.http.httprequeststesting.restapi.model.venues.floor.WallModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NataliLapshina on 22.05.2015.
 */
public class VenueObject extends BaseResponse {

    public static final String NAME_FIELD = "name";

    public static final String TYPE_INDOORS = "indoors";
    public static final String TYPE_INDOORATLAS = "indooratlas";
    private String id;
    private String name;
    private String description;
    @SerializedName("lng")
    private double longitude;
    @SerializedName("lat")
    private double latitude;
    private String image;
    private String indoors_api_key;
    private String indoors_building_id;
    private String indooratlas_api_key;
    private String indooratlas_secret;
    private String indooratlas_venue_id;
    private String image_url;
    private String user_id;
    private String type;
    private String mall_id;
    private String shop_id;
    private String created_at;
    private String updated_at;
    private ArrayList<Floor> floors;
    private ArrayList<Shop> shops;
    private boolean navigation;
    private String distance; //distance not null only for nearest venue request
    private int access;
    @SerializedName("gps_corners")
    public List<VenueSyncObject.ServerLatLong> gpsCorners;

    @SerializedName("subscription_id")
    private int subscriptionId;

    public VenueObject(VenueObject venueObject) {
        this.id = venueObject.id;
        this.name = venueObject.name;
        this.description = venueObject.description;
        this.longitude = venueObject.longitude;
        this.latitude = venueObject.latitude;
        this.user_id = venueObject.user_id;
        this.type = venueObject.type;
        this.mall_id = venueObject.mall_id;
        this.shop_id = venueObject.shop_id;
        this.created_at = venueObject.created_at;
        this.updated_at = venueObject.updated_at;
        this.image = venueObject.image;
        this.indooratlas_api_key = venueObject.indooratlas_api_key;
        this.indooratlas_secret = venueObject.indooratlas_secret;
        this.indooratlas_venue_id = venueObject.indooratlas_venue_id;
        this.indoors_building_id = venueObject.indoors_building_id;
        this.indoors_api_key = venueObject.indoors_api_key;
        this.image_url = venueObject.image_url;
        this.floors = venueObject.floors;
    }


    public int getAccess() {
        return access;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMall_id() {
        return mall_id;
    }

    public void setMall_id(String mall_id) {
        this.mall_id = mall_id;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isNavigation() {
        return navigation;
    }

    public void setNavigation(boolean navigation) {
        this.navigation = navigation;
    }

    public int getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(int subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getIndoors_building_id() {
        return indoors_building_id;
    }

    public void setIndoors_building_id(String indoors_building_id) {
        this.indoors_building_id = indoors_building_id;
    }

    public String getIndoors_api_key() {
        return indoors_api_key;
    }

    public void setIndoors_api_key(String indoors_api_key) {
        this.indoors_api_key = indoors_api_key;
    }

    public String getIndooratlas_api_key() {
        return indooratlas_api_key;
    }

    public void setIndooratlas_api_key(String indooratlas_api_key) {
        this.indooratlas_api_key = indooratlas_api_key;
    }

    public String getIndooratlas_secret() {
        return indooratlas_secret;
    }

    public void setIndooratlas_secret(String indooratlas_secret) {
        this.indooratlas_secret = indooratlas_secret;
    }

    public String getIndooratlas_venue_id() {
        return indooratlas_venue_id;
    }

    public void setIndooratlas_venue_id(String indooratlas_venue_id) {
        this.indooratlas_venue_id = indooratlas_venue_id;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }


    public ArrayList<Floor> getFloors() {
        return floors;
    }

    public void setFloors(ArrayList<Floor> floors) {
        this.floors = floors;
    }

    public ArrayList<Shop> getShops() {
        return shops;
    }

    public void setShops(ArrayList<Shop> shops) {
        this.shops = shops;
    }




    public static class FloorSize {
        private long mmHeight;
        private long mmWidth;
        private long pxHeight;
        private long pxWidth;

        public FloorSize(long mmHeight, long mmWidth, long pxHeight, long pxWidth) {
            this.mmHeight = mmHeight;
            this.mmWidth = mmWidth;
            this.pxHeight = pxHeight;
            this.pxWidth = pxWidth;
        }

        public long getMmHeight() {
            return mmHeight;
        }

        public void setMmHeight(long mmHeight) {
            this.mmHeight = mmHeight;
        }

        public long getMmWidth() {
            return mmWidth;
        }

        public void setMmWidth(long mmWidth) {
            this.mmWidth = mmWidth;
        }

        public long getPxHeight() {
            return pxHeight;
        }

        public void setPxHeight(long pxHeight) {
            this.pxHeight = pxHeight;
        }

        public long getPxWidth() {
            return pxWidth;
        }

        public void setPxWidth(long pxWidth) {
            this.pxWidth = pxWidth;
        }
    }

    public static class Shop {

        private String id;
        private String name;
        private String description;
        private String mall_id;
        private String image;
        private String color;
        private String image_url;
        @SerializedName("vertexes")
        private ArrayList<Vertex> vertexes;
        private int level;

        private ArrayList<Entry> entries;
        private String map_image;
        private String width;
        private String map_offset_x;
        private String map_offset_y;
        private String height;
        private ArrayList<ProductPlacements> productPlacements;
        @SerializedName("px_walls")
        private List<WallModel> pxWalls;

        public Shop(String shopId, String shopName, String description, String image, String imageUrl, String color) {
            this.id = shopId;
            this.name = shopName;
            this.description = description;
            this.image = image;
            this.image_url = imageUrl;
            this.color = color;
        }

        public List<WallModel> getPxWalls() {
            return pxWalls;
        }

        public void setPxWalls(List<WallModel> pxWalls) {
            this.pxWalls = pxWalls;
        }

        public ArrayList<ProductPlacements> getProductPlacements() {
            return productPlacements;
        }

        public void setProductPlacements(ArrayList<ProductPlacements> productPlacements) {
            this.productPlacements = productPlacements;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getMall_id() {
            return mall_id;
        }

        public void setMall_id(String mall_id) {
            this.mall_id = mall_id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getImage_url() {
            return image_url;
        }

        public void setImage_url(String image_url) {
            this.image_url = image_url;
        }

        public ArrayList<Entry> getEntries() {
            return entries;
        }

        public void setEntries(ArrayList<Entry> entries) {
            this.entries = entries;
        }

        public ArrayList<Vertex> getVertexes() {
            return vertexes;
        }

        public void setVertexes(ArrayList<Vertex> vertexes) {
            this.vertexes = vertexes;
        }

        public int getLevel() {
            return level;
        }

        public void setLevel(int level) {
            this.level = level;
        }

        public String getMap_image() {
            return map_image;
        }

        public void setMap_image(String map_image) {
            this.map_image = map_image;
        }

        public String getWidth() {
            return width;
        }

        public void setWidth(String width) {
            this.width = width;
        }

        public String getMap_offset_x() {
            return map_offset_x;
        }

        public void setMap_offset_x(String map_offset_x) {
            this.map_offset_x = map_offset_x;
        }

        public String getMap_offset_y() {
            return map_offset_y;
        }

        public void setMap_offset_y(String map_offset_y) {
            this.map_offset_y = map_offset_y;
        }

        public String getHeight() {
            return height;
        }

        public void setHeight(String height) {
            this.height = height;
        }

    }


}
