package com.itech.http.httprequeststesting.restapi.model.child;

import com.itech.http.httprequeststesting.restapi.model.BaseResponse;

/**
 * Created by BARBIN on 07.09.2015.
 */
public class ChildObject extends BaseResponse {
    private String id;
    private String name;
    private String code;
    private String photo;
    private String photo_url;
    private String photo_landscape_url;
    private String x;
    private String y;
    private String z;
    private String lat;
    private String lng;
    private String in_mall;
    private String in_mall_time;

    public ChildObject() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public String getY() {
        return y;
    }

    public void setY(String y) {
        this.y = y;
    }

    public String getZ() {
        return z;
    }

    public void setZ(String z) {
        this.z = z;
    }

    public String getGps_latitude() {
        return lat;
    }

    public void setGps_latitude(String gps_latitude) {
        this.lat = gps_latitude;
    }

    public String getGps_longitude() {
        return lng;
    }

    public void setGps_longitude(String gps_longitude) {
        this.lng = gps_longitude;
    }

    public String getIn_mall() {
        return this.in_mall;
    }

    public void setIn_mall(String in_mall) {
        this.in_mall = in_mall;
    }

    public String getIn_mall_time() {
        return in_mall_time;
    }

    public void setIn_mall_time(String in_mall_time) {
        this.in_mall_time = in_mall_time;
    }

    public String getPhoto_landscape_url() {
        return photo_landscape_url;
    }

    public void setPhoto_landscape_url(String photo_landscape_url) {
        this.photo_landscape_url = photo_landscape_url;
    }
}
