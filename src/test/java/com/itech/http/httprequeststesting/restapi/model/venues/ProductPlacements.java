package com.itech.http.httprequeststesting.restapi.model.venues;

import com.itech.http.httprequeststesting.restapi.model.venues.products.ProductsObject;

/**
 * Created by BARBIN on 30.10.2015.
 */
public class ProductPlacements {
    private String id;
    private String shop_id;
    private String product_id;
    private String level;
    private String px;
    private String py;
    private ProductsObject product;

    public ProductPlacements() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getPx() {
        return px;
    }

    public void setPx(String x) {
        this.px = x;
    }

    public String getPy() {
        return py;
    }

    public void setPy(String y) {
        this.py = y;
    }

    public ProductsObject getProduct() {
        return product;
    }

    public void setProduct(ProductsObject product) {
        this.product = product;
    }


}
