package com.itech.http.httprequeststesting.restapi.model.venues.subscription;

import com.google.gson.annotations.SerializedName;


/**
 * Created by natalia on 11/22/16.
 */

public class SubscriptionObject {

    private String id;
    private String name;
    private String description;

    @SerializedName("feature_indoor_mapping")
    private int indoorMappingOn;

    @SerializedName("feature_maps")
    private int mapsOn;

    @SerializedName("feature_search")
    private int searchOn;

    @SerializedName("feature_outdoor_to_indoor_routing")
    private int outdoorToIndoorRoutingOn;

    @SerializedName("feature_indoor_nav_voice_distance")
    private int indoorNavVoiceOn;

    @SerializedName("feature_push_notification")
    private int pushNotificationOn;

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getIndoorMappingOn() {
        return indoorMappingOn;
    }

    public int getIndoorNavVoiceOn() {
        return indoorNavVoiceOn;
    }

    public int getMapsOn() {
        return mapsOn;
    }

    public int getSearchOn() {
        return searchOn;
    }

    public int getOutdoorToIndoorRoutingOn() {
        return outdoorToIndoorRoutingOn;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setIndoorMappingOn(int indoorMappingOn) {
        this.indoorMappingOn = indoorMappingOn;
    }

    public void setIndoorNavVoiceOn(int indoorNavVoiceOn) {
        this.indoorNavVoiceOn = indoorNavVoiceOn;
    }

    public void setMapsOn(int mapsOn) {
        this.mapsOn = mapsOn;
    }

    public void setOutdoorToIndoorRoutingOn(int outdoorToIndoorRoutingOn) {
        this.outdoorToIndoorRoutingOn = outdoorToIndoorRoutingOn;
    }

    public void setSearchOn(int searchOn) {
        this.searchOn = searchOn;
    }

    public int getPushNotificationOn() {
        return pushNotificationOn;
    }

    public void setPushNotificationOn(int pushNotificationOn) {
        this.pushNotificationOn = pushNotificationOn;
    }
}
