package com.itech.http.httprequeststesting.restapi.model.venues.facility;


import com.itech.http.httprequeststesting.restapi.model.venues.shops.spaces.SpaceSearchObject;

import java.util.List;

/**
 * Created by barbinkh on 16.03.17.
 */

public class FacilitySearchObject extends FacilityObject {
    private List<SpaceSearchObject> placements;

    public List<SpaceSearchObject> getPlacements() {
        return placements;
    }

    public void setPlacements(List<SpaceSearchObject> placements) {
        this.placements = placements;
    }
}
