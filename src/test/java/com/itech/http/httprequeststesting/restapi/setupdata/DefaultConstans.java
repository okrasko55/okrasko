package com.itech.http.httprequeststesting.restapi.setupdata;

/**
 * Created by Igor on 12.04.2017.
 */

public class DefaultConstans {

    //Floor
    public static final Integer DEFAULT_WIDTH = 800;
    public static final Integer DEFAULT_HEIGHT = 600;
    public static final Integer DEFAULT_MM_WIDTH = 20000;
    public static final Integer DEFAULT_MM_HEIGHT = 15000;
    public static final String DEFAULT_PX_PERIMETR = "[]";
    public static final String DEFAULT_PX_WALLS = "[]";
    public static final String DEFAULT_DOORS = "[]";
    public static final String DEFAULT_EXITS = "[]";
    public static final Double DEFAULT_ANGLE_TO_NORTH = Double.valueOf(-999);
    public static final Integer DEFAULT_PX_WALL_HEIGHT = 120;
    public static final Integer DEFAULT_MM_WALL_HEIGHT = 3000;


}
