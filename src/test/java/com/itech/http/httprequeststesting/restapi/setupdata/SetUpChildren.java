package com.itech.http.httprequeststesting.restapi.setupdata;

import com.google.gson.reflect.TypeToken;
import com.itech.http.httprequeststesting.DB.DataAccessObject;
import com.itech.http.httprequeststesting.DB.tables.ChildDataSet;
import com.itech.http.httprequeststesting.DB.tables.UserDataSet;
import com.itech.http.httprequeststesting.restapi.utils.JsonToJava;
import com.itech.http.httprequeststesting.restapi.utils.TimeUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by inevzorov on 3/23/17.
 */

public class SetUpChildren {
    private static ChildDataSet child;
    private static List<ChildDataSet> gotChildren;
    private static List<ChildDataSet> addedChildren;

    public static List<ChildDataSet> addChildrenToParent(UserDataSet user) throws Exception {
        Random random = new Random();
        int rand = random.nextInt(1000);
        gotChildren = JsonToJava.getData("child.json", new TypeToken<List<ChildDataSet>>() {
        }.getType());
        addedChildren = new ArrayList<>();
        for (ChildDataSet item : gotChildren) {
            child = new ChildDataSet(user.getId(), item.getName() + rand, item.getCode() + rand++,
                    TimeUtils.getBeginningTimeStamp(), TimeUtils.getCurrentTimeStamp(), item.getInMall(), item.getToken() + random.nextInt(10000));
            DataAccessObject.addObject(child);
            addedChildren.add(child);
            System.out.println("Child_ID: " + child.getId() + " - " + child.getToken());
        }
        return addedChildren;
    }

}
