package com.itech.http.httprequeststesting.restapi;


import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Igor on 23.02.2017.
 */

public class RestClient {

    public static OkHttpClient okHttpClient;

    static {

        okHttpClient = new OkHttpClient.Builder().readTimeout(30, TimeUnit.SECONDS)
                .build();

    }

    private ApiService apiService;

    public RestClient(String endpoint) {

//        mainLooperHandler = new Handler(Looper.getMainLooper());
/*

        Retrofit restAdapter = new Retrofit.Builder()
                .baseUrl(endpoint)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
*/


        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(endpoint)
                .client(okHttpClient)
                .build();
        apiService = retrofit.create(ApiService.class);
    }


    public ApiService getApiService() {
        return apiService;
    }


}