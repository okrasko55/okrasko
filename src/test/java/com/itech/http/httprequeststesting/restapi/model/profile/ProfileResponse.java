package com.itech.http.httprequeststesting.restapi.model.profile;

import com.google.gson.annotations.SerializedName;
import com.itech.http.httprequeststesting.restapi.model.BaseResponse;
import com.itech.http.httprequeststesting.restapi.model.car.CarObject;
import com.itech.http.httprequeststesting.restapi.model.child.ChildObject;
import com.itech.http.httprequeststesting.restapi.model.friends.FriendResponse;

/**
 * Created by Natalia on 21.09.2015.
 */
public class ProfileResponse extends BaseResponse {

    @SerializedName("friends")
    private BaseProfileObject<FriendResponse> friendObject;
    @SerializedName("cars")
    private BaseProfileObject<CarObject> carObject;
    @SerializedName("children")
    private BaseProfileObject<ChildObject> childrenObject;

    public BaseProfileObject<FriendResponse> getFriendObject() {
        return friendObject;
    }

    public BaseProfileObject<CarObject> getCarObject() {
        return carObject;
    }

    public BaseProfileObject<ChildObject> getChildrenObject() {
        return childrenObject;
    }
}
