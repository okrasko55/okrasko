package com.itech.http.httprequeststesting.restapi.model.venues;

import com.itech.http.httprequeststesting.restapi.model.BaseResponseSync;
import com.itech.http.httprequeststesting.restapi.model.venues.products.ProductModel;

/**
 * Created by Администратор on 11.03.2017.
 */

public class ProductSyncModelResponse extends BaseResponseSync<ProductModel> {
}
