package com.itech.http.httprequeststesting.restapi.model.venues.products;



import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by barbinkh on 03.08.2015.
 */
public class TypeModel implements Comparator<TypeModel> {

    public static final String NAME_FIELD = "name";
    public static final String DENY_STATUS = "deny";

    private String id;
    private String name;
    private String status;
    private String image_url;
    private String description;
    private ArrayList<CategoryModel> categories = new ArrayList<>();

    public TypeModel() {

    }

    public TypeModel(String id, String name, String status, String image_url, String description) {
        this.id = id;
        this.name = name;
        this.image_url = image_url;
        this.status = status;
        this.description = description;
    }


    public ArrayList<CategoryModel> getCategoryList() {
        return categories;
    }

    public void setCategoryList(ArrayList<CategoryModel> categories) {
        this.categories = categories;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int compare(TypeModel lhs, TypeModel rhs) {
        return lhs.getName().compareTo(rhs.getName());
    }

    @Override
    public boolean equals(Object aThat) {
        if (!(aThat instanceof TypeModel)) {
            return false;
        }

        TypeModel that = (TypeModel) aThat;

        return
                false;//EqualsUtil.areEqual(this.id, that.id);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }


}
