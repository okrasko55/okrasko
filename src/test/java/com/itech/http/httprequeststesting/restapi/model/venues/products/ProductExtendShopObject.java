package com.itech.http.httprequeststesting.restapi.model.venues.products;


import com.google.gson.annotations.SerializedName;
import com.itech.http.httprequeststesting.restapi.model.venues.shops.ShopExtendPlacementObject;

import java.util.List;

/**
 * Created by Natalia on 02.11.2015.
 */
public class ProductExtendShopObject {

    private String id;
    private String name;
    private String description;
    private String image_url;
    private float px;
    private float py;
    private long level;
    @SerializedName("mall_id")
    private String venueId;
    private String shopId;
    @SerializedName("shops")
    private List<ShopExtendPlacementObject> shopExtendPlacementObject;

    public ProductExtendShopObject(String id, String name, String description, String image_url, float px, float py, long level, String venueId, String shopId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.image_url = image_url;
        this.px = px;
        this.py = py;
        this.level = level;
        this.venueId = venueId;
        this.shopId = shopId;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getImage_url() {
        return image_url;
    }

    public long getLevel() {
        return level;
    }

    public float getPx() {
        return px;
    }

    public float getPy() {
        return py;
    }

    public String getShopId() {
        return shopId;
    }

    public String getVenueId() {
        return venueId;
    }

    public List<ShopExtendPlacementObject> getShopExtendPlacementObject() {
        return shopExtendPlacementObject;
    }
}
