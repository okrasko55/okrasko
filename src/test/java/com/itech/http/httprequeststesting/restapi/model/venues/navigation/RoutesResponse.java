package com.itech.http.httprequeststesting.restapi.model.venues.navigation;

import com.itech.http.httprequeststesting.restapi.model.BaseResponse;

import java.util.List;

/**
 * Created by on 29.07.2015.
 */
public class RoutesResponse extends BaseResponse {

    private int id;

    //List of root steps
    private List<RouteStepObject> points;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<RouteStepObject> getPoints() {
        return points;
    }

    public void setPoints(List<RouteStepObject> points) {
        this.points = points;
    }

}


