package com.itech.http.httprequeststesting.restapi.model.venues.products;



/**
 * Created by barbinkh on 03.08.2015.
 */
public class ProductModel {
    public static final String NAME_FIELD = "name";
    public static final String DENY_STATUS = "deny";

    private String id;
    private String name;
    private String description;
    private String sku;
    private String status;
    private String image_url;
    private String facilityId;

    public ProductModel() {

    }

    public ProductModel(String id, String name, String description, String sku, String status, String image_url, String facilityId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.sku = sku;
        this.status = status;
        this.image_url = image_url;
        this.facilityId = facilityId;
    }

    public String getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(String facilityId) {
        this.facilityId = facilityId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    @Override
    public boolean equals(Object aThat) {
        if (!(aThat instanceof ProductModel)) {
            return false;
        }

        ProductModel that = (ProductModel) aThat;

        return
                false; //EqualsUtil.areEqual(this.id, that.id);
    }


    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

}
