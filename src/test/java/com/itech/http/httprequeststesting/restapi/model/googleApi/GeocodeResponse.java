package com.itech.http.httprequeststesting.restapi.model.googleApi;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Pavel on 26.08.2016.
 */
public class GeocodeResponse {
    @SerializedName("results")
    List<ResponseResult> result;

    public List<ResponseResult> getResult() {
        return result;
    }

    public class ResponseResult {
        @SerializedName("address_components")
        List<AddressComponent> addressComponent;

        public List<AddressComponent> getAddressComponent() {
            return addressComponent;
        }

        public class AddressComponent {
            public String getLongName() {
                return longName;
            }

            public String getShortName() {
                return shortName;
            }

            public List<String> getTypes() {
                return types;
            }

            @SerializedName("long_name")
            String longName;
            @SerializedName("short_name")
            String shortName;
            List<String> types;
        }
    }
}
