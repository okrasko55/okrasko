package com.itech.http.httprequeststesting.restapi.model.friends;

import com.itech.http.httprequeststesting.restapi.model.user.UserResponse;

/**
 * Created by Natalia on 21.08.2015.
 */
public class FriendRequestObject {

    private String to_user_id;

    private UserResponse fromUser;
    private UserResponse toUser;
    private String id;
    private String from_user_id;
    public FriendRequestObject() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFrom_user_id() {
        return from_user_id;
    }

    public void setFrom_user_id(String from_user_id) {
        this.from_user_id = from_user_id;
    }

    public String getTo_user_id() {
        return to_user_id;
    }

    public void setTo_user_id(String to_user_id) {
        this.to_user_id = to_user_id;
    }

    public UserResponse getFromUser() {
        return fromUser;
    }

    public void setFromUser(UserResponse fromUser) {
        this.fromUser = fromUser;
    }

    public UserResponse getToUserUser() {
        return toUser;
    }

    public void setToUser(UserResponse toUser) {
        this.toUser = toUser;
    }

}
