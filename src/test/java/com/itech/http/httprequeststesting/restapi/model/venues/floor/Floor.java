package com.itech.http.httprequeststesting.restapi.model.venues.floor;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Pavel on 28.12.2016.
 */
public class Floor {
    private Integer id;
    private Integer level;
    private long width;
    private long height;
    private long mmwidth;
    private long mmheight;
    private String image_name;
    private String step_width;
    private String step_height;
    private String created_at;
    private String updated_at;
    private String image_url;
    //    @SerializedName("px_perimeter")
//    private List<WallModel> perimeter;
    @SerializedName("px_perimeter")
    private String perimeters;
    @SerializedName("px_walls")
    private String walls;
    @SerializedName("angle_to_north")
    private float angleToNorth;
    private String doors;
    private String exits;
    private List<Portal> portals;
    @SerializedName("px_wall_height")
    private float wallHeight;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public long getWidth() {
        return width;
    }

    public void setWidth(long width) {
        this.width = width;
    }

    public long getHeight() {
        return height;
    }

    public void setHeight(long height) {
        this.height = height;
    }

    public long getMmwidth() {
        return mmwidth;
    }

    public void setMmwidth(long mmwidth) {
        this.mmwidth = mmwidth;
    }

    public long getMmheight() {
        return mmheight;
    }

    public void setMmheight(long mmheight) {
        this.mmheight = mmheight;
    }

    public String getImage_name() {
        return image_name;
    }

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }

    public String getStep_width() {
        return step_width;
    }

    public void setStep_width(String step_width) {
        this.step_width = step_width;
    }

    public String getStep_height() {
        return step_height;
    }

    public void setStep_height(String step_height) {
        this.step_height = step_height;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }


//    public List<WallModel> getPerimeter() {
//        return perimeter;
//    }
//
//    public void setPerimeter(List<WallModel> perimeter) {
//        this.perimeter = perimeter;
//    }

    public String getWalls() {
        return walls;
    }

    public void setWalls(String walls) {
        this.walls = walls;
    }

    public float getAngleToNorth() {
        return angleToNorth;
    }

    public void setAngleToNorth(float angleToNorth) {
        this.angleToNorth = angleToNorth;
    }

    public String getDoors() {
        return doors;
    }

    public String getExits() {
        return exits;
    }

    public List<Portal> getPortals() {
        return portals;
    }

    public void setDoors(String doors) {
        this.doors = doors;
    }

    public String getPerimeters() {
        return perimeters;
    }

    public float getWallHeight() {
        return wallHeight;
    }

    public void setWallHeight(float wallHeight) {
        this.wallHeight = wallHeight;
    }
}
