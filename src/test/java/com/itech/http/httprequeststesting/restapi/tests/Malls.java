package com.itech.http.httprequeststesting.restapi.tests;

import com.itech.http.httprequeststesting.DB.DataAccessObject;
import com.itech.http.httprequeststesting.DB.tables.TokenDataSet;
import com.itech.http.httprequeststesting.restapi.BuildConfig;
import com.itech.http.httprequeststesting.restapi.RestClient;
import com.itech.http.httprequeststesting.restapi.model.BaseResponseSync;
import com.itech.http.httprequeststesting.restapi.model.venues.VenueSyncObject;
import com.itech.http.httprequeststesting.restapi.requestmanager.RequestManager;
import com.itech.http.httprequeststesting.restapi.utils.TimeUtils;
import com.itech.http.httprequeststesting.restapi.utils.UsersContainer;

import org.junit.AfterClass;
import org.junit.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;
import java.util.Random;

import rx.observers.TestSubscriber;

/**
 * Created by Igor on 23.02.2017.
 */

public class Malls {

    private static String token;

    private static List<VenueSyncObject> venueOldList;
    private static List<VenueSyncObject> venueNewList;
    private static BaseResponseSync<VenueSyncObject> venueSyncResponseOld;
    private static BaseResponseSync<VenueSyncObject> venueSyncResponseNew;
    private static RestClient clientOld;
    private static RestClient clientNew;
    private static UsersContainer usersContainer;
    private static TokenDataSet tokenDataSet;


    @BeforeClass
    public static void setUp() throws Exception {
        clientOld = new RestClient(BuildConfig.URL_OLD);
        clientNew = new RestClient(BuildConfig.URL_NEW);
        usersContainer = new UsersContainer();

        //Get token
        Random random = new Random();
        token = "77a2aaaa3f6e1c8f1200fbf9ee876b6440e" + random.nextInt(10000);
        tokenDataSet = new TokenDataSet(token, usersContainer.getUser().getId(), TimeUtils.getBeginningTimeStamp() + 31536000);
        DataAccessObject.addObject(tokenDataSet);
        TestSubscriber <BaseResponseSync<VenueSyncObject>> subscriber = RequestManager.getVenueSyncResponse(token, TimeUtils.getBeginningTimeStamp(), clientOld);
        if (subscriber.getOnErrorEvents()==null) {
            venueSyncResponseOld = subscriber.getOnNextEvents().get(0);
        } else {
            System.out.println(subscriber.getOnErrorEvents().get(0).toString());
        }
        TestSubscriber <BaseResponseSync<VenueSyncObject>> subscriberNew = RequestManager.getVenueSyncResponse(token, TimeUtils.getBeginningTimeStamp(), clientNew);
        if (subscriber.getOnErrorEvents()==null) {
            venueSyncResponseOld = subscriberNew.getOnNextEvents().get(0);
        } else {
            System.out.println(subscriberNew.getOnErrorEvents().get(0).toString());
        }
        venueOldList = venueSyncResponseOld.getUpdatedList();
        venueNewList = venueSyncResponseNew.getUpdatedList();

    }
    @AfterClass
    public static void clearDB(){
        usersContainer.clear();

    }


    @Test
    public void checkUpdatedList() {
        Assert.assertEquals(venueOldList.size(), venueNewList.size());
        for (int i = 0; i < venueNewList.size(); i++) {
            Assert.assertEquals(venueOldList.get(i).getId(), venueNewList.get(i).getId());
            Assert.assertEquals(venueOldList.get(i).getName(), venueNewList.get(i).getName());
            Assert.assertEquals(venueOldList.get(i).getDescription(), venueNewList.get(i).getDescription());
            Assert.assertEquals(venueOldList.get(i).getLatitude(), venueNewList.get(i).getLatitude(), 0.01);
            Assert.assertEquals(venueOldList.get(i).getLongitude(), venueNewList.get(i).getLongitude(), 0.01);
            Assert.assertEquals(venueOldList.get(i).getImage(), venueNewList.get(i).getImage());
            Assert.assertEquals(venueOldList.get(i).getIndoorsApiKey(), venueNewList.get(i).getIndoorsApiKey());
            Assert.assertEquals(venueOldList.get(i).getIndoorsBuildingId(), venueNewList.get(i).getIndoorsBuildingId());
            Assert.assertEquals(venueOldList.get(i).getIndoorAtlasApiKey(), venueNewList.get(i).getIndoorAtlasApiKey());
            Assert.assertEquals(venueOldList.get(i).getIndoorAtlasSecret(), venueNewList.get(i).getIndoorAtlasSecret());
            Assert.assertEquals(venueOldList.get(i).getIndoorAtlasVenueId(), venueNewList.get(i).getIndoorAtlasVenueId());
            //Assert.assertEquals(venueOldList.get(i).getGpsCorners(), venueNewList.get(i).getGpsCorners());
            Assert.assertEquals(venueOldList.get(i).getImageUrl(), venueNewList.get(i).getImageUrl());
            Assert.assertEquals(venueOldList.get(i).getAccess(), venueNewList.get(i).getAccess());
            //Assert.assertEquals(venueOldList.get(i).getNavigation(), venueNewList.get(i).getNavigation());
            Assert.assertEquals(venueOldList.get(i).getSubscriptionId(), venueNewList.get(i).getSubscriptionId());
        }

    }

    @Test
    public void checkDeletedList() {
        Assert.assertEquals(venueSyncResponseOld.getDeletedIdList(), venueSyncResponseNew.getDeletedIdList());
    }

    @Test
    public void checkLostList() {
        Assert.assertEquals(venueSyncResponseOld.getLostList(), venueSyncResponseNew.getLostList());
    }


}
