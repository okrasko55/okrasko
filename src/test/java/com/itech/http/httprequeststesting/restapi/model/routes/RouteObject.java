package com.itech.http.httprequeststesting.restapi.model.routes;

import com.google.gson.annotations.SerializedName;
import com.itech.http.httprequeststesting.restapi.model.BaseResponse;

import java.util.HashMap;
import java.util.List;

/**
 * Created by artem on 22.06.16.
 */
public class RouteObject extends BaseResponse {
    //    @SerializedName("status")
//    private String routeStatus;
    @SerializedName("geocoded_waypoints")
    private List<GeocodedWaypoint> geocodedWaypoints;
    @SerializedName("routes")
    private List<Route> routes;

//    public String getRouteStatus() {
//        return routeStatus;
//    }
//
//    public void setRouteStatus(String routeStatus) {
//        this.routeStatus = routeStatus;
//    }

    public List<GeocodedWaypoint> getGeocodedWaypoints() {
        return geocodedWaypoints;
    }

    public void setGeocodedWaypoints(List<GeocodedWaypoint> geocodedWaypoints) {
        this.geocodedWaypoints = geocodedWaypoints;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public class GeocodedWaypoint {
        @SerializedName("geocoder_status")
        private String geocoderStatus;
        @SerializedName("place_id")
        private String placeId;
        @SerializedName("types")
        private List<String> types;

        public String getGeocoderStatus() {
            return geocoderStatus;
        }

        public void setGeocoderStatus(String geocoderStatus) {
            this.geocoderStatus = geocoderStatus;
        }

        public String getPlaceId() {
            return placeId;
        }

        public void setPlaceId(String placeId) {
            this.placeId = placeId;
        }

        public List<String> getTypes() {
            return types;
        }

        public void setTypes(List<String> types) {
            this.types = types;
        }
    }

    public class Route {
        @SerializedName("bounds")
        private HashMap<String, LatLng> bounds;
        @SerializedName("copyrights")
        private String copyrights;
        @SerializedName("legs")
        private List<Leg> legs;
        @SerializedName("overview_polyline")
        private HashMap<String, String> overviewPolyline;
        @SerializedName("summary")
        private String summary;
        @SerializedName("warnings")
        private List<String> warnings;
        @SerializedName("waypoint_order")
        private List<String> waypointOrder;

        public HashMap<String, LatLng> getBounds() {
            return bounds;
        }

        public void setBounds(HashMap<String, LatLng> bounds) {
            this.bounds = bounds;
        }

        public String getCopyrights() {
            return copyrights;
        }

        public void setCopyrights(String copyrights) {
            this.copyrights = copyrights;
        }

        public List<Leg> getLegs() {
            return legs;
        }

        public void setLegs(List<Leg> legs) {
            this.legs = legs;
        }

        public HashMap<String, String> getOverviewPolyline() {
            return overviewPolyline;
        }

        public void setOverviewPolyline(HashMap<String, String> overviewPolyline) {
            this.overviewPolyline = overviewPolyline;
        }

        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        public List<String> getWarnings() {
            return warnings;
        }

        public void setWarnings(List<String> warnings) {
            this.warnings = warnings;
        }

        public List<String> getWaypointOrder() {
            return waypointOrder;
        }

        public void setWaypointOrder(List<String> waypointOrder) {
            this.waypointOrder = waypointOrder;
        }
    }

    public class LatLng {
        @SerializedName("lat")
        private float lat;
        @SerializedName("lng")
        private float lng;

        public float getLat() {
            return lat;
        }

        public void setLat(float lat) {
            this.lat = lat;
        }

        public float getLng() {
            return lng;
        }

        public void setLng(float lng) {
            this.lng = lng;
        }
    }

    public class Leg {
        @SerializedName("distance")
        private HashMap<String, String> distance;
        @SerializedName("duration")
        private HashMap<String, String> duration;
        @SerializedName("end_address")
        private String endAddress;
        @SerializedName("end_location")
        private HashMap<String, String> endLocation;
        @SerializedName("start_address")
        private String startAddress;
        @SerializedName("start_location")
        private HashMap<String, String> startLocation;
        @SerializedName("steps")
        private List<Step> steps;
        @SerializedName("via_waypoint")
        private List<String> viaWaypoint;

        public HashMap<String, String> getDistance() {
            return distance;
        }

        public void setDistance(HashMap<String, String> distance) {
            this.distance = distance;
        }

        public HashMap<String, String> getDuration() {
            return duration;
        }

        public void setDuration(HashMap<String, String> duration) {
            this.duration = duration;
        }

        public String getEndAddress() {
            return endAddress;
        }

        public void setEndAddress(String endAddress) {
            this.endAddress = endAddress;
        }

        public HashMap<String, String> getEndLocation() {
            return endLocation;
        }

        public void setEndLocation(HashMap<String, String> endLocation) {
            this.endLocation = endLocation;
        }

        public String getStartAddress() {
            return startAddress;
        }

        public void setStartAddress(String startAddress) {
            this.startAddress = startAddress;
        }

        public HashMap<String, String> getStartLocation() {
            return startLocation;
        }

        public void setStartLocation(HashMap<String, String> startLocation) {
            this.startLocation = startLocation;
        }

        public List<Step> getSteps() {
            return steps;
        }

        public void setSteps(List<Step> steps) {
            this.steps = steps;
        }

        public List<String> getViaWaypoint() {
            return viaWaypoint;
        }

        public void setViaWaypoint(List<String> viaWaypoint) {
            this.viaWaypoint = viaWaypoint;
        }
    }

    public class Step {
        @SerializedName("distance")
        private HashMap<String, String> distance;
        @SerializedName("duration")
        private HashMap<String, String> duration;
        @SerializedName("end_location")
        private HashMap<String, String> endLocation;
        @SerializedName("html_instructions")
        private String htmlInstructions;
        @SerializedName("polyline")
        private HashMap<String, String> polyline;
        @SerializedName("start_location")
        private HashMap<String, String> startLocation;
        @SerializedName("travel_mode")
        private String travelMode;

        public HashMap<String, String> getDistance() {
            return distance;
        }

        public void setDistance(HashMap<String, String> distance) {
            this.distance = distance;
        }

        public HashMap<String, String> getDuration() {
            return duration;
        }

        public void setDuration(HashMap<String, String> duration) {
            this.duration = duration;
        }

        public HashMap<String, String> getEndLocation() {
            return endLocation;
        }

        public void setEndLocation(HashMap<String, String> endLocation) {
            this.endLocation = endLocation;
        }

        public String getHtmlInstructions() {
            return htmlInstructions;
        }

        public void setHtmlInstructions(String htmlInstructions) {
            this.htmlInstructions = htmlInstructions;
        }

        public HashMap<String, String> getPolyline() {
            return polyline;
        }

        public void setPolyline(HashMap<String, String> polyline) {
            this.polyline = polyline;
        }

        public HashMap<String, String> getStartLocation() {
            return startLocation;
        }

        public void setStartLocation(HashMap<String, String> startLocation) {
            this.startLocation = startLocation;
        }

       /* public com.google.android.gms.maps.model.LatLng getStartLocationAsLatLng() {
            return new com.google.android.gms.maps.model.LatLng(Double.valueOf(startLocation.get("lat")), Double.valueOf(startLocation.get("lng")));
        }

        public com.google.android.gms.maps.model.LatLng getEndLocationAsLatLng() {
            return new com.google.android.gms.maps.model.LatLng(Double.valueOf(endLocation.get("lat")), Double.valueOf(endLocation.get("lng")));
        }*/

        public String getTravelMode() {
            return travelMode;
        }

        public void setTravelMode(String travelMode) {
            this.travelMode = travelMode;
        }
    }
}
