package com.itech.http.httprequeststesting.restapi.model.venues;

import com.itech.http.httprequeststesting.restapi.model.BaseResponseSync;
import com.itech.http.httprequeststesting.restapi.model.venues.products.FacilityModel;

/**
 * Created by Администратор on 11.03.2017.
 */

public class FacilitySyncModelResponse extends BaseResponseSync<FacilityModel> {
}
