package com.itech.http.httprequeststesting.restapi.model.venues;

import com.google.gson.annotations.SerializedName;
import com.itech.http.httprequeststesting.restapi.model.BaseResponse;

import java.util.List;

/**
 * Created by Администратор on 11.03.2017.
 */

public class VenueSyncModel extends BaseResponse {
    @SerializedName("floors")
    private FloorSyncModelResponse floorList;

    @SerializedName("shops")
    private FacilitySyncModelResponse shops;

    @SerializedName("products")
    private ProductSyncModelResponse products;

    @SerializedName("placements")
    private SpacesSyncModelResponse spaces;

    @SerializedName("placement_products")
    private List<PlacementProducts> placementProducts;

    @SerializedName("timestamp")
    private long timestamp;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public FloorSyncModelResponse getFloorList() {
        return floorList;
    }

    public void setFloorList(FloorSyncModelResponse floorList) {
        this.floorList = floorList;
    }

    public FacilitySyncModelResponse getShops() {
        return shops;
    }

    public void setShops(FacilitySyncModelResponse shops) {
        this.shops = shops;
    }

    public ProductSyncModelResponse getProducts() {
        return products;
    }

    public void setProducts(ProductSyncModelResponse products) {
        this.products = products;
    }

    public SpacesSyncModelResponse getSpaces() {
        return spaces;
    }

    public void setSpaces(SpacesSyncModelResponse spaces) {
        this.spaces = spaces;
    }

    public List<PlacementProducts> getPlacementProducts() {
        return placementProducts;
    }

    public void setPlacementProducts(List<PlacementProducts> placementProducts) {
        this.placementProducts = placementProducts;
    }
}