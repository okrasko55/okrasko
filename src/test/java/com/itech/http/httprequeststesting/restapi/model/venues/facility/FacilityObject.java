package com.itech.http.httprequeststesting.restapi.model.venues.facility;


import com.google.gson.annotations.SerializedName;
import com.itech.http.httprequeststesting.restapi.model.venues.floor.Entry;
import com.itech.http.httprequeststesting.restapi.model.venues.floor.WallModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by natalia on 3/7/17.
 */

public class FacilityObject {
    private String id;
    private String name;
    private String description;
    private String mall_id;
    private String image;
    private String color;
    private String image_url;
    private String level;

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    @SerializedName("floor_id")
    private long floorId;

    private ArrayList<Entry> entries;

    @SerializedName("px_walls")
    private List<WallModel> pxWalls;

    @SerializedName("type_ids")
    private List<Long> types;

    @SerializedName("category_ids")
    private List<Long> categories;

    public List<Long> getCategories() {
        return categories;
    }

    public List<Long> getTypes() {
        return types;
    }

    public long getFloorId() {
        return floorId;
    }

    public void setCategories(List<Long> categories) {
        this.categories = categories;
    }

    public void setFloorId(long floorId) {
        this.floorId = floorId;
    }

    public void setTypes(List<Long> types) {
        this.types = types;
    }

    public List<WallModel> getPxWalls() {
        return pxWalls;
    }

    public void setPxWalls(List<WallModel> pxWalls) {
        this.pxWalls = pxWalls;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMall_id() {
        return mall_id;
    }

    public void setMall_id(String mall_id) {
        this.mall_id = mall_id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public ArrayList<Entry> getEntries() {
        return entries;
    }

    public void setEntries(ArrayList<Entry> entries) {
        this.entries = entries;
    }

}
