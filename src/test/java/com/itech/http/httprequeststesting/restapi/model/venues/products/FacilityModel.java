package com.itech.http.httprequeststesting.restapi.model.venues.products;


import java.util.HashSet;

/**
 * Created by barbinkh on 03.08.2015.
 */
public class FacilityModel implements Comparable<FacilityModel> {

    private String id;
    private String name;
    private String description;
    private String mall_id;
    private String level;
    private String image_url;
    private HashSet<ProductModel> products;

    public FacilityModel() {

    }

    public FacilityModel(String id, String name, String mall_id, String description, String level, String image_url) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.mall_id = mall_id;
        this.level = level;
        this.image_url = image_url;
    }

    public HashSet<ProductModel> getProducts() {
        return products;
    }

    public void setProducts(HashSet<ProductModel> products) {
        this.products = products;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMall_id() {
        return mall_id;
    }

    public void setMall_id(String mall_id) {
        this.mall_id = mall_id;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }

        if (o instanceof FacilityModel) {
            FacilityModel that = (FacilityModel) o;
            return name.equals(that.name) && id.equals(that.id);
        }
        return false;
    }


    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public int compareTo(FacilityModel o) {
        return 0;
    }

/*    @Override
    public int compareTo(@NonNull FacilityModel facilityModel) {
        return name.compareTo(facilityModel.getName());
    }*/
}
