package com.itech.http.httprequeststesting.restapi.tests;

import com.itech.http.httprequeststesting.DB.DataAccessObject;
import com.itech.http.httprequeststesting.DB.tables.AdminDataSet;
import com.itech.http.httprequeststesting.DB.tables.InvitationsDataSet;
import com.itech.http.httprequeststesting.DB.tables.MallsDataSet;
import com.itech.http.httprequeststesting.DB.tables.TokenDataSet;
import com.itech.http.httprequeststesting.DB.tables.UserDataSet;
import com.itech.http.httprequeststesting.Logger.Log4jConfigure;
import com.itech.http.httprequeststesting.restapi.BuildConfig;
import com.itech.http.httprequeststesting.restapi.RestClient;
import com.itech.http.httprequeststesting.restapi.model.BaseResponseSync;
import com.itech.http.httprequeststesting.restapi.model.InviteResponseModel;
import com.itech.http.httprequeststesting.restapi.model.venues.VenueSyncObject;
import com.itech.http.httprequeststesting.restapi.requestmanager.RequestManager;
import com.itech.http.httprequeststesting.restapi.setupdata.SetupMalls;
import com.itech.http.httprequeststesting.restapi.utils.TimeUtils;
import com.itech.http.httprequeststesting.restapi.utils.UsersContainer;

import org.junit.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Igor on 28.02.2017.
 */

public class MallsSyncTests {

    private String token;
    private Integer userId;
    private String userEmail;
    private List<VenueSyncObject> venueNewList;
    private BaseResponseSync<VenueSyncObject> venueSyncResponseNew;
    private InviteResponseModel inviteResponseModel;
    private static RestClient clientNew;
    private List<MallsDataSet> addedMalls;
    private List<MallsDataSet> publicMalls;
    private List<MallsDataSet> privateMalls;
    private boolean isPresent;
    private Random random;
    private TokenDataSet tokenDataSet;
    private UsersContainer usersContainer;


    @BeforeClass
    public static void init() {
        Log4jConfigure.configureSimpleConsole();
        clientNew = new RestClient(BuildConfig.URL_NEW);
    }

    @Before
    public void setUp() throws Exception {
        //Initialize
        random = new Random();

        addedMalls = SetupMalls.addMallsToDB();
        publicMalls = SetupMalls.getPublicMalls();
        privateMalls = SetupMalls.getPrivateMalls();



        //Get token
        usersContainer = new UsersContainer();
        token = "77a2aaaa3f6e1c8f1200fbf9ee876b6440e" + random.nextInt(10000);
        tokenDataSet = new TokenDataSet(token, usersContainer.getUser().getId(), TimeUtils.getBeginningTimeStamp() + 31536000);
//        SignResponse response = RequestManager.getSignInResponse("automation@user.com", "a", clientNew);
        DataAccessObject.addObject(tokenDataSet);


        //Set session user id
        userId = usersContainer.getUser().getId();

        //Set session user email
        userEmail = usersContainer.getUser().getEmail();

        //isPresent reset
        isPresent = false;

    }


    @After
    public void deleteMall() {

        for (int i = 0; i < addedMalls.size(); i++) {
            DataAccessObject.delete(addedMalls.get(i));
        }
        usersContainer.clear();
    }


    @Test
    public void checkUpdatedPresentPublic() {
        //Get sync response
        venueSyncResponseNew = RequestManager.getVenueSyncResponse(token, 0L, clientNew).getOnNextEvents().get(0);
        venueNewList = venueSyncResponseNew.getUpdatedList();

        for (int i = 0; i < publicMalls.size(); i++) {
            isPresent = false;
            for (int j = 0; j < venueNewList.size(); j++) {
                if (Integer.valueOf(venueNewList.get(j).getId()) == publicMalls.get(i).getId()) {
                    isPresent = true;
                }
            }
            Assert.assertTrue("Public mall with id: [" + publicMalls.get(i).getId() + "] is ABSENT in the Updated list", isPresent);
        }
    }


    @Test
    public void checkUpdatedAbsentPrivate() {
        //Get sync response
        venueSyncResponseNew = RequestManager.getVenueSyncResponse(token, 0L, clientNew).getOnNextEvents().get(0);
        venueNewList = venueSyncResponseNew.getUpdatedList();

        for (int i = 0; i < privateMalls.size(); i++) {
            isPresent = false;
            for (int j = 0; j < venueNewList.size(); j++) {
                if (Integer.valueOf(venueNewList.get(j).getId()) == privateMalls.get(i).getId()) {
                    isPresent = true;
                }
            }
            Assert.assertFalse("Private mall with id: [" + privateMalls.get(i).getId() + "] is PRESENT in the Updated list", isPresent);
        }
    }

    @Test
    public void checkDeleted() {
        Long currentTimestamp = TimeUtils.getCurrentTimeStamp();

        //Deleting malls by setting deleted_at date in the DB
        for (int i = 0; i < publicMalls.size(); i++) {
            publicMalls.get(i).setDeletedAt(currentTimestamp);
            DataAccessObject.update(publicMalls.get(i));
        }
        //Sync malls
        venueSyncResponseNew = RequestManager.getVenueSyncResponse(token, currentTimestamp, clientNew).getOnNextEvents().get(0);
        List<Integer> deletedList = venueSyncResponseNew.getDeletedIdList();

        //Search changed mall in the response
        for (int i = 0; i < publicMalls.size(); i++) {
            isPresent = false;
            for (int j = 0; j < deletedList.size(); j++) {
                if (Integer.valueOf(publicMalls.get(j).getId()).equals(Integer.valueOf(deletedList.get(i)))) {
                    isPresent = true;
                }
            }
            Assert.assertTrue("Public mall with id: [" + publicMalls.get(i).getId() + "] is ABSENT in the DELETED list", isPresent);
        }

    }

    @Test
    public void addAdmin() {
        //Add relationship between private mall and logged in user
        DataAccessObject.addObject(new AdminDataSet(userId, "mall", privateMalls.get(0).getId(), null, TimeUtils.getCurrentTimeStamp(), TimeUtils.getCurrentTimeStamp(), null));

        //Update mall update_at field in the DB
        privateMalls.get(0).setUpdatedAt(TimeUtils.getCurrentTimeStamp());
        DataAccessObject.update(privateMalls.get(0));

        //Sync malls
        venueSyncResponseNew = RequestManager.getVenueSyncResponse(token, privateMalls.get(0).getUpdatedAt() - 1, clientNew).getOnNextEvents().get(0);
        venueNewList = venueSyncResponseNew.getUpdatedList();

        //Search changed mall in the response
        for (int i = 0; i < venueNewList.size(); i++) {
            if (Integer.valueOf(venueNewList.get(i).getId()) == privateMalls.get(0).getId()) {
                isPresent = true;
            }
        }

        Assert.assertTrue("Private mall with id: [" + privateMalls.get(0).getId() + "] is ABSENT in the Updated list after status has been ADDED TO ADMIN", isPresent);
    }

    @Test
    public void addInvitation() {
        InvitationsDataSet invitationsDataSet;
        //Generate invite token
        String inviteToken = String.valueOf(random.nextInt(1000000000));

        //Add invitation for user in the DB
        invitationsDataSet = new InvitationsDataSet(1, userId, userEmail, privateMalls.get(0).getId(), 1, TimeUtils.getCurrentTimeStamp(), TimeUtils.getCurrentTimeStamp(), null, inviteToken);
        DataAccessObject.addObject(invitationsDataSet);

        //Approve invitation
        inviteResponseModel = RequestManager.getInviteApproveResponse(token, inviteToken, clientNew);

        //Sync malls
        venueSyncResponseNew = RequestManager.getVenueSyncResponse(token, inviteResponseModel.getTimestamp() - 1, clientNew).getOnNextEvents().get(0);
        venueNewList = venueSyncResponseNew.getUpdatedList();

        //Search changed mall in the response
        for (int i = 0; i < venueNewList.size(); i++) {
            if (Integer.valueOf(venueNewList.get(i).getId()) == privateMalls.get(0).getId()) {
                isPresent = true;
            }
        }
        Assert.assertTrue("Private mall with id: [" + privateMalls.get(0).getId() + "] is ABSENT in the Updated list after user has been INVITED", isPresent);

        //Clear DB
        DataAccessObject.delete(invitationsDataSet);
    }

    @Test
    public void LostInvitation() {
        //Generate invite token
        String inviteToken = String.valueOf(random.nextInt(1000000000));

        //Add invitation for user in the DB
        List<InvitationsDataSet> invitationsList;
        invitationsList = new ArrayList<>();
        InvitationsDataSet invitationsDataSet;

        for (int i = 0; i < privateMalls.size(); i++) {
            invitationsDataSet = new InvitationsDataSet(1, userId, userEmail, privateMalls.get(i).getId(), 1, TimeUtils.getCurrentTimeStamp(), TimeUtils.getCurrentTimeStamp(), null, inviteToken);
                    DataAccessObject.addObject(invitationsDataSet);
            invitationsList.add(invitationsDataSet);
        }

        //Approve invitation
        inviteResponseModel = RequestManager.getInviteApproveResponse(token, inviteToken, clientNew);

        Long currentTimeStamp = TimeUtils.getCurrentTimeStamp();

        //Cancel invitation  by setting deleted_at date in the DB
        for (int i = 0; i < privateMalls.size(); i++) {
            invitationsList.get(i).setDeletedAt(currentTimeStamp);
            DataAccessObject.update(invitationsList.get(i));
        }


        //Set access_updated date for Mall in the DB
        for (int i = 0; i < privateMalls.size(); i++) {
            //privateMalls.get(i).setAccessUpdated(currentTimeStamp);
            privateMalls.get(i).setUpdatedAt(currentTimeStamp);
            DataAccessObject.update(privateMalls.get(i));
        }

        //Sync malls
        venueSyncResponseNew = RequestManager.getVenueSyncResponse(token, currentTimeStamp - 1, clientNew).getOnNextEvents().get(0);
        List<Integer> lostList = venueSyncResponseNew.getLostList();

        //Search changed mall in the response
        for (int i = 0; i < privateMalls.size(); i++) {
            isPresent = false;
            for (int j = 0; j < lostList.size(); j++) {
                if (Integer.valueOf(privateMalls.get(j).getId()).equals(Integer.valueOf(lostList.get(i)))) {
                    isPresent = true;
                }
            }
            Assert.assertTrue("Private mall with id: [" + privateMalls.get(i).getId() + "] is ABSENT in the LOST list after access has been LOST (Invitation)", isPresent);
        }

    }

    @Test
    public void LostAdmin() {
        UserDataSet userDataSet;
        //Add relationship between private mall and logged in user
        AdminDataSet adminDataSet = new AdminDataSet(userId, "mall", privateMalls.get(0).getId(), null, TimeUtils.getCurrentTimeStamp(), TimeUtils.getCurrentTimeStamp(), null);
        DataAccessObject.addObject(adminDataSet);

        //Update mall update_at field in the DB
        privateMalls.get(0).setUpdatedAt(TimeUtils.getCurrentTimeStamp());
        DataAccessObject.update(privateMalls.get(0));

        //Sync malls
        venueSyncResponseNew = RequestManager.getVenueSyncResponse(token, privateMalls.get(0).getUpdatedAt() - 1, clientNew).getOnNextEvents().get(0);
        venueNewList = venueSyncResponseNew.getUpdatedList();

        //Search changed mall in the response
        for (int i = 0; i < venueNewList.size(); i++) {
            if (Integer.valueOf(venueNewList.get(i).getId()) == privateMalls.get(0).getId()) {
                isPresent = true;
            }
        }
        Assert.assertTrue("Private mall with id: [" + privateMalls.get(0).getId() + "] is ABSENT in the Updated list after status has been ADDED TO ADMIN", isPresent);

        Long currentTimeStamp = TimeUtils.getCurrentTimeStamp();

        //Deleting admin relationship by setting deleted_at date in the DB
        adminDataSet.setDeletedAt(currentTimeStamp);
        DataAccessObject.update(adminDataSet);

        //Set updated_at date for Mall in the DB
        for (int i = 0; i < privateMalls.size(); i++) {
            //privateMalls.get(i).setAccessUpdated(currentTimeStamp);
            privateMalls.get(i).setUpdatedAt(currentTimeStamp);
            DataAccessObject.update(privateMalls.get(i));
        }

        //Set updated_at date for User in the DB
        userDataSet = (UserDataSet) DataAccessObject.get(UserDataSet.class, userId);
        userDataSet.setUpdatedUt(currentTimeStamp);
        DataAccessObject.update(userDataSet);

        //Sync malls
        venueSyncResponseNew = RequestManager.getVenueSyncResponse(token, currentTimeStamp - 1, clientNew).getOnNextEvents().get(0);
        List<Integer> lostList = venueSyncResponseNew.getLostList();

        //Search changed mall in the response
        for (int i = 0; i < privateMalls.size(); i++) {
            isPresent = false;
            for (int j = 0; j < lostList.size(); j++) {
                if (Integer.valueOf(privateMalls.get(j).getId()).equals(Integer.valueOf(lostList.get(i)))) {
                    isPresent = true;
                }
            }
            Assert.assertTrue("Private mall with id: [" + privateMalls.get(i).getId() + "] is ABSENT in the LOST list after access has been LOST (Admin)", isPresent);
        }

    }

    @Test
    public void deleteInvitation() {
        //Generate invite token
        String inviteToken = String.valueOf(random.nextInt(1000000000));

        //Add invitation for user in the DB
        InvitationsDataSet invitationsDataSet = new InvitationsDataSet(1, userId, userEmail, privateMalls.get(0).getId(), 1, TimeUtils.getCurrentTimeStamp(), TimeUtils.getCurrentTimeStamp(), null, inviteToken);
                DataAccessObject.addObject(invitationsDataSet);

        //Approve invitation
        inviteResponseModel = RequestManager.getInviteApproveResponse(token, inviteToken, clientNew);

        //Get updated invitation
        invitationsDataSet = (InvitationsDataSet) DataAccessObject.get(InvitationsDataSet.class, invitationsDataSet.getId());

        //Deleting invitation by setting deleted_at date in the DB
        invitationsDataSet.setDeletedAt(TimeUtils.getCurrentTimeStamp());
        DataAccessObject.update(invitationsDataSet);

        //Sync malls
        venueSyncResponseNew = RequestManager.getVenueSyncResponse(token, invitationsDataSet.getDeletedAt() - 1, clientNew).getOnNextEvents().get(0);
        venueNewList = venueSyncResponseNew.getUpdatedList();

        //Search changed mall in the response
        for (int i = 0; i < venueNewList.size(); i++) {
            if (Integer.valueOf(venueNewList.get(i).getId()) == privateMalls.get(0).getId()) {
                isPresent = true;
            }
        }
        Assert.assertFalse("Private mall with id: [" + privateMalls.get(0).getId() + "] is PRESENT in the Updated list after invitation has been DELETED", isPresent);

        //Clear DB
        DataAccessObject.delete(invitationsDataSet);
    }

}
