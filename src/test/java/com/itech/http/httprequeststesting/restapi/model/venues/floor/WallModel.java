package com.itech.http.httprequeststesting.restapi.model.venues.floor;


import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by pavel on 11/19/15.
 */
public class WallModel {
    String type;
    @SerializedName("points")
    List<NodeModel> nodeModels;
    float depth;

    public WallModel() {

    }

    public WallModel(WallModel wallModel) {
        depth = wallModel.depth;
        type = wallModel.type;
        nodeModels = new LinkedList<>();
        for (NodeModel nodeModel : wallModel.nodeModels) {
            nodeModels.add(new NodeModel(nodeModel));
        }
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float getDepth() {
        return depth;
    }

    public void setDepth(float depth) {
        this.depth = depth;
    }

    public List<NodeModel> getNodeModels() {
        return nodeModels;
    }

    public void setNodeModels(List<NodeModel> nodeModels) {
        this.nodeModels = nodeModels;
    }

    @Override
    public String toString() {
        return nodeModels.get(0).toString() + "," + nodeModels.get(1).toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WallModel wallModel = (WallModel) o;
        if (nodeModels == null || wallModel.nodeModels == null)
            return false;

        for (int i = 0; i < nodeModels.size(); i++) {
            if (!nodeModels.get(i).equals(wallModel.nodeModels.get(i))) {
                return false;
            }
        }

        return true;

    }

    @Override
    public int hashCode() {
        return nodeModels != null ? nodeModels.hashCode() : 0;
    }
}
