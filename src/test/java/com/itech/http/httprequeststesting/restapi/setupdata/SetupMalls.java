package com.itech.http.httprequeststesting.restapi.setupdata;

import com.google.gson.reflect.TypeToken;
import com.itech.http.httprequeststesting.DB.DataAccessObject;
import com.itech.http.httprequeststesting.DB.tables.FloorDataSet;
import com.itech.http.httprequeststesting.DB.tables.MallsDataSet;
import com.itech.http.httprequeststesting.DB.tables.TypeDataSet;
import com.itech.http.httprequeststesting.restapi.utils.JsonToJava;
import com.itech.http.httprequeststesting.restapi.utils.TimeUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Igor on 16.03.2017.
 */

public class SetupMalls {
    private static List<MallsDataSet> allMalls;
    private static List<MallsDataSet> publicMalls;
    private static List<MallsDataSet> privateMalls;
    private static MallsDataSet mallsDataSet;
    private static List<MallsDataSet> addedMalls;

    public static void deleteAddedMalls(List<MallsDataSet> allMalls) {
        allMalls.forEach(DataAccessObject::delete);
    }

    public static List<MallsDataSet> addMallsToDB() throws Exception {
        Random random = new Random();
        int rand = random.nextInt(1000);
        addedMalls = new ArrayList<>();
        allMalls = JsonToJava.getData("malls.json", new TypeToken<List<MallsDataSet>>() {
        }.getType());
        publicMalls = new ArrayList<>();
        privateMalls = new ArrayList<>();
        for (int i = 0; i < allMalls.size(); i++) {
            mallsDataSet = new MallsDataSet(allMalls.get(i).getMallName() + rand++, allMalls.get(i).getDescription(),
                    TimeUtils.getBeginningTimeStamp(), TimeUtils.getBeginningTimeStamp(), allMalls.get(i).getIndoorsApiKey(),
                    allMalls.get(i).getIndoorsBuildingId(), allMalls.get(i).getLongitude(), allMalls.get(i).getLatitude(),
                    allMalls.get(i).getImage(), allMalls.get(i).getIndoorAtlasApiKey(), allMalls.get(i).getIndoorAtlasSecret(),
                    allMalls.get(i).getIndoorAtlasVenueId(), allMalls.get(i).getGpsCorners(), allMalls.get(i).getDeletedAt(),
                    allMalls.get(i).getAreaId(), allMalls.get(i).getAccess(), allMalls.get(i).getAccessUpdated(),
                    allMalls.get(i).getSubscriptionId());

            DataAccessObject.addObject(mallsDataSet);
            addedMalls.add(mallsDataSet);
            if (mallsDataSet.getAccess() == 0) {
                //publicMalls.add(allMalls.get(i));
                publicMalls.add(mallsDataSet);
            }
            if (mallsDataSet.getAccess() == 1) {
                //privateMalls.add(allMalls.get(i));
                privateMalls.add(mallsDataSet);
            }

        }
        return addedMalls;
    }



    public static List<MallsDataSet> getPublicMalls() {
        return publicMalls;
    }

    public static List<MallsDataSet> getPrivateMalls() {
        return privateMalls;
    }

}
