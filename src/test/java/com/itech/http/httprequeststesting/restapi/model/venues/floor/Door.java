package com.itech.http.httprequeststesting.restapi.model.venues.floor;

/**
 * Created by Pavel on 28.12.2016.
 */
public class Door {
    float px;
    float py;
    int width;

    public float getPx() {

        return px;
    }

    public void setPx(int px) {
        this.px = px;
    }

    public float getPy() {
        return py;
    }

    public void setPy(int py) {
        this.py = py;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
}
