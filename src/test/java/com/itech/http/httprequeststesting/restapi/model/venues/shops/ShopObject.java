package com.itech.http.httprequeststesting.restapi.model.venues.shops;

import com.itech.http.httprequeststesting.restapi.model.venues.typescategories.TypeCategoryObject;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by NataliLapshina on 24.06.2015.
 */
public class ShopObject {

    public static final String NAME_FIELD = "name";

    private String id;
    private String name;
    private String description;
    private String mall_id;
    private String level;
    private String image_url;
    private ArrayList<ShopEnter> entries;
    private Map<String, ShopWall> walls;
    private ArrayList<TypeCategoryObject> types;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVeneId() {
        return mall_id;
    }

    public void setVenueId(String mall_id) {
        this.mall_id = mall_id;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public ArrayList<ShopEnter> getEntries() {
        return entries;
    }

    public void setEntries(ArrayList<ShopEnter> entries) {
        this.entries = entries;
    }

    public Map<String, ShopWall> getWalls() {
        return walls;
    }

    public void setWalls(Map<String, ShopWall> walls) {
        this.walls = walls;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public ArrayList<TypeCategoryObject> getTypeList() {
        return types;
    }

    public void setTypeList(ArrayList<TypeCategoryObject> typeList) {
        this.types = typeList;
    }

    public static class ShopEnter {
        private String id;
        private String shop_id;
        private int x;
        private int y;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getShop_id() {
            return shop_id;
        }

        public void setShop_id(String shop_id) {
            this.shop_id = shop_id;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }
    }

    public static class ShopWall {

        private String id;

        private int x;
        private int y;
        private int z;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }

        public int getZ() {
            return z;
        }

        public void setZ(int z) {
            this.z = z;
        }
    }
}
