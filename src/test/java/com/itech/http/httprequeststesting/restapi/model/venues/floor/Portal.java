package com.itech.http.httprequeststesting.restapi.model.venues.floor;

/**
 * Created by Pavel on 28.12.2016.
 */
public class Portal {

    float from_px;
    float from_py;
    int from_z;
    float to_px;
    float to_py;
    int to_z;

    public float getFrom_px() {
        return from_px;
    }

    public float getFrom_py() {
        return from_py;
    }

    public int getFrom_z() {
        return from_z;
    }

    public float getTo_px() {
        return to_px;
    }

    public float getTo_py() {
        return to_py;
    }


    public int getTo_z() {
        return to_z;
    }

}
