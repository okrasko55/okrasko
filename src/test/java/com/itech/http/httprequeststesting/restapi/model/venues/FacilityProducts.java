package com.itech.http.httprequeststesting.restapi.model.venues;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by natalia on 10.03.17.
 */

public class FacilityProducts {

    @SerializedName("shop_id")
    private Long shopId;

    @SerializedName("product_ids")
    private List<Long> productsIds;

    public List<Long> getProductsIds() {
        return productsIds;
    }

    public void setProductsIds(List<Long> productsIds) {
        this.productsIds = productsIds;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }
}
