package com.itech.http.httprequeststesting.restapi.tests;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.itech.http.httprequeststesting.DB.DataAccessObject;
import com.itech.http.httprequeststesting.DB.tables.AdminDataSet;
import com.itech.http.httprequeststesting.DB.tables.FacilityDataSet;
import com.itech.http.httprequeststesting.DB.tables.FloorDataSet;
import com.itech.http.httprequeststesting.DB.tables.MallsDataSet;
import com.itech.http.httprequeststesting.DB.tables.TokenDataSet;
import com.itech.http.httprequeststesting.Logger.Log4jConfigure;
import com.itech.http.httprequeststesting.restapi.BuildConfig;
import com.itech.http.httprequeststesting.restapi.RestClient;
import com.itech.http.httprequeststesting.restapi.model.venues.VenueSyncModel;
import com.itech.http.httprequeststesting.restapi.model.venues.floor.Floor;
import com.itech.http.httprequeststesting.restapi.requestmanager.RequestManager;
import com.itech.http.httprequeststesting.restapi.setupdata.SetUpMallData;
import com.itech.http.httprequeststesting.restapi.setupdata.SetupMalls;
import com.itech.http.httprequeststesting.restapi.utils.JsonToJava;
import com.itech.http.httprequeststesting.restapi.utils.TimeUtils;
import com.itech.http.httprequeststesting.restapi.utils.UsersContainer;

import org.json.JSONObject;
import org.junit.Assert;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import rx.observers.TestSubscriber;

/**
 * Created by Администратор on 11.03.2017.
 */

public class MallSyncTests {

    private static Logger log;
    private static RestClient clientNew;
    private String token;
    private List<MallsDataSet> addedMalls;
    private List<MallsDataSet> publicMalls;
    private List<MallsDataSet> privateMalls;
    private TokenDataSet tokenDataSet;
    private UsersContainer usersContainer;
    private static Random random;

    @BeforeClass
    public static void init() {
        random = new Random();
        Log4jConfigure.configureSimpleConsole();
        log = Logger.getLogger(MallSyncTests.class);
        System.setProperty("http.proxyHost", "localhost");
        System.setProperty("http.proxyPort", "8888");
        clientNew = new RestClient(BuildConfig.URL_NEW);

    }

    public void setUpStandard() throws Exception {


        SetUpMallData.addFloor(publicMalls.get(0), -1, "map");
        SetUpMallData.addFloor(publicMalls.get(0), -0, "map");
        SetUpMallData.addFloor(publicMalls.get(0), 1, "map");

        SetUpMallData.addType(publicMalls.get(0), "Shop", "approved");
        SetUpMallData.addType(publicMalls.get(0), "Service", "approved");

        SetUpMallData.addCategories(publicMalls.get(0), "category.json");

        SetUpMallData.linkCategoryToType(publicMalls.get(0).getCategoriesList().get(0), publicMalls.get(0).getTypesList().get(0));
        SetUpMallData.linkCategoryToType(publicMalls.get(0).getCategoriesList().get(1), publicMalls.get(0).getTypesList().get(0));
        SetUpMallData.linkCategoryToType(publicMalls.get(0).getCategoriesList().get(2), publicMalls.get(0).getTypesList().get(1));

        SetUpMallData.addFacilitiesToFloor(publicMalls.get(0), "facility.json", -1);

        SetUpMallData.linkFacilityToCategories(publicMalls.get(0).getFloorsList().get(0).getFacilitiesList().get(0),
                publicMalls.get(0).getCategoriesList().get(0));

        SetUpMallData.linkFacilityToCategories(publicMalls.get(0).getFloorsList().get(0).getFacilitiesList().get(0),
                publicMalls.get(0).getCategoriesList().get(1));

        SetUpMallData.linkFacilityToCategories(publicMalls.get(0).getFloorsList().get(0).getFacilitiesList().get(1),
                publicMalls.get(0).getCategoriesList().get(2));

        for (FacilityDataSet facilityDataSet : publicMalls.get(0).getFloorsList().get(0).getFacilitiesList()) {
            SetUpMallData.addPointPlacementToFacility(facilityDataSet, "placement.json", random.nextInt(1000), random.nextInt(1000));
        }


        SetUpMallData.addProductsToFacility(publicMalls.get(0).getId(), publicMalls.get(0).getFloorsList().get(0).getFacilitiesList().get(0), "product.json");

    }

    @Before
    public void addUser() throws Exception {
        addedMalls = new ArrayList<>();
        publicMalls = new ArrayList<>();
        privateMalls = new ArrayList<>();

        publicMalls.add(SetUpMallData.addMall(49.989956, 36.264839, 3, 0, 15));
        addedMalls.add(publicMalls.get(0));
        privateMalls.add(SetUpMallData.addMall(49.990142, 36.253906, 3, 1, 15));
        addedMalls.add(privateMalls.get(0));

        usersContainer = new UsersContainer();
        token = "77a2aaaa3f6e1c8f1200fbf9ee876b6440e" + random.nextInt(10000);
        tokenDataSet = new TokenDataSet(token, usersContainer.getUser().getId(), TimeUtils.getBeginningTimeStamp() + 31536000);
        DataAccessObject.addObject(tokenDataSet);

        //setUpStandard();

    }
    @After
    public void clearDB() {
        if (addedMalls != null){
            SetupMalls.deleteAddedMalls(addedMalls);
        }

        usersContainer.clear();
        //addedMalls.forEach(DataAccessObject::delete);
    }

    @Test
    public void addDoublePortal() throws Exception {
        //String floorBefore = "{\"perimeters\":{\"svg-1\":{\"walls\":{\"1_0_2\":{\"x\":0,\"y\":2,\"z\":1,\"id\":\"1_0_2\"},\"1_1_2\":{\"x\":1,\"y\":2,\"z\":1,\"id\":\"1_1_2\"},\"1_2_2\":{\"x\":2,\"y\":2,\"z\":1,\"id\":\"1_2_2\"},\"1_3_2\":{\"x\":3,\"y\":2,\"z\":1,\"id\":\"1_3_2\"},\"1_4_2\":{\"x\":4,\"y\":2,\"z\":1,\"id\":\"1_4_2\"},\"1_5_2\":{\"x\":5,\"y\":2,\"z\":1,\"id\":\"1_5_2\"},\"1_6_2\":{\"x\":6,\"y\":2,\"z\":1,\"id\":\"1_6_2\"},\"1_7_2\":{\"x\":7,\"y\":2,\"z\":1,\"id\":\"1_7_2\"},\"1_8_2\":{\"x\":8,\"y\":2,\"z\":1,\"id\":\"1_8_2\"},\"1_9_2\":{\"x\":9,\"y\":2,\"z\":1,\"id\":\"1_9_2\"},\"1_9_3\":{\"x\":9,\"y\":3,\"z\":1,\"id\":\"1_9_3\"},\"1_9_4\":{\"x\":9,\"y\":4,\"z\":1,\"id\":\"1_9_4\"},\"1_9_5\":{\"x\":9,\"y\":5,\"z\":1,\"id\":\"1_9_5\"},\"1_9_6\":{\"x\":9,\"y\":6,\"z\":1,\"id\":\"1_9_6\"},\"1_9_7\":{\"x\":9,\"y\":7,\"z\":1,\"id\":\"1_9_7\"},\"1_8_7\":{\"x\":8,\"y\":7,\"z\":1,\"id\":\"1_8_7\"},\"1_7_7\":{\"x\":7,\"y\":7,\"z\":1,\"id\":\"1_7_7\"},\"1_6_7\":{\"x\":6,\"y\":7,\"z\":1,\"id\":\"1_6_7\"},\"1_5_7\":{\"x\":5,\"y\":7,\"z\":1,\"id\":\"1_5_7\"},\"1_4_7\":{\"x\":4,\"y\":7,\"z\":1,\"id\":\"1_4_7\"},\"1_3_7\":{\"x\":3,\"y\":7,\"z\":1,\"id\":\"1_3_7\"},\"1_2_7\":{\"x\":2,\"y\":7,\"z\":1,\"id\":\"1_2_7\"},\"1_1_7\":{\"x\":1,\"y\":7,\"z\":1,\"id\":\"1_1_7\"},\"1_0_7\":{\"x\":0,\"y\":7,\"z\":1,\"id\":\"1_0_7\"},\"1_0_6\":{\"x\":0,\"y\":6,\"z\":1,\"id\":\"1_0_6\"},\"1_0_5\":{\"x\":0,\"y\":5,\"z\":1,\"id\":\"1_0_5\"},\"1_0_4\":{\"x\":0,\"y\":4,\"z\":1,\"id\":\"1_0_4\"},\"1_0_3\":{\"x\":0,\"y\":3,\"z\":1,\"id\":\"1_0_3\"}},\"pxWalls\":[{\"type\":\"linear\",\"points\":[{\"x\":9.843245735361847,\"y\":78.66912555709234},{\"x\":275.3957276778853,\"y\":74.7502689411403}],\"depth\":15},{\"type\":\"linear\",\"points\":[{\"x\":275.3957276778853,\"y\":74.7502689411403},{\"x\":277.36284001844166,\"y\":229.1685876748117}],\"depth\":15},{\"type\":\"linear\",\"points\":[{\"x\":277.36284001844166,\"y\":229.1685876748117},{\"x\":12.786230213616104,\"y\":232.1192561856462}],\"depth\":15},{\"type\":\"linear\",\"points\":[{\"x\":12.786230213616104,\"y\":232.1192561856462},{\"x\":9.843245735361847,\"y\":78.66912555709234}],\"depth\":15}],\"points\":[{\"x\":9.843245735361847,\"y\":78.66912555709234},{\"x\":275.3957276778853,\"y\":74.7502689411403},{\"x\":277.36284001844166,\"y\":229.1685876748117},{\"x\":12.786230213616104,\"y\":232.1192561856462},{\"x\":9.843245735361847,\"y\":78.66912555709234}],\"facilities\":{},\"indoorWalls\":{},\"doors\":{},\"exits\":{},\"portals\":{}}},\"nodes\":{},\"px_wall_height\":null,\"mm_wall_height\":3000}";
        String floorAfter = "{\"perimeters\":{\"svg-perimeter-1\":{\"points\":[{\"x\":9.8432457353618,\"y\":78.669125557092},{\"x\":275.39572767789,\"y\":74.75026894114},{\"x\":277.36284001844,\"y\":229.16858767481},{\"x\":12.786230213616,\"y\":232.11925618565},{\"x\":9.8432457353618,\"y\":78.669125557092}],\"doors\":{},\"exits\":{},\"facilities\":{},\"indoorWalls\":{},\"portals\":{\"svg-portal-579\":{\"from\":{\"x\":5,\"y\":6,\"z\":-1,\"px\":170,\"py\":201},\"to\":{\"x\":92,\"y\":36,\"z\":1,\"px\":462,\"py\":183},\"dir\":\"duplex\"}},\"pxWalls\":[{\"type\":\"linear\",\"points\":[{\"x\":9.8432457353618,\"y\":78.669125557092},{\"x\":275.39572767789,\"y\":74.75026894114}],\"depth\":15},{\"type\":\"linear\",\"points\":[{\"x\":275.39572767789,\"y\":74.75026894114},{\"x\":277.36284001844,\"y\":229.16858767481}],\"depth\":15},{\"type\":\"linear\",\"points\":[{\"x\":277.36284001844,\"y\":229.16858767481},{\"x\":12.786230213616,\"y\":232.11925618565}],\"depth\":15},{\"type\":\"linear\",\"points\":[{\"x\":12.786230213616,\"y\":232.11925618565},{\"x\":9.8432457353618,\"y\":78.669125557092}],\"depth\":15}],\"walls\":{\"1_0_2\":{\"x\":0,\"y\":2,\"z\":-1,\"id\":\"1_0_2\"},\"1_1_2\":{\"x\":1,\"y\":2,\"z\":-1,\"id\":\"1_1_2\"},\"1_2_2\":{\"x\":2,\"y\":2,\"z\":-1,\"id\":\"1_2_2\"},\"1_3_2\":{\"x\":3,\"y\":2,\"z\":-1,\"id\":\"1_3_2\"},\"1_4_2\":{\"x\":4,\"y\":2,\"z\":-1,\"id\":\"1_4_2\"},\"1_5_2\":{\"x\":5,\"y\":2,\"z\":-1,\"id\":\"1_5_2\"},\"1_6_2\":{\"x\":6,\"y\":2,\"z\":-1,\"id\":\"1_6_2\"},\"1_7_2\":{\"x\":7,\"y\":2,\"z\":-1,\"id\":\"1_7_2\"},\"1_8_2\":{\"x\":8,\"y\":2,\"z\":-1,\"id\":\"1_8_2\"},\"1_9_2\":{\"x\":9,\"y\":2,\"z\":-1,\"id\":\"1_9_2\"},\"1_9_3\":{\"x\":9,\"y\":3,\"z\":-1,\"id\":\"1_9_3\"},\"1_9_4\":{\"x\":9,\"y\":4,\"z\":-1,\"id\":\"1_9_4\"},\"1_9_5\":{\"x\":9,\"y\":5,\"z\":-1,\"id\":\"1_9_5\"},\"1_9_6\":{\"x\":9,\"y\":6,\"z\":-1,\"id\":\"1_9_6\"},\"1_9_7\":{\"x\":9,\"y\":7,\"z\":-1,\"id\":\"1_9_7\"},\"1_8_7\":{\"x\":8,\"y\":7,\"z\":-1,\"id\":\"1_8_7\"},\"1_7_7\":{\"x\":7,\"y\":7,\"z\":-1,\"id\":\"1_7_7\"},\"1_6_7\":{\"x\":6,\"y\":7,\"z\":-1,\"id\":\"1_6_7\"},\"1_5_7\":{\"x\":5,\"y\":7,\"z\":-1,\"id\":\"1_5_7\"},\"1_4_7\":{\"x\":4,\"y\":7,\"z\":-1,\"id\":\"1_4_7\"},\"1_3_7\":{\"x\":3,\"y\":7,\"z\":-1,\"id\":\"1_3_7\"},\"1_2_7\":{\"x\":2,\"y\":7,\"z\":-1,\"id\":\"1_2_7\"},\"1_1_7\":{\"x\":1,\"y\":7,\"z\":-1,\"id\":\"1_1_7\"},\"1_0_7\":{\"x\":0,\"y\":7,\"z\":-1,\"id\":\"1_0_7\"},\"1_0_6\":{\"x\":0,\"y\":6,\"z\":-1,\"id\":\"1_0_6\"},\"1_0_5\":{\"x\":0,\"y\":5,\"z\":-1,\"id\":\"1_0_5\"},\"1_0_4\":{\"x\":0,\"y\":4,\"z\":-1,\"id\":\"1_0_4\"},\"1_0_3\":{\"x\":0,\"y\":3,\"z\":-1,\"id\":\"1_0_3\"}}}},\"nodes\":{},\"px_wall_height\":null,\"mm_wall_height\":3000}";
        String session = "_csrf=e51c5ca112e3e61a27e9e9b2db3ebaa459ee7017ac8154b6900df4fceb266b5ba%3A2%3A%7Bi%3A0%3Bs%3A5%3A%22_csrf%22%3Bi%3A1%3Bs%3A32%3A%226CoNBVgUZP7wJqdEgmHOHnKBR6pYPhjA%22%3B%7D; token=ad04f6223b71cd553cf1abab9736bb66e666dc9227fe8b4c75902f905f9f7ca3a%3A2%3A%7Bi%3A0%3Bs%3A5%3A%22token%22%3Bi%3A1%3Bs%3A40%3A%2265bf26a67712eb20eea4d58854cd322543ce1aa2%22%3B%7D; PHPSESSID=ip6pjp542pj0flhauorako5uj0";
        //Add new floors
        FloorDataSet floorFrom = SetUpMallData.addFloor(publicMalls.get(0), -1, "map");
        FloorDataSet floorTo = SetUpMallData.addFloor(publicMalls.get(0), 1, "map");
        //Set updated date to the public venue
        publicMalls.get(0).setUpdatedAt(TimeUtils.getCurrentTimeStamp());
        DataAccessObject.update(publicMalls.get(0));

        String url = "/floors/" + floorFrom.getId() + "/save-all";

        RequestManager.saveFloorPlan(url, new JsonParser().parse(floorAfter).getAsJsonObject(), session, clientNew);

        floorFrom = (FloorDataSet) DataAccessObject.get(FloorDataSet.class, floorFrom.getId());
        floorFrom.setPxPerimeter("[]");
        floorFrom.setPxWalls("[]");
        floorFrom.setDoors("[]");
        floorFrom.setExits("[]");
        DataAccessObject.update(floorFrom);

        TestSubscriber<VenueSyncModel> venue = getResponse(publicMalls.get(0).getId(), floorFrom.getUpdatedAt());

        Assert.assertEquals(-1, venue.getOnNextEvents().get(0).getFloorList().getUpdatedList().get(0).getPortals().get(0).getFrom_z());
        Assert.assertEquals(1, venue.getOnNextEvents().get(0).getFloorList().getUpdatedList().get(0).getPortals().get(0).getTo_z());

    }

    @Test
    public void privateMallIsNotExist() throws Exception {
        TestSubscriber<VenueSyncModel> subscriber;
        for (int i = 0; i < privateMalls.size(); i++) {
            subscriber = RequestManager.getVenueIdSyncResponse(token, privateMalls.get(i).getId(),
                    String.valueOf(privateMalls.get(i).getUpdatedAt() - 1), clientNew);
            Assert.assertEquals("retrofit2.adapter.rxjava.HttpException: HTTP 403 Forbidden", String.valueOf(subscriber.getOnErrorEvents().get(0)));
        }
    }

    @Test
    public void addedFloorInUpdate() throws Exception {
        //Add new floor
        FloorDataSet floor = SetUpMallData.addFloor(publicMalls.get(0), -1, "map");
        //Set updated date to the public venue
        publicMalls.get(0).setUpdatedAt(TimeUtils.getCurrentTimeStamp());
        DataAccessObject.update(publicMalls.get(0));
        //Get response since updated floor date
        TestSubscriber<VenueSyncModel> venue = getResponse(publicMalls.get(0).getId(), floor.getUpdatedAt());
        //Check added floor in the updated list
        Assert.assertTrue("Floor id: " + floor.getId() + " MISSING in the UPDATED list: " + venue.getOnNextEvents().get(0).getFloorList().getUpdatedList().toString(),
                isFloorPresentInUpdatedList(floor.getId(), venue.getOnNextEvents().get(0).getFloorList().getUpdatedList()));
    }

    @Test
    public void deletedFloorInDelete() throws Exception {
        //Add new floor
        FloorDataSet floor = SetUpMallData.addFloor(publicMalls.get(0), -1, "map");
        //Set deleted date for the floor
        floor.setDeletedAt(TimeUtils.getCurrentTimeStamp());
        DataAccessObject.update(floor);
        //Set updated date to the public venue
        publicMalls.get(0).setUpdatedAt(TimeUtils.getCurrentTimeStamp());
        DataAccessObject.update(publicMalls.get(0));
        //Get response since deleted floor date
        TestSubscriber<VenueSyncModel> venue = getResponse(publicMalls.get(0).getId(), floor.getDeletedAt());
        //Check deleted mall in the deleted list
        Assert.assertTrue("Floor id: " + floor.getId() + " MISSING in the DELETED list: " + venue.getOnNextEvents().get(0).getFloorList().getDeletedIdList().toString(),
                isFloorPresentInList(floor.getId(), venue.getOnNextEvents().get(0).getFloorList().getDeletedIdList()));
    }

    @Test
    public void deletedFloorAbsentInUpdate() throws Exception {
        //Add new floor
        FloorDataSet floor = SetUpMallData.addFloor(publicMalls.get(0), -1, "map");
        //Set deleted date to the floor
        floor.setDeletedAt(TimeUtils.getCurrentTimeStamp());
        DataAccessObject.update(floor);
        //Set updated date to the public venue
        publicMalls.get(0).setUpdatedAt(TimeUtils.getCurrentTimeStamp());
        DataAccessObject.update(publicMalls.get(0));
        //Get response since deleted floor date
        TestSubscriber<VenueSyncModel> venue = getResponse(publicMalls.get(0).getId(), floor.getDeletedAt());
        //Check deleted mall is not preset in the updated list
        Assert.assertFalse("Floor id: " + floor.getId() + " present in the UPDATED list: " + venue.getOnNextEvents().get(0).getFloorList().getUpdatedList().toString(),
                isFloorPresentInUpdatedList(floor.getId(), venue.getOnNextEvents().get(0).getFloorList().getUpdatedList()));
    }



    public TestSubscriber<VenueSyncModel> getResponse(Integer mallId, Long updatedAt){
        TestSubscriber<VenueSyncModel> subscriber;
        subscriber = RequestManager.getVenueIdSyncResponse(token, mallId,
                String.valueOf(updatedAt - 1), clientNew);
        return subscriber;
    }

    private boolean isFloorPresentInUpdatedList(Integer floorId, List<Floor> updatedList) {
        for (Floor item : updatedList) {
            if (floorId.equals(Integer.valueOf(item.getId()))) {
                return true;
            }
        }
        return false;
    }

    private boolean isFloorPresentInList(Integer floorId, List<Integer> list) {
        for (Integer item : list) {
            if (floorId.equals(Integer.valueOf(item))) {
                return true;
            }
        }
        return false;
    }
}