package com.itech.http.httprequeststesting.restapi.model.venues;

import com.google.gson.annotations.SerializedName;
import com.itech.http.httprequeststesting.restapi.model.BaseResponseSync;

import java.util.List;

/**
 * Created by Pavel on 29.10.2015.
 */
public class VenueSyncResponse extends BaseResponseSync<VenueSyncObject> {

    @SerializedName("lost")
    private List<Long> lostAccessIdList;

    public List<Long> getLostAccessIdList() {
        return lostAccessIdList;
    }
}
