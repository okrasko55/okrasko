package com.itech.http.httprequeststesting.restapi.model.venues.shops.spaces;

import com.google.gson.annotations.SerializedName;
import com.itech.http.httprequeststesting.restapi.model.venues.floor.WallModel;

import java.util.List;

/**
 * Created by barbinkh on 27.02.17.
 */

public class Spaces {
    @SerializedName("id")
    private int id;
    @SerializedName("shop_id")
    private int shopId;
    @SerializedName("name")
    private String spaceName;
    @SerializedName("px_walls")
    private List<WallModel> walls;
    @SerializedName("color")
    private String color;
    @SerializedName("px_wall_height")
    private float height;
    @SerializedName("category_ids")
    private List<Long> categoryIds;
    @SerializedName("type_ids")
    private List<Long> typeIds;
    @SerializedName("x")
    private float x;
    @SerializedName("y")
    private float y;

    public Spaces(int id, int shopId, String spaceName, List<WallModel> walls, List<Long> categoryIds, List<Long> typeIds, String color, float height, float x, float y) {
        this.id = id;
        this.shopId = shopId;
        this.spaceName = spaceName;
        this.walls = walls;
        this.categoryIds = categoryIds;
        this.typeIds = typeIds;
        this.color = color;
        this.height = height;
        this.x = x;
        this.y = y;
    }

    public Spaces(int id, int shopId, String spaceName, List<WallModel> walls, List<Long> categoryIds, List<Long> typeIds, String color, float height) {
        this.id = id;
        this.shopId = shopId;
        this.spaceName = spaceName;
        this.walls = walls;
        this.categoryIds = categoryIds;
        this.typeIds = typeIds;
        this.color = color;
        this.height = height;
        this.x = x;
        this.y = y;
    }

    public Spaces() {
    }


    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public String getSpaceName() {
        return spaceName;
    }

    public void setSpaceName(String spaceName) {
        this.spaceName = spaceName;
    }

    public List<WallModel> getWalls() {
        return walls;
    }

    public void setWalls(List<WallModel> walls) {
        this.walls = walls;
    }

    public List<Long> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(List<Long> categoryIds) {
        this.categoryIds = categoryIds;
    }

    public List<Long> getTypeIds() {
        return typeIds;
    }

    public void setTypeIds(List<Long> typeIds) {
        this.typeIds = typeIds;
    }

}
