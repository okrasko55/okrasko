package com.itech.http.httprequeststesting.restapi.utils;

import com.itech.http.httprequeststesting.DB.DataAccessObject;
import com.itech.http.httprequeststesting.DB.tables.ChildDataSet;
import com.itech.http.httprequeststesting.DB.tables.UserDataSet;
import com.itech.http.httprequeststesting.restapi.setupdata.SetUpChildren;
import com.itech.http.httprequeststesting.restapi.setupdata.SetUpUsers;

import java.util.HashMap;
import java.util.List;

/**
 * Created by inevzorov on 3/23/17.
 */

public class UsersContainer {
    private UserDataSet user;
    private List<ChildDataSet> childList;
    private List<UserDataSet> friendList;
    private HashMap<UserDataSet, List<ChildDataSet>> userChildren;

    public UsersContainer() throws Exception {
        user = SetUpUsers.addUsers().get(0);
        childList = SetUpChildren.addChildrenToParent(user);
    }

    public UserDataSet getUser() {
        return user;
    }

    public List<ChildDataSet> getChildList() {
        return childList;
    }

    public void clear(){
        childList.forEach(DataAccessObject::delete);
        DataAccessObject.delete(user);
    }
}
