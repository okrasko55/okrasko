package com.itech.http.httprequeststesting.restapi.model.venues;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Natalia on 16.09.2015.
 */
public class VenueSyncObject {

    public VenueSyncObject() {
    }

    public static final String NAME_FIELD = "name";

    private String id;

    private String name;

    private String description;

    @SerializedName("lng")
    private double longitude;

    @SerializedName("lat")
    private double latitude;

    private String image;

    @SerializedName("indoors_api_key")
    private String indoorsApiKey;

    @SerializedName("indoors_building_id")
    private String indoorsBuildingId;

    @SerializedName("indooratlas_api_key")
    private String indoorAtlasApiKey;

    @SerializedName("indooratlas_secret")
    private String indoorAtlasSecret;

    @SerializedName("indooratlas_venue_id")
    private String indoorAtlasVenueId;

    @SerializedName("image_url")
    private String imageUrl;

/*    @SerializedName("gps_corners")
    public List<ServerLatLong> gpsCorners;*/

    /*private boolean navigation;*/

    @SerializedName("subscription_id")
    private int subscriptionId;

   /* @SerializedName("access_text")
    private int accessText;
*/
    private int access;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getAccess() {
        return access;
    }

/*    public int getAccessText() {
        return accessText;
    }*/

    public String getName() {
        return name;
    }

/*
    public boolean getNavigation() {
        return navigation;
    }
*/

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIndoorsBuildingId() {
        return indoorsBuildingId;
    }

  /*  public List<ServerLatLong> getGpsCorners() {
        return gpsCorners;
    }*/

    public void setIndoorsBuildingId(String indoorsBuildingId) {
        this.indoorsBuildingId = indoorsBuildingId;
    }

    public String getIndoorsApiKey() {
        return indoorsApiKey;
    }

    public void setIndoorsApiKey(String indoorsApiKey) {
        this.indoorsApiKey = indoorsApiKey;
    }

    public String getIndoorAtlasApiKey() {
        return indoorAtlasApiKey;
    }

    public void setIndoorAtlasApiKey(String indoorAtlasApiKey) {
        this.indoorAtlasApiKey = indoorAtlasApiKey;
    }

    public String getIndoorAtlasSecret() {
        return indoorAtlasSecret;
    }

    public void setIndoorAtlasSecret(String indoorAtlasSecret) {
        this.indoorAtlasSecret = indoorAtlasSecret;
    }

    public String getIndoorAtlasVenueId() {
        return indoorAtlasVenueId;
    }

    public void setIndoorAtlasVenueId(String indoorAtlasVenueId) {
        this.indoorAtlasVenueId = indoorAtlasVenueId;
    }


    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.image = imageUrl;
    }

   /* public boolean isNavigation() {
        return navigation;
    }

    public void setNavigation(boolean navigation) {
        this.navigation = navigation;
    }*/

    public int getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(int subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public static class ServerLatLong {
        public float lat;
        public float lng;
    }

}
