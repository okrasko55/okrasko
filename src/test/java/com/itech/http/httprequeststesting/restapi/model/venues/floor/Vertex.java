package com.itech.http.httprequeststesting.restapi.model.venues.floor;

/**
 * Created by Pavel on 28.12.2016.
 */
public class Vertex {
    private int x;
    private int y;
    private int z;
    private float px;
    private float py;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }


    public void setPx(int px) {
        this.px = px;
    }

    public float getPx() {
        return px;
    }

    public void setPx(float px) {
        this.px = px;
    }

    public float getPy() {
        return py;
    }

    public void setPy(int py) {
        this.py = py;
    }

    public void setPy(float py) {
        this.py = py;
    }
}
