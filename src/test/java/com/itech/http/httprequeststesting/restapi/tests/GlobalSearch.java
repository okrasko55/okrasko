package com.itech.http.httprequeststesting.restapi.tests;

import com.google.gson.reflect.TypeToken;
import com.itech.http.httprequeststesting.DB.DataAccessObject;
import com.itech.http.httprequeststesting.DB.tables.FacilityDataSet;
import com.itech.http.httprequeststesting.DB.tables.MallsDataSet;
import com.itech.http.httprequeststesting.DB.tables.TokenDataSet;
import com.itech.http.httprequeststesting.Logger.Log4jConfigure;
import com.itech.http.httprequeststesting.restapi.BuildConfig;
import com.itech.http.httprequeststesting.restapi.RestClient;
import com.itech.http.httprequeststesting.restapi.model.venues.products.GlobalSearchProductModel;
import com.itech.http.httprequeststesting.restapi.requestmanager.RequestManager;
import com.itech.http.httprequeststesting.restapi.setupdata.SetUpMallData;
import com.itech.http.httprequeststesting.restapi.utils.JsonToJava;
import com.itech.http.httprequeststesting.restapi.utils.TimeUtils;
import com.itech.http.httprequeststesting.restapi.utils.UsersContainer;

import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;
import java.util.Random;

import rx.observers.TestSubscriber;

/**
 * Created by inevzorov on 3/27/17.
 */

public class GlobalSearch {
    private static Logger log;
    private static RestClient clientNew;
    private static MallsDataSet mallsDataSet;
    private static String token;
    private static UsersContainer usersContainer;
    private static TokenDataSet tokenDataSet;


    @BeforeClass
    public static void init() throws Exception {
        Log4jConfigure.configureSimpleConsole();
        log = Logger.getLogger(MallSyncTests.class);
        clientNew = new RestClient(BuildConfig.URL_NEW);
        usersContainer = new UsersContainer();
        Random random = new Random();
        //Set session token
        token = "77a2aaaa3f6e1c8f1200fbf9ee876b6440e" + random.nextInt(10000);
        tokenDataSet = new TokenDataSet(token, usersContainer.getUser().getId(), TimeUtils.getBeginningTimeStamp() + 31536000);
        DataAccessObject.addObject(tokenDataSet);

        log.info(token);
        //Add mall
        mallsDataSet = SetUpMallData.addMall(49.989956, 36.264839, 3, 0, 15);
    }

    public void setUpStandard() throws Exception {
        Random random = new Random();

        SetUpMallData.addFloor(mallsDataSet, -1, "map");
        SetUpMallData.addFloor(mallsDataSet, -0, "map");
        SetUpMallData.addFloor(mallsDataSet, 1, "map");

        SetUpMallData.addType(mallsDataSet, "Shop", "approved");
        SetUpMallData.addType(mallsDataSet, "Service", "approved");

        SetUpMallData.addCategories(mallsDataSet, "category.json");

        SetUpMallData.linkCategoryToType(mallsDataSet.getCategoriesList().get(0), mallsDataSet.getTypesList().get(0));
        SetUpMallData.linkCategoryToType(mallsDataSet.getCategoriesList().get(1), mallsDataSet.getTypesList().get(0));
        SetUpMallData.linkCategoryToType(mallsDataSet.getCategoriesList().get(2), mallsDataSet.getTypesList().get(1));

        SetUpMallData.addFacilitiesToFloor(mallsDataSet, "facility.json", -1);

        SetUpMallData.linkFacilityToCategories(mallsDataSet.getFloorsList().get(0).getFacilitiesList().get(0),
                mallsDataSet.getCategoriesList().get(0));

        SetUpMallData.linkFacilityToCategories(mallsDataSet.getFloorsList().get(0).getFacilitiesList().get(0),
                mallsDataSet.getCategoriesList().get(1));

        SetUpMallData.linkFacilityToCategories(mallsDataSet.getFloorsList().get(0).getFacilitiesList().get(1),
                mallsDataSet.getCategoriesList().get(2));

        for (FacilityDataSet facilityDataSet : mallsDataSet.getFloorsList().get(0).getFacilitiesList()) {
            SetUpMallData.addPointPlacementToFacility(facilityDataSet, "placement.json", random.nextInt(1000), random.nextInt(1000));
        }


        SetUpMallData.addProductsToFacility(mallsDataSet.getId(), mallsDataSet.getFloorsList().get(0).getFacilitiesList().get(0), "product.json");

    }

    @AfterClass
    public static void clear() {
        if (usersContainer.getUser() != null) {
            usersContainer.clear();
        }
        if (mallsDataSet != null) {
            DataAccessObject.delete(mallsDataSet);
        }
    }

    @Test
    public void getSomeResponse() throws Exception {
        setUpStandard();
        getGlobalSearchProductResponse(token, "Kyiv city", "bbbb");
    }


    public GlobalSearchProductModel getGlobalSearchProductResponse(String token, String area, String value) throws Exception {
        TestSubscriber<GlobalSearchProductModel> response = RequestManager.getGlobalSearchProducts(token, area, value, clientNew);
        if (response.getOnErrorEvents().size() > 0) {
            Assert.assertNull("Response error is: " + response.getOnErrorEvents().get(0).getMessage(), response.getOnErrorEvents().get(0));
        }
        return response.getOnNextEvents().get(0);
    }

}