package com.itech.http.httprequeststesting.restapi.model.venues.shops;

import com.itech.http.httprequeststesting.restapi.model.venues.ProductPlacements;
import com.itech.http.httprequeststesting.restapi.model.venues.typescategories.TypeCategoryObject;

import java.util.ArrayList;

/**
 * Created by NataliLapshina on 24.06.2015.
 */
public class ShopExtendPlacementObject {

    public static final String NAME_FIELD = "name";

    private String id;
    private String name;
    private String description;
    private String mall_id;
    private String level;
    private String image_url;

    private ArrayList<ProductPlacements> productPlacements;


    private ArrayList<TypeCategoryObject> types;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVenueId() {
        return mall_id;
    }

    public void setVenueId(String mall_id) {
        this.mall_id = mall_id;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }


    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public ArrayList<ProductPlacements> getProductPlacements() {
        return productPlacements;
    }
}
