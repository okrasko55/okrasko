package com.itech.http.httprequeststesting.restapi.model.sign;


import com.itech.http.httprequeststesting.restapi.model.BaseResponse;
import com.itech.http.httprequeststesting.restapi.model.venues.VenueObject;

import java.util.List;

/**
 * Created by NataliLapshina on 18.05.2015.
 */
public class SignResponse extends BaseResponse {

    private String token;
    private String first_name;
    private String last_name;
    private String id;
    private String photo_url;
    private String photo_landscape_url;
    private String phone_code;
    private String phone_value;
    private String email;
    private int location_access;
    private int profile_visible;
    private int voice_nav;
    private int send_push;
    private int accept_ads;

    //    private Phone phone;
    private List<VenueObject> admins;

    public int getSend_push() {
        return send_push;
    }

    public int getLocation_access() {
        return location_access;
    }

    public void setLocation_access(int location_access) {
        this.location_access = location_access;
    }

    public int getProfile_visible() {
        return profile_visible;
    }

    public void setProfile_visible(int profile_visible) {
        this.profile_visible = profile_visible;
    }

    public int getVoice_nav() {
        return voice_nav;
    }

    public void setVoice_nav(int voice_nav) {
        this.voice_nav = voice_nav;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    public List<VenueObject> getAdmins() {
        return admins;
    }

    public void setAdmins(List<VenueObject> admins) {
        this.admins = admins;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_code() {
        return phone_code;
    }

    public void setPhone_code(String phone_code) {
        this.phone_code = phone_code;
    }

    public String getPhone_value() {
        return phone_value;
    }

    public void setPhone_value(String phone_value) {
        this.phone_value = phone_value;
    }

    public int getAccept_ads() {
        return accept_ads;
    }

    public void setAccept_ads(int accept_ads) {
        this.accept_ads = accept_ads;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SignResponse{");
        sb.append("token='").append(token).append('\'');
        sb.append(", first_name='").append(first_name).append('\'');
        sb.append(", last_name='").append(last_name).append('\'');
        sb.append(", id='").append(id).append('\'');
        sb.append(", photo_url='").append(photo_url).append('\'');
        sb.append(", phone_code='").append(phone_code).append('\'');
        sb.append(", phone_value='").append(phone_value).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", admins=").append(admins);
        sb.append('}');
        return sb.toString();
    }

    public String getPhoto_landscape_url() {
        return photo_landscape_url;
    }

    public void setPhoto_landscape_url(String photo_landscape_url) {
        this.photo_landscape_url = photo_landscape_url;
    }
}
