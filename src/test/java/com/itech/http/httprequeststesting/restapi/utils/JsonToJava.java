package com.itech.http.httprequeststesting.restapi.utils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.itech.http.httprequeststesting.DB.tables.CategoryDataSet;
import com.itech.http.httprequeststesting.DB.tables.FacilityDataSet;
import com.itech.http.httprequeststesting.DB.tables.FloorDataSet;
import com.itech.http.httprequeststesting.DB.tables.MallsDataSet;
import com.itech.http.httprequeststesting.DB.tables.TypeDataSet;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by Igor on 10.03.2017.
 */

public class JsonToJava {
    public static <T> List <T> getData(String fileName, Type listType) throws Exception {
        InputStream is = new FileInputStream("src/main/res/" + fileName);
        Reader reader = new StringReader(readResponse(is));
        return new Gson().fromJson(reader, listType);
    }

   /* public static JsonObject getJson (String fileName) throws Exception {
        InputStream is = new FileInputStream("src/main/res/" + fileName);
        Reader reader = new StringReader(readResponse(is));
        return new Gson().to;
    }
*/
    static String readResponse(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while ((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }
}
