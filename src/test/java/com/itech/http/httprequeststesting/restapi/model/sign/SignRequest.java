package com.itech.http.httprequeststesting.restapi.model.sign;



import com.itech.http.httprequeststesting.restapi.APIConstants;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by NataliLapshina on 18.05.2015.
 */
public class SignRequest {

    private String code;
    private String email;
    private String phone_code;
    private String phone_value;
    private String password;
    private String first_name;
    private String last_name;


    public SignRequest(String email, String phoneCode, String phoneValue, String password) {
        this.email = email;
        this.phone_code = phoneCode;
        this.phone_value = phoneValue;
        this.password = password;
    }

    public SignRequest(String code, String email, String phoneCode, String phoneValue, String password) {
        this.code = code;
        this.email = email;
        this.phone_code = phoneCode;
        this.phone_value = phoneValue;
        this.password = password;
    }

    public SignRequest(String email, String phoneCode, String phoneValue, String password, String first_name, String last_name) {
        this.email = email;
        this.phone_code = phoneCode;
        this.phone_value = phoneValue;
        this.password = password;
        this.first_name = first_name;
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_code() {
        return phone_code;
    }

    public void setPhone_code(String phone_code) {
        this.phone_code = phone_code;
    }

    public String getPhone_value() {
        return phone_value;
    }

    public void setPhone_value(String phone_value) {
        this.phone_value = phone_value;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public Map<String, String> convertToParamsMap() {
        Map<String, String> result = new HashMap<>();
        result.put(APIConstants.EMAIL_PARAM, email);
        result.put(APIConstants.PHONE_CODE_PARAM, phone_code);
        result.put(APIConstants.PHONE_VALUE_PARAM, phone_value);
        result.put(APIConstants.PASSWORD_PARAM, password);
        result.put(APIConstants.CODE_PARAM, code);
        result.put(APIConstants.EXCLUDE_PARAM, APIConstants.ADMINS_PARAM);
        return result;
    }
}
