package com.itech.http.httprequeststesting.restapi.model.venues.products;

import com.google.gson.annotations.SerializedName;
import com.itech.http.httprequeststesting.restapi.model.BaseResponse;
import com.itech.http.httprequeststesting.restapi.model.venues.FacilityProducts;
import com.itech.http.httprequeststesting.restapi.model.venues.PlacementProducts;
import com.itech.http.httprequeststesting.restapi.model.venues.facility.FacilityObject;
import com.itech.http.httprequeststesting.restapi.model.venues.shops.spaces.Spaces;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by barbinkh on 16.03.17.
 */

public class GlobalSearchProductModel extends BaseResponse {
    private List<ProductsObject> products;
    private List<Spaces> placements;
    @SerializedName("placement_products")
    private List<PlacementProducts> spaceProducts;
    private List<FacilityObject> shops;
    @SerializedName("shop_products")
    private List<FacilityProducts> shopProducts;

    public GlobalSearchProductModel() {
        products = new ArrayList<>();
        placements = new ArrayList<>();
        spaceProducts = new ArrayList<>();
        shops = new ArrayList<>();
        placements = new ArrayList<>();
        shopProducts = new ArrayList<>();
    }

    public List<ProductsObject> getProducts() {
        return products;
    }

    public void setProducts(List<ProductsObject> products) {
        this.products = products;
    }

    public List<Spaces> getPlacements() {
        return placements;
    }

    public void setPlacements(List<Spaces> placements) {
        this.placements = placements;
    }

    public List<PlacementProducts> getSpaceProducts() {
        return spaceProducts;
    }

    public void setSpaceProducts(List<PlacementProducts> spaceProducts) {
        this.spaceProducts = spaceProducts;
    }

    public List<FacilityObject> getShops() {
        return shops;
    }

    public void setShops(List<FacilityObject> shops) {
        this.shops = shops;
    }

    public List<FacilityProducts> getShopProducts() {
        return shopProducts;
    }

    public void setShopProducts(List<FacilityProducts> shopProducts) {
        this.shopProducts = shopProducts;
    }
}
