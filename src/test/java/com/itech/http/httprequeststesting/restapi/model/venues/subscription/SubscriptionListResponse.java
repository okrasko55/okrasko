package com.itech.http.httprequeststesting.restapi.model.venues.subscription;

import com.itech.http.httprequeststesting.restapi.model.BaseResponseSync;

/**
 * Created by natalia on 11/22/16.
 */

public class SubscriptionListResponse extends BaseResponseSync<SubscriptionObject> {
}
