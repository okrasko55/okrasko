package com.itech.http.httprequeststesting.restapi.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Natalia on 21.09.2015.
 */
public class BaseResponseSync<T> extends BaseResponse {

    @SerializedName("update")
    private List<T> updatedList;

    @SerializedName("delete")
    private List<Integer> deletedIdList;

    @SerializedName("lost")
    private List<Integer> lostIdList;

    public List<Integer> getDeletedIdList() {
        return deletedIdList;
    }

    public List<T> getUpdatedList() {
        return updatedList;
    }

    public List<Integer> getLostList() {
        return lostIdList;
    }

    public BaseResponseSync() {
    }

}
