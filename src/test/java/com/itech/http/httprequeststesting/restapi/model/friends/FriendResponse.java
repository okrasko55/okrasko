package com.itech.http.httprequeststesting.restapi.model.friends;

import com.itech.http.httprequeststesting.restapi.model.BaseResponse;

/**
 * Created by Natalia on 15.08.2015.
 */
public class FriendResponse extends BaseResponse {
    private String id;
    private FriendObject friend;

    public FriendResponse() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public FriendObject getFriend() {
        return friend;
    }

    public void setFriend(FriendObject friend) {
        this.friend = friend;
    }
}
