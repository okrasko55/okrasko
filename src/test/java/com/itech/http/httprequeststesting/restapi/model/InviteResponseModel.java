package com.itech.http.httprequeststesting.restapi.model;

import java.util.List;

/**
 * Created by Pavel on 17.06.2016.
 */
public class InviteResponseModel {
    public List<String> errors;
    private String status;
    private Long timestamp;

    public Long getTimestamp() {
        return timestamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getErrors() {
        return errors;
    }
}
