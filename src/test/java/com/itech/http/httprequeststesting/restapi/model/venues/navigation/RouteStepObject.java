package com.itech.http.httprequeststesting.restapi.model.venues.navigation;

/**
 * Created by  on 29.07.2015.
 */
public class RouteStepObject {
    private String id;
    private int x;
    private int y;
    private int z;
    private double angle;
    private double px;
    private double py;
    private double scaleMMtoPX;

    public RouteStepObject() {
    }

    public RouteStepObject(double px, double py) {
        this.px = px;
        this.py = py;
    }

    public double getScaleMMtoPX() {
        return scaleMMtoPX;
    }

    public void setScaleMMtoPX(float scaleMMtoPX) {
        this.scaleMMtoPX = scaleMMtoPX;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public double getPx() {
        return px;
    }

    public void setPx(float px) {
        this.px = px;
    }

    public double getPy() {
        return py;
    }

    public void setPy(float py) {
        this.py = py;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }


}
