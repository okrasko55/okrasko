package com.itech.http.httprequeststesting.restapi.model.friends;


import com.itech.http.httprequeststesting.restapi.model.BaseResponse;

/**
 * Created by Natalia on 14.08.2015.
 */
public class AddFriendResponse extends BaseResponse {

    private String id;
    private String from_user_id;
    private String to_user_id;

    public AddFriendResponse() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFromUserId() {
        return from_user_id;
    }

    public void setFromUserId(String fromUserId) {
        this.from_user_id = fromUserId;
    }

    public String getToUserId() {
        return to_user_id;
    }

    public void setToUserId(String toUserId) {
        this.to_user_id = toUserId;
    }
}
