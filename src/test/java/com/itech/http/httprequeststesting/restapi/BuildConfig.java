package com.itech.http.httprequeststesting.restapi;

/**
 * Created by Igor on 23.02.2017.
 */

public final class BuildConfig {
    public static final boolean DEBUG = Boolean.parseBoolean("true");
    public static final String APPLICATION_ID = "com.smartnavigationsystems.mallapp.mallapplication";
    public static final String BUILD_TYPE = "debug";
    public static final String FLAVOR = "dev";
    public static final int VERSION_CODE = 8;
    public static final String VERSION_NAME = "1.0.2";
    // Fields from product flavor: dev
    public static final String API_VERSION_OLD = "v2";
    public static final String API_VERSION_NEW = "v3";
    public static final String GOOLE_API_ENDPOINT = "https://maps.googleapis.com";
    public static final String HOST = "http://dev.sns.ae";
    public static final String SOCKET_ADDRESS = "http://dev.sns.ae:3001";
    public static final String SOCKET_VERSION = "v2";
    public static final String URL_OLD;
    public static final String URL_NEW;


    static {
        URL_OLD = String.format("%s/api/%s/", BuildConfig.HOST, BuildConfig.API_VERSION_OLD);
        URL_NEW = String.format("%s/api/%s/", BuildConfig.HOST, BuildConfig.API_VERSION_NEW);
    }
}