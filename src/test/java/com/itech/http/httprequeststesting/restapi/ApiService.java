package com.itech.http.httprequeststesting.restapi;

import com.google.gson.JsonObject;
import com.itech.http.httprequeststesting.restapi.model.BaseResponse;
import com.itech.http.httprequeststesting.restapi.model.BaseResponseSync;
import com.itech.http.httprequeststesting.restapi.model.InviteResponseModel;
import com.itech.http.httprequeststesting.restapi.model.car.CarObject;
import com.itech.http.httprequeststesting.restapi.model.child.ChildObject;
import com.itech.http.httprequeststesting.restapi.model.friends.AddFriendResponse;
import com.itech.http.httprequeststesting.restapi.model.friends.FriendCountResponse;
import com.itech.http.httprequeststesting.restapi.model.friends.FriendRequestObject;
import com.itech.http.httprequeststesting.restapi.model.friends.FriendsRequestsList;
import com.itech.http.httprequeststesting.restapi.model.googleApi.GeocodeResponse;
import com.itech.http.httprequeststesting.restapi.model.routes.RouteObject;
import com.itech.http.httprequeststesting.restapi.model.sign.SignResponse;
import com.itech.http.httprequeststesting.restapi.model.theme.ThemeObject;
import com.itech.http.httprequeststesting.restapi.model.user.UserResponse;
import com.itech.http.httprequeststesting.restapi.model.user.UsersListResponse;
import com.itech.http.httprequeststesting.restapi.model.venues.VenueObject;
import com.itech.http.httprequeststesting.restapi.model.venues.VenueSyncModel;
import com.itech.http.httprequeststesting.restapi.model.venues.VenueSyncObject;
import com.itech.http.httprequeststesting.restapi.model.venues.navigation.RoutesResponse;
import com.itech.http.httprequeststesting.restapi.model.venues.products.GlobalSearchProductModel;
import com.itech.http.httprequeststesting.restapi.model.venues.products.ProductsListResponse;
import com.itech.http.httprequeststesting.restapi.model.venues.shops.ShopObject;
import com.itech.http.httprequeststesting.restapi.model.venues.shops.ShopsListResponse;
import com.itech.http.httprequeststesting.restapi.model.venues.typescategories.TypesCategoriesListResponse;
import com.itech.http.httprequeststesting.restapi.model.version.VersionResponse;
import com.squareup.okhttp.RequestBody;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;
import rx.Observable;

/**
 * Created by Igor on 23.02.2017.
 */

public interface ApiService {
    String CONTENT_TYPE_APPLICATION_JSON_CHARSET_UTF_8 = "Content-Type: application/json;charset=UTF-8";

    //Login/registration requests

    @FormUrlEncoded
    @POST(APIConstants.AUTH_LOGIN)
    Observable<SignResponse> postSignRequest(@FieldMap Map<String, String> params  );


    @GET(APIConstants.AUTH_LOGOUT)
    Observable<BaseResponse> postLogout(@Query(APIConstants.TOKEN_PARAM) String token);

    @FormUrlEncoded
    @POST(APIConstants.AUTH_PASSWORD_RECOVER)
    Observable<SignResponse> postPasswordRecover(@FieldMap Map<String, String> params);

//    @FormUrlEncoded
//    @POST(APIConstants.AUTH_PASSWORD_RECOVER)
//    void postPasswordRecover(@FieldMap Map<String, String> params Observable<SignResponse> );

    @FormUrlEncoded
    @POST(APIConstants.AUTH_RECOVER_CODE)
    Observable<SignResponse> postRecoverCode(@FieldMap Map<String, String> params  );


    @FormUrlEncoded
    @POST(APIConstants.AUTH_REGISTER)
    Observable<SignResponse> postRegisterRequest(@FieldMap Map<String, String> params  );


    @Multipart
    @POST(APIConstants.AUTH_REGISTER)
    Observable<SignResponse> postRegisterRequest(@PartMap Map<String, String> params, @Part(APIConstants.PHOTO_PARAM) RequestBody  file,
                             @Part(APIConstants.PHOTO_PARAM_LANDSCAPE) RequestBody  file_land                              );


    //Venues lists requests


    @FormUrlEncoded
    @POST(APIConstants.VENUES_NEAREST_LIST)
    Observable<List<VenueObject>> getVenuesNearestList(@Query(APIConstants.TOKEN_PARAM) String token, @Field(APIConstants.LATITUDE_PARAM) double latutude,
                              @Field(APIConstants.LONGITUDE_PARAM) double longitude );

    @FormUrlEncoded
    @POST(APIConstants.VENUES_SYNC)
    Observable<BaseResponseSync<VenueSyncObject>> getVenuesSyncList(@Query(APIConstants.TOKEN_PARAM) String token, @Field(APIConstants.SINCE_PARAM) Long since);

    @FormUrlEncoded
    @POST(APIConstants.VENUE_ID_SYNC)
    Observable<VenueSyncModel> getVenueSync(@Path(APIConstants.PATH_ID) Integer id, @Query(APIConstants.TOKEN_PARAM) String token, @Field(APIConstants.SINCE_PARAM) String since);


    @FormUrlEncoded
    @POST(APIConstants.FLOOR_SEARCh)
    Observable<Response> getFloorSearch(@Query(APIConstants.TOKEN_PARAM) String token, @Field(APIConstants.MALL_ID_PARAM) String mallId);

   //    @Headers({CONTENT_TYPE_APPLICATION_JSON_CHARSET_UTF_8})
    @GET(APIConstants.VENUE_DATA)
    Observable<VenueObject> getVenueData(@Path(APIConstants.PATH_ID) String venueId, @Query(APIConstants.TOKEN_PARAM) String token, @Query(APIConstants.EXPAND_PARAM) String expand);

    //User profile requests

    //    @Headers({CONTENT_TYPE_APPLICATION_JSON_CHARSET_UTF_8})
    @GET(APIConstants.USER_GET_DATA)
    Observable<UserResponse> getUserData(@Query(APIConstants.TOKEN_PARAM) String token, @Path(APIConstants.PATH_ID) String userId  );

    @GET(APIConstants.USER_GET_DATA)
    Observable<UserResponse> getUserProfile(@Query(APIConstants.TOKEN_PARAM) String token, @Path(APIConstants.PATH_ID) String userId);

    @Multipart
    @POST(APIConstants.USER_PUT_DATA)
    Observable<UserResponse> putUserData(@Path("id") String userId, @PartMap Map<String, String> params, @Part(APIConstants.PHOTO_PARAM) RequestBody  file, @Part(APIConstants.PHOTO_PARAM_LANDSCAPE) RequestBody  file_land
                      );

    @Multipart
    @POST(APIConstants.USER_PHOTO)
    Observable<SignResponse> postUserPhoto(@Query(APIConstants.TOKEN_PARAM) String token, @Path(APIConstants.PATH_ID) String userId,
                       @Part(APIConstants.PHOTO_PARAM) RequestBody  file, @Part(APIConstants.PHOTO_PARAM_LANDSCAPE) RequestBody  file_land  );


    @FormUrlEncoded
    @POST(APIConstants.USER_SEARCH)
    Observable<UsersListResponse> getUsersSearchList(@Query(APIConstants.TOKEN_PARAM) String token,
                            @Query(APIConstants.SORT_PARAM) String sort, @Field(APIConstants.SEARCH_PARAM) String searchText,
                            @Field(APIConstants.FRIENDS_PARAM) double friendParam  );

    @FormUrlEncoded
    @POST(APIConstants.USER_DEVICE)
    Observable<BaseResponse> subscrubeUserDeviceToPush(@Path(APIConstants.PATH_ID) String userId, @Query(APIConstants.TOKEN_PARAM) String token,
                                                          @Field(APIConstants.DEVICE_TOKEN) String deviceToken, @Field(APIConstants.DEVICE_PUSH_SEVICE) String devicePushType);


    @FormUrlEncoded
    @POST(APIConstants.TYPES_SEARCH_LIST)
    Observable<TypesCategoriesListResponse> getTypesSearchList(@Query(APIConstants.TOKEN_PARAM) String token, @Query(APIConstants.PAGE_PARAM) int page,
                            @Query(APIConstants.SORT_PARAM) String sort, @Query(APIConstants.EXPAND_PARAM) String expand,
                            @Field(APIConstants.MALL_ID_PARAM) String mallId, @Field(APIConstants.SEARCH_PARAM) String searchText,
                            @Field(APIConstants.CREATED_AT_PARAM) String createdAt, @Field(APIConstants.UPDATED_AT_PARAM) String updatedAt);


    @FormUrlEncoded
    @POST(APIConstants.CATEGORIES_SEARCH_LIST)
    Observable<TypesCategoriesListResponse> getCategoriesSearchList(@Query(APIConstants.TOKEN_PARAM) String token, @Query(APIConstants.PAGE_PARAM) int page,
                                 @Query(APIConstants.SORT_PARAM) String sort, @Query(APIConstants.EXPAND_PARAM) String expand,
                                 @Field(APIConstants.MALL_ID_PARAM) String mallId, @Field(APIConstants.SEARCH_PARAM) String searchText,
                                 @Field(APIConstants.CREATED_AT_PARAM) String createdAt, @Field(APIConstants.UPDATED_AT_PARAM) String updatedAt,
                                 @Field(APIConstants.TYPE_PARAM) String type  );

    @FormUrlEncoded
    @POST(APIConstants.PRODUCTS_SEARCH_LIST)
    Observable<ProductsListResponse> getProductsSearchList(@Query(APIConstants.TOKEN_PARAM) String token, @Query(APIConstants.PAGE_PARAM) int page,
                               @Query(APIConstants.SORT_PARAM) String sort, @Query(APIConstants.EXPAND_PARAM) String expand,
                               @Query(APIConstants.EXCLUDE_PARAM) String exclude, @Query(APIConstants.FIELDS_PARAM) String fields,
                               @Field(APIConstants.MALL_ID_PARAM) String mallId, @Field(APIConstants.SEARCH_PARAM) String searchText);

    @FormUrlEncoded
    @POST(APIConstants.SHOPS_SEARCH_LIST)
    Observable<ShopsListResponse> getFacilitySearchList(@Query(APIConstants.TOKEN_PARAM) String token, @Query(APIConstants.PAGE_PARAM) int page,
                                                        @Query(APIConstants.SORT_PARAM) String sort, @Query(APIConstants.EXPAND_PARAM) String expand,
                                                        @Query(APIConstants.EXCLUDE_PARAM) String exclude, @Query(APIConstants.FIELDS_PARAM) String fields,
                                                        @Field(APIConstants.MALL_ID_PARAM) String mallId, @Field(APIConstants.SEARCH_PARAM) String searchText);


    @FormUrlEncoded
    @POST(APIConstants.SHOPS_SEARCH_LIST)
    Observable<ShopsListResponse> getShopsSearchList(@Query(APIConstants.TOKEN_PARAM) String token, @Query(APIConstants.PAGE_PARAM) int page,
                            @Query(APIConstants.SORT_PARAM) String sort, @Field(APIConstants.MALL_ID_PARAM) String venueId,
                            @Field(APIConstants.TYPE_ID_PARAM) String typeId, @Field(APIConstants.CATEGORY_ID_PARAM) String categoryId,
                            @Field(APIConstants.SEARCH_PARAM) String searchText  );




    @GET(APIConstants.ROUTES_SEARCH)
    Observable<RoutesResponse> getRoutesPixels(@Query(APIConstants.TOKEN_PARAM) String token, @Query(APIConstants.MALL_ID_PARAM) String mallId,
                                               @Query(APIConstants.ROOT_FROM_X_PARAM) double fromX, @Query(APIConstants.ROOT_FROM_Y_PARAM) double fromY,
                                               @Query(APIConstants.ROOT_FROM_Z_PARAM) long fromZ, @Query(APIConstants.ROOT_TO_X_PARAM) double toX,
                                               @Query(APIConstants.ROOT_TO_Y_PARAM) double toY, @Query(APIConstants.ROOT_TO_Z_PARAM) int toZ);


    @GET(APIConstants.ROUTES_TO_ENTRY)
    Observable<RoutesResponse> getRouteToEntryPoint(@Query(APIConstants.TOKEN_PARAM) String token, @Query(APIConstants.MALL_ID_PARAM) String mallId,
                                    @Query(APIConstants.ROOT_FROM_X_PARAM) double fromX, @Query(APIConstants.ROOT_FROM_Y_PARAM) double fromY,
                                    @Query(APIConstants.ROOT_FROM_Z_PARAM) long fromZ, @Query(APIConstants.ROOT_ENTRY_ID) int toEntryId);


    @GET(APIConstants.ROUTES_TO_ENTRY)
    Observable<RoutesResponse> routeToEntryPoint(@Query(APIConstants.TOKEN_PARAM) String token, @Query(APIConstants.MALL_ID_PARAM) String mallId,
                                                 @Query(APIConstants.ROOT_FROM_X_PARAM) double fromX, @Query(APIConstants.ROOT_FROM_Y_PARAM) double fromY,
                                                 @Query(APIConstants.ROOT_FROM_Z_PARAM) long fromZ, @Query(APIConstants.ROOT_ENTRY_ID) int toEntryId);

    @GET(APIConstants.ROUTES_TO_NEAREST_EXIT)
    Observable<RoutesResponse> routeToNearestExit(@Query(APIConstants.TOKEN_PARAM) String token, @Query(APIConstants.MALL_ID_PARAM) String mallId,
                                                  @Query(APIConstants.ROOT_FROM_X_PARAM) double fromX, @Query(APIConstants.ROOT_FROM_Y_PARAM) double fromY,
                                                  @Query(APIConstants.ROOT_FROM_Z_PARAM) long fromZ);

    @GET(APIConstants.ROUTES_FROM_NEAREST_EXIT)
    Observable<RoutesResponse> routeFromNearestExit(@Query(APIConstants.TOKEN_PARAM) String token, @Query(APIConstants.MALL_ID_PARAM) String mallId,
                                                    @Query(APIConstants.ROOT_TO_X_PARAM) double toX, @Query(APIConstants.ROOT_TO_Y_PARAM) double toY,
                                                    @Query(APIConstants.ROOT_TO_Z_PARAM) long toZ);

    @GET(APIConstants.ROUTES_SEARCH)
    Observable<RoutesResponse> routeToProduct(@Query(APIConstants.TOKEN_PARAM) String token, @Query(APIConstants.MALL_ID_PARAM) String mallId,
                                              @Query(APIConstants.ROOT_FROM_X_PARAM) double fromX, @Query(APIConstants.ROOT_FROM_Y_PARAM) double fromY,
                                              @Query(APIConstants.ROOT_FROM_Z_PARAM) long fromZ, @Query(APIConstants.ROOT_TO_X_PARAM) double toX,
                                              @Query(APIConstants.ROOT_TO_Y_PARAM) double toY, @Query(APIConstants.ROOT_TO_Z_PARAM) int toZ);

    //Friends requests
    @FormUrlEncoded
    @POST(APIConstants.FRIEND_REQUEST)
    Observable<AddFriendResponse> postFriendRequestToUser(@Query(APIConstants.TOKEN_PARAM) String token, @Field(APIConstants.TO_USER_ID_PARAM) String userId);


    @GET(APIConstants.FRIEND_REQUEST_TO_ME)
    Observable<FriendsRequestsList> getFriendsRequestToCurrentUser(@Query(APIConstants.TOKEN_PARAM) String token, @Query(APIConstants.PAGE_PARAM) int page,
                                        @Query(APIConstants.SORT_PARAM) String sort);

    @GET(APIConstants.FRIEND_REQUEST_FROM_ME)
    Observable<FriendsRequestsList> getFriendsRequestFromCurrentUser(@Query(APIConstants.TOKEN_PARAM) String token, @Query(APIConstants.PAGE_PARAM) int page,
                                          @Query(APIConstants.SORT_PARAM) String sort);

    @FormUrlEncoded
    @POST(APIConstants.FRIEND_REQUEST_TO_ME_COUNT)
    Observable<FriendCountResponse> getFriendsRequestToMeCount(@Query(APIConstants.TOKEN_PARAM) String token, @Field(APIConstants.TO_USER_ID_PARAM) String touUserId);


    @FormUrlEncoded
    @POST(APIConstants.FRIEND_REQUEST_APPROVE)
    Observable<FriendRequestObject> approveFriendRequest(@Query(APIConstants.TOKEN_PARAM) String token, @Field(APIConstants.PATH_ID) String requestId);

    @DELETE(APIConstants.FRIEND_REQUEST_DELETE)
    Observable<FriendRequestObject> deleteFriendRequest(@Query(APIConstants.TOKEN_PARAM) String token, @Path(APIConstants.PATH_ID) String requestId);


    @FormUrlEncoded
    @POST(APIConstants.FRIENDS_DELETE_USER_DELETE)
    Observable<FriendRequestObject> postDeleteFriend(@Query(APIConstants.TOKEN_PARAM) String token, @Field(APIConstants.PATH_ID) String friendId);

    @Multipart
    @POST(APIConstants.CHILD_CREATE)
    Observable<ChildObject>  postChildCreate(@PartMap Map<String, String> params, @Part(APIConstants.PHOTO_PARAM) RequestBody  file,
                         @Part(APIConstants.PHOTO_PARAM_LANDSCAPE) RequestBody  file_land);

    @DELETE(APIConstants.CHILD_DELETE)
    Observable<ChildObject> deleteChild(@Query(APIConstants.TOKEN_PARAM) String token, @Path(APIConstants.PATH_ID) String childId);

    @Multipart
    @POST(APIConstants.CHILD_UPDATE)
    Observable<ChildObject>  postChildUpdate(@PartMap Map<String, String> params, @Part(APIConstants.PHOTO_PARAM) RequestBody  file,
                         @Part(APIConstants.PHOTO_PARAM_LANDSCAPE) RequestBody  file_land);


    @FormUrlEncoded
    @POST(APIConstants.BEACONS_SYNC)
    Observable<Response> postBeaconsSync(@Query(APIConstants.TOKEN_PARAM) String token, @Field(APIConstants.SINCE_PARAM) String since, @Field(APIConstants.AREA_PARAM) String area);

    @FormUrlEncoded
    @POST(APIConstants.USER_PROFILE_SYNC)
    Observable<Response> postProfileDataSync(@Query(APIConstants.TOKEN_PARAM) String token, @Field(APIConstants.SINCE_PARAM) String since);

    @FormUrlEncoded
    @POST(APIConstants.SUBSCRIPTIONS_SYNC)
    Observable<Response> postSubscriptionsSync(@Query(APIConstants.TOKEN_PARAM) String token, @Field(APIConstants.SINCE_PARAM) String since);


    @Multipart
    @POST(APIConstants.CAR_CREATE)
    Observable<CarObject> postCarCreate(@PartMap Map<String, String> params, @Part(APIConstants.PHOTO_PARAM) RequestBody  file,
                                        @Part(APIConstants.PHOTO_PARAM_LANDSCAPE) RequestBody file_land);

    @DELETE(APIConstants.CAR_DELETE)
    Observable<CarObject> deleteCar(@Query(APIConstants.TOKEN_PARAM) String token, @Path(APIConstants.PATH_ID) String carId);

    @Multipart
    @POST(APIConstants.CAR_UPDATE)
    Observable<CarObject> putCarUpdate(@Query(APIConstants.TOKEN_PARAM) String token, @Path(APIConstants.PATH_ID) String carId,
                                       @PartMap Map<String, Object> params, @Part(APIConstants.PHOTO_PARAM) RequestBody  file,
                                       @Part(APIConstants.PHOTO_PARAM_LANDSCAPE) RequestBody  file_land);

    //THEMES REQUESTS
    @FormUrlEncoded
    @POST(APIConstants.THEMES_SYNC)
    Observable<Response> getThemeSyncList(@Query(APIConstants.TOKEN_PARAM) String token, @Query(APIConstants.EXPAND_PARAM) String expand,
                                          @Field(APIConstants.SINCE_PARAM) String since);

    @POST(APIConstants.THEMES_SEARCH)
    Observable<List<ThemeObject>> getThemeList(@Query(APIConstants.TOKEN_PARAM) String token);

    @FormUrlEncoded
    @POST(APIConstants.INVITE_APPROVE)
    Observable<InviteResponseModel> getInviteApprove(@Query(APIConstants.TOKEN_PARAM) String token, @Field(APIConstants.INVITE_TOKEN) String inviteToken);

    //    Google maps destinations API request
    @GET(APIConstants.GET_ROUTE)
    Observable<RouteObject> getRoute(@Query("origin") String origin,
                                     @Query("destination") String destination,
                                     @Query("key") String apiKey,
                                     @Query("mode") String mode);

    //    Google maps destinations API request
    @GET(APIConstants.GET_GEOCODE)
    Observable<GeocodeResponse> getGeocode(@Query("latlng") String latlng, @Query("key") String apiKey
    );

    @FormUrlEncoded
    @POST(APIConstants.SEARCH_SHOP_ACCESS)
    Observable<List<ShopObject>> getSearchFacility(@Query(APIConstants.TOKEN_PARAM) String token, @Field(APIConstants.AREA_PARAM) String area, @Field(APIConstants.SEARCH_PARAM) String search);

    @FormUrlEncoded
    @POST(APIConstants.SEARCH_PRODUCT_ACCESS)
    Observable<GlobalSearchProductModel> getSearchProducts(@Query(APIConstants.TOKEN_PARAM) String token, @Field(APIConstants.AREA_PARAM) String area, @Field(APIConstants.SEARCH_PARAM) String search);

    @GET(APIConstants.APP_VERSION)
    Observable<VersionResponse> getLastAppVersion();

    @POST
    Observable<BaseResponse> saveFloorPlan (@Url String url, @Header("Cookie") String session, @Body JsonObject params);

}
