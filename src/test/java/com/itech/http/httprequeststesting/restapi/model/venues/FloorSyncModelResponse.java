package com.itech.http.httprequeststesting.restapi.model.venues;

import com.itech.http.httprequeststesting.restapi.model.BaseResponseSync;
import com.itech.http.httprequeststesting.restapi.model.venues.floor.Floor;

/**
 * Created by Администратор on 11.03.2017.
 */

public class FloorSyncModelResponse extends BaseResponseSync<Floor> {
}