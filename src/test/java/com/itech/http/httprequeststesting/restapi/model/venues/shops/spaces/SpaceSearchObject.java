package com.itech.http.httprequeststesting.restapi.model.venues.shops.spaces;


import com.itech.http.httprequeststesting.restapi.model.venues.products.ProductsObject;

import java.util.List;

/**
 * Created by barbinkh on 16.03.17.
 */

public class SpaceSearchObject extends Spaces {
    private List<ProductsObject> products;

    public List<ProductsObject> getProducts() {
        return products;
    }

    public void setProducts(List<ProductsObject> products) {
        this.products = products;
    }
}
