package com.itech.http.httprequeststesting.restapi.tests;

import com.itech.http.httprequeststesting.DB.DataAccessObject;
import com.itech.http.httprequeststesting.DB.tables.AdminDataSet;
import com.itech.http.httprequeststesting.DB.tables.MallsDataSet;
import com.itech.http.httprequeststesting.DB.tables.TokenDataSet;
import com.itech.http.httprequeststesting.Logger.Log4jConfigure;
import com.itech.http.httprequeststesting.restapi.BuildConfig;
import com.itech.http.httprequeststesting.restapi.RestClient;
import com.itech.http.httprequeststesting.restapi.model.BaseResponseSync;
import com.itech.http.httprequeststesting.restapi.model.venues.VenueSyncObject;
import com.itech.http.httprequeststesting.restapi.requestmanager.RequestManager;
import com.itech.http.httprequeststesting.restapi.setupdata.SetupMalls;
import com.itech.http.httprequeststesting.restapi.utils.TimeUtils;
import com.itech.http.httprequeststesting.restapi.utils.UsersContainer;

import org.junit.After;
import org.junit.Assert;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;
import java.util.Random;

/**
 * Created by inevzorov on 3/22/17.
 */

public class ChildTests {
    private static Logger log;
    private UsersContainer usersContainer;
    private static RestClient clientNew;
    private String token;
    private List<MallsDataSet> addedMalls;
    private List<MallsDataSet> publicMalls;
    private List<MallsDataSet> privateMalls;
    private BaseResponseSync<VenueSyncObject> venueSyncResponseNew;
    private List<VenueSyncObject> venueNewList;
    private TokenDataSet tokenDataSet;

    @BeforeClass
    public static void init() {
        Log4jConfigure.configureSimpleConsole();
        log = Logger.getLogger(MallSyncTests.class);
        clientNew = new RestClient(BuildConfig.URL_NEW);
    }

    @Before
    public void setUp() throws Exception {
        usersContainer = new UsersContainer();
        Random random = new Random();
        //Set session token
        token = "77a2aaaa3f6e1c8f1200fbf9ee876b6440e" + random.nextInt(10000);
        tokenDataSet = new TokenDataSet(token, usersContainer.getUser().getId(), TimeUtils.getBeginningTimeStamp() + 31536000);
        DataAccessObject.addObject(tokenDataSet);

        log.info(token);

        addedMalls = SetupMalls.addMallsToDB();
        publicMalls = SetupMalls.getPublicMalls();
        privateMalls = SetupMalls.getPrivateMalls();
    }

    @After
    public void clearDB(){
        for (int i = 0; i < addedMalls.size(); i++) {
            DataAccessObject.delete(addedMalls.get(i));
        }
        usersContainer.clear();
    }

    @Test
    public void getListEntities() throws Exception {
        //Add relationship between private mall and logged in usersContainer
        DataAccessObject.addObject(new AdminDataSet(usersContainer.getUser().getId(), "mall", privateMalls.get(0).getId(), null, TimeUtils.getCurrentTimeStamp(), TimeUtils.getCurrentTimeStamp(), null));

        //Update mall update_at field in the DB
        privateMalls.get(0).setUpdatedAt(TimeUtils.getCurrentTimeStamp());
        DataAccessObject.update(privateMalls.get(0));

        //Sync malls
        venueSyncResponseNew = RequestManager.getVenueSyncResponse(token, privateMalls.get(0).getUpdatedAt() - 1, clientNew).getOnNextEvents().get(0);
        venueNewList = venueSyncResponseNew.getUpdatedList();
        boolean isPresent = false;
        //Search changed mall in the response
        for (int i = 0; i < venueNewList.size(); i++) {
            if (Integer.valueOf(venueNewList.get(i).getId()) == privateMalls.get(0).getId()) {
                isPresent = true;
            }
        }
        Assert.assertTrue("Private mall with id: [" + privateMalls.get(0).getId() + "] is ABSENT in the Updated list after USER has been ADDED TO ADMIN", isPresent);

        venueSyncResponseNew = RequestManager.getVenueSyncResponse(usersContainer.getChildList().get(0).getToken(), privateMalls.get(0).getUpdatedAt() - 1, clientNew).getOnNextEvents().get(0);
        venueNewList = venueSyncResponseNew.getUpdatedList();
        isPresent = false;
        //Search changed mall in the response
        for (int i = 0; i < venueNewList.size(); i++) {
            if (Integer.valueOf(venueNewList.get(i).getId()) == privateMalls.get(0).getId()) {
                isPresent = true;
            }
        }
        Assert.assertTrue("Private mall with id: [" + privateMalls.get(0).getId() + "] is ABSENT in the Updated list after CHILD has been ADDED TO ADMIN", isPresent);
    }
}
