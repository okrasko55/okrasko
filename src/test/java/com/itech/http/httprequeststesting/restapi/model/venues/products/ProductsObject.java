package com.itech.http.httprequeststesting.restapi.model.venues.products;


import com.itech.http.httprequeststesting.restapi.model.venues.shops.ShopObject;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by BARBIN on 30.07.2015.
 */
public class ProductsObject implements Comparator<ProductsObject> {

    public static final String NAME_FIELD = "name";
    public static final String DENY_STATUS = "deny";

    private String id;
    private String name;
    private String description;
    private String image;
    private String sku;
    private String status;
    private String image_url;
    private ArrayList<ShopObject> shopsWithTypes = new ArrayList<>();
    private ArrayList<TypeModel> shopsCategoriesTypes = new ArrayList<>();

    public ProductsObject() {
    }

    public ArrayList<TypeModel> getShopsCategoriesTypes() {
        return shopsCategoriesTypes;
    }

    public void setShopsCategoriesTypes(ArrayList<TypeModel> shopsCategoriesTypes) {
        this.shopsCategoriesTypes = shopsCategoriesTypes;
    }

    public ArrayList<ShopObject> getShopsWithTypes() {
        return shopsWithTypes;
    }

    public void setShopsWithTypes(ArrayList<ShopObject> shopsWithTypes) {
        this.shopsWithTypes = shopsWithTypes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    @Override
    public int compare(ProductsObject lhs, ProductsObject rhs) {
        return lhs.getName().compareTo(rhs.getName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductsObject that = (ProductsObject) o;

        return id.equals(that.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }


}
