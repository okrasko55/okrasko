package com.itech.http.httprequeststesting.restapi.model.version;


import com.itech.http.httprequeststesting.restapi.model.BaseResponse;

/**
 * Created by natalia on 2/2/17.
 */

public class VersionResponse extends BaseResponse {

    private String version;
    private String platform;

    public String getVersion() {
        return version;
    }
}
