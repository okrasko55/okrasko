package com.itech.http.httprequeststesting.restapi.setupdata;

import com.google.gson.reflect.TypeToken;
import com.itech.http.httprequeststesting.DB.DataAccessObject;
import com.itech.http.httprequeststesting.DB.tables.CategoryDataSet;
import com.itech.http.httprequeststesting.DB.tables.CategoryTypeDataSet;
import com.itech.http.httprequeststesting.DB.tables.FacilityCategoryDataSet;
import com.itech.http.httprequeststesting.DB.tables.FacilityDataSet;
import com.itech.http.httprequeststesting.DB.tables.Floor;
import com.itech.http.httprequeststesting.DB.tables.FloorDataSet;
import com.itech.http.httprequeststesting.DB.tables.MallsDataSet;
import com.itech.http.httprequeststesting.DB.tables.PlacementDataSet;
import com.itech.http.httprequeststesting.DB.tables.PlacementProductDataSet;
import com.itech.http.httprequeststesting.DB.tables.ProductCategoryDataSet;
import com.itech.http.httprequeststesting.DB.tables.ProductDataSet;
import com.itech.http.httprequeststesting.DB.tables.ProductFacilityDataSet;
import com.itech.http.httprequeststesting.DB.tables.TypeDataSet;
import com.itech.http.httprequeststesting.restapi.utils.JsonToJava;
import com.itech.http.httprequeststesting.restapi.utils.TimeUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * Created by Igor on 12.04.2017.
 */

public class SetUpMallData {

    public static MallsDataSet addMall(Double lng, Double ltd, Integer areaId, Integer access, Integer subscriptionId) {
        MallsDataSet mallsDataSet;

        Random random = new Random();
        mallsDataSet = new MallsDataSet("Auto Venue " + random.nextInt(1000), "", TimeUtils.getBeginningTimeStamp(),
                TimeUtils.getCurrentTimeStamp(), "", "", lng, ltd, "", "", "", "", "", null, areaId, access, TimeUtils.getBeginningTimeStamp(),
                subscriptionId);
        DataAccessObject.addObject(mallsDataSet);

        mallsDataSet.setFloorsList(new ArrayList<>());
        mallsDataSet.setTypesList(new ArrayList<>());
        mallsDataSet.setFacilitiesList(new ArrayList<>());

        return mallsDataSet;
    }

    public static FloorDataSet addFloor(MallsDataSet mall, Integer level, String type) {
        FloorDataSet floorDataSet;

        floorDataSet = new FloorDataSet(level, DefaultConstans.DEFAULT_WIDTH, DefaultConstans.DEFAULT_HEIGHT, null, mall.getId(),
                type, TimeUtils.getBeginningTimeStamp(), TimeUtils.getCurrentTimeStamp(),
                DefaultConstans.DEFAULT_MM_WIDTH, DefaultConstans.DEFAULT_MM_HEIGHT, null, null, null, null, null, null, null, null,
                null, null, DefaultConstans.DEFAULT_PX_PERIMETR, DefaultConstans.DEFAULT_PX_WALLS,
                DefaultConstans.DEFAULT_ANGLE_TO_NORTH, DefaultConstans.DEFAULT_DOORS, DefaultConstans.DEFAULT_EXITS,
                DefaultConstans.DEFAULT_PX_WALL_HEIGHT, DefaultConstans.DEFAULT_MM_WALL_HEIGHT);
        DataAccessObject.addObject(floorDataSet);
        mall.getFloorsList().add(floorDataSet);
        floorDataSet.setFacilitiesList(new ArrayList<>());
        return floorDataSet;
    }

    public static void addType(MallsDataSet mall, String name, String status) {
        TypeDataSet typeDataSet;
        List<CategoryDataSet> categoriesList = new ArrayList<>();

        typeDataSet = new TypeDataSet(name, null, TimeUtils.getBeginningTimeStamp(), TimeUtils.getCurrentTimeStamp(),
                null, mall.getId(), null, status, null, 0);
        DataAccessObject.addObject(typeDataSet);

        typeDataSet.setCategoriesList(categoriesList);
        mall.getTypesList().add(typeDataSet);
    }

    public static void addCategories(MallsDataSet mall, String fileName) throws Exception {
        CategoryDataSet categoryDataSet;


        List<CategoryDataSet> addedCategories = JsonToJava.getData(fileName, new TypeToken<List<CategoryDataSet>>() {
        }.getType());
        List<CategoryDataSet> categoriesList = new ArrayList<>();
        for (CategoryDataSet addedCategory : addedCategories) {
            categoryDataSet = new CategoryDataSet(addedCategory.getName(), addedCategory.getDescription(),
                    TimeUtils.getBeginningTimeStamp(), TimeUtils.getCurrentTimeStamp(), addedCategory.getImage(),
                    mall.getId(), null, addedCategory.getStatus(), null, addedCategory.getCopyId());
            DataAccessObject.addObject(categoryDataSet);
            categoriesList.add(categoryDataSet);
        }
        mall.setCategoriesList(categoriesList);
    }

    public static void linkCategoryToType(CategoryDataSet category, TypeDataSet type) {
        CategoryTypeDataSet categoryTypeDataSet;
        categoryTypeDataSet = new CategoryTypeDataSet(category.getId(), type.getId(),
                TimeUtils.getBeginningTimeStamp(), TimeUtils.getCurrentTimeStamp(), null);
        DataAccessObject.addObject(categoryTypeDataSet);
        type.getCategoriesList().add(category);
    }

    public static void addFacilitiesToFloor(MallsDataSet mall, String fileName, Integer level) throws Exception {

        List<FacilityDataSet> addedFacilities = JsonToJava.getData(fileName, new TypeToken<List<FacilityDataSet>>() {
        }.getType());

        FacilityDataSet facilityDataSet;

        List<FacilityDataSet> facilitiesListFloor = new ArrayList<>();
        for (FacilityDataSet addedFacility : addedFacilities) {
            facilityDataSet = new FacilityDataSet(addedFacility.getName(), addedFacility.getDescription(),
                    mall.getId(), TimeUtils.getBeginningTimeStamp(), TimeUtils.getCurrentTimeStamp(),
                    addedFacility.getWalls(), level, addedFacility.getImage(), addedFacility.getVertexes(),
                    addedFacility.getColor(), addedFacility.getMapImage(), addedFacility.getWidth(),
                    addedFacility.getHeight(), addedFacility.getMapOffsetX(), addedFacility.getMapOffsetY(),
                    addedFacility.getWallSize(), null, addedFacility.getPxWalls());
            DataAccessObject.addObject(facilityDataSet);
            facilitiesListFloor.add(facilityDataSet);
        }
        for (FloorDataSet floorDataSet : mall.getFloorsList()) {
            if (floorDataSet.getFloorLevel().equals(level)) {
                floorDataSet.setFacilitiesList(facilitiesListFloor);
            }
        }
        for (FacilityDataSet facilityFloor : facilitiesListFloor) {
            mall.getFacilitiesList().add(facilityFloor);
        }
    }


    public static void linkFacilityToCategories(FacilityDataSet facility, CategoryDataSet category) {
        FacilityCategoryDataSet facilityCategoryDataSet;
        facilityCategoryDataSet = new FacilityCategoryDataSet(facility.getId(),
                category.getId(), TimeUtils.getBeginningTimeStamp(),
                TimeUtils.getCurrentTimeStamp(), null);
        DataAccessObject.addObject(facilityCategoryDataSet);
        facility.getCategoriesList().add(category);
    }

    public static void addPointPlacementToFacility(FacilityDataSet facility, String fileName, Integer x, Integer y) throws Exception {
        PlacementDataSet placementDataSet;
        List<PlacementDataSet> placementsList = JsonToJava.getData(fileName, new TypeToken<List<PlacementDataSet>>() {
        }.getType());

        List<PlacementDataSet> placementDataSets = new ArrayList<>();
        for (int i = 0; i < placementsList.size(); i++) {
            placementDataSet = new PlacementDataSet(facility.getId(), null,
                    placementsList.get(i).getWalls(), placementsList.get(i).getPxWalls(),
                    TimeUtils.getBeginningTimeStamp(), TimeUtils.getCurrentTimeStamp(),
                    placementsList.get(i).getColor(), null, placementsList.get(i).getPxWallHeight(),
                    x, y);
            DataAccessObject.addObject(placementDataSet);

            placementDataSets.add(placementDataSet);
        }
        facility.setPlacementsList(placementDataSets);
    }

    public static void addProductsToFacility(Integer mallId, FacilityDataSet facility, String fileName) throws Exception {
        Random random = new Random();
        int rand = random.nextInt(1000);
        ProductDataSet productDataSet;

        List<ProductDataSet> products = JsonToJava.getData(fileName, new TypeToken<List<ProductDataSet>>() {
        }.getType());
            List<ProductDataSet> addedProductsFacility = new ArrayList<>();
            for (ProductDataSet product : products) {
                productDataSet = new ProductDataSet(mallId, product.getName() + rand++, product.getDescription(),
                        product.getImage(), TimeUtils.getBeginningTimeStamp(), TimeUtils.getCurrentTimeStamp(), product.getSku(), null);
                DataAccessObject.addObject(productDataSet);
                addedProductsFacility.add(productDataSet);

                linkProductFacility(productDataSet.getId(), facility.getId());
                linkProductToCategory(productDataSet.getId(), facility.getCategoriesList());
                linkProductToPlacement(productDataSet.getId(), facility.getPlacementsList());

            }
            facility.setProductsList(addedProductsFacility);

    }

    public static void linkProductFacility(Integer productId, Integer facilityId) {
        ProductFacilityDataSet facilityProducts;
        facilityProducts = new ProductFacilityDataSet(productId, facilityId, TimeUtils.getBeginningTimeStamp(),
                TimeUtils.getCurrentTimeStamp());
        DataAccessObject.addObject(facilityProducts);
    }

    public static void linkProductToCategory(Integer productId, List<CategoryDataSet> categories) {
        ProductCategoryDataSet productCategoryDataSet;
        for (CategoryDataSet category : categories) {
            productCategoryDataSet = new ProductCategoryDataSet(productId, category.getId(), TimeUtils.getBeginningTimeStamp(),
                    TimeUtils.getCurrentTimeStamp());
            DataAccessObject.addObject(productCategoryDataSet);
        }
    }

    public static void linkProductToPlacement(Integer productId, List<PlacementDataSet> placements) {
        PlacementProductDataSet placementProductDataSet;
        for (PlacementDataSet placement : placements) {
            placementProductDataSet = new PlacementProductDataSet(placement.getId(), productId, TimeUtils.getBeginningTimeStamp(),
                    TimeUtils.getCurrentTimeStamp());
            DataAccessObject.addObject(placementProductDataSet);
        }
    }

}
