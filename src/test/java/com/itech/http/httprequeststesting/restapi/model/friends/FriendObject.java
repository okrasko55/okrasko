package com.itech.http.httprequeststesting.restapi.model.friends;

import java.io.Serializable;

/**
 * Created by Natalia on 15.08.2015.
 */
public class FriendObject implements Serializable {

    public static final String FRIEND_NAME = "first_name";

    private String id;
    private String email;
    private String token;
    private String first_name;
    private String last_name;
    private String photo;
    private String photo_url;
    private String photo_landscape_url;
    private String phone_code;
    private String phone_value;
    private int profile_visible;
    private int location_access;
    private String x;
    private String y;
    private String z;
    private String lat;
    private String lng;
    private String in_mall;
    private String in_mall_time;
    private String created_at;
    private String updated_at;
    private String status;

    public FriendObject() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public int getLocation_access() {
        return location_access;
    }

    public void setLocation_access(int location_access) {
        this.location_access = location_access;
    }

    public int getProfile_visible() {
        return profile_visible;
    }

    public void setProfile_visible(int profile_visible) {
        this.profile_visible = profile_visible;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    public String getPhone_code() {
        return phone_code;
    }

    public void setPhone_code(String phone_code) {
        this.phone_code = phone_code;
    }

    public String getPhone_value() {
        return phone_value;
    }

    public void setPhone_value(String phone_value) {
        this.phone_value = phone_value;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public String getY() {
        return y;
    }

    public void setY(String y) {
        this.y = y;
    }

    public String getZ() {
        return z;
    }

    public void setZ(String z) {
        this.z = z;
    }

    public String getGps_latitude() {
        return lat;
    }

    public void setGps_latitude(String gps_latitude) {
        this.lat = gps_latitude;
    }

    public String getGps_longitude() {
        return lng;
    }

    public void setGps_longitude(String gps_longitude) {
        this.lng = gps_longitude;
    }

    public String getIn_mall() {
        return this.in_mall;
    }

    public void setIn_mall(String in_mall) {
        this.in_mall = in_mall;
    }

    public String getIn_mall_time() {
        return in_mall_time;
    }

    public void setIn_mall_time(String in_mall_time) {
        this.in_mall_time = in_mall_time;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserResponse{");
        sb.append(", id='").append(id).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", token='").append(token).append('\'');
        sb.append(", first_name='").append(first_name).append('\'');
        sb.append(", last_name='").append(last_name).append('\'');
        sb.append(", photo='").append(photo).append('\'');
        sb.append(", photo_url='").append(photo_url).append('\'');
        sb.append(", phone_code='").append(phone_code).append('\'');
        sb.append(", phone_value='").append(phone_value).append('\'');
        sb.append(", created_at='").append(created_at).append('\'');
        sb.append(", updated_at='").append(updated_at).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public String getPhoto_landscape_url() {
        return photo_landscape_url;
    }

    public void setPhoto_landscape_url(String photo_landscape_url) {
        this.photo_landscape_url = photo_landscape_url;
    }
}
