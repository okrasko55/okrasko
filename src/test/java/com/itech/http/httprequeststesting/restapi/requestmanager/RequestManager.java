package com.itech.http.httprequeststesting.restapi.requestmanager;

import com.google.gson.JsonObject;
import com.itech.http.httprequeststesting.restapi.RestClient;
import com.itech.http.httprequeststesting.restapi.model.BaseResponse;
import com.itech.http.httprequeststesting.restapi.model.BaseResponseSync;
import com.itech.http.httprequeststesting.restapi.model.InviteResponseModel;
import com.itech.http.httprequeststesting.restapi.model.sign.SignRequest;
import com.itech.http.httprequeststesting.restapi.model.sign.SignResponse;
import com.itech.http.httprequeststesting.restapi.model.venues.VenueSyncModel;
import com.itech.http.httprequeststesting.restapi.model.venues.VenueSyncObject;
import com.itech.http.httprequeststesting.restapi.model.venues.products.GlobalSearchProductModel;
import com.squareup.okhttp.RequestBody;

import org.json.JSONObject;

import java.util.Map;

import rx.observers.TestSubscriber;

/**
 * Created by Igor on 24.02.2017.
 */

public class RequestManager {
    public static SignResponse getSignInResponse(String login, String password, RestClient client) {
        Map<String, String> credentials;
        TestSubscriber<SignResponse> signInSubscriber = new TestSubscriber<>();
        credentials = new SignRequest("", login, "", "", password).convertToParamsMap();
        client.getApiService().postSignRequest(credentials).subscribe(signInSubscriber);
        SignResponse response = signInSubscriber.getOnNextEvents().get(0);
        return response;
    }

    public static TestSubscriber<BaseResponseSync<VenueSyncObject>> getVenueSyncResponse(String token, Long since, RestClient client) {
        TestSubscriber<BaseResponseSync<VenueSyncObject>> subscriber = new TestSubscriber<>();
        client.getApiService().getVenuesSyncList(token, since).subscribe(subscriber);
        return subscriber;
    }

    public static InviteResponseModel getInviteApproveResponse(String token, String inviteToken, RestClient client) {
        TestSubscriber<InviteResponseModel> subscriber = new TestSubscriber<>();
        client.getApiService().getInviteApprove(token, inviteToken).subscribe(subscriber);
        return subscriber.getOnNextEvents().get(0);
    }

    public static TestSubscriber<VenueSyncModel> getVenueIdSyncResponse(String token, Integer id, String since,  RestClient client){
        TestSubscriber<VenueSyncModel> subscriber = new TestSubscriber<>();
        client.getApiService().getVenueSync( id, token, since).subscribe(subscriber);
        return subscriber;
    }
    public static TestSubscriber<GlobalSearchProductModel> getGlobalSearchProducts(String token, String area, String value, RestClient client){
        TestSubscriber<GlobalSearchProductModel> subscriber = new TestSubscriber<>();
        client.getApiService().getSearchProducts( token, area, value).subscribe(subscriber);
        return subscriber;
    }

    public static void  saveFloorPlan(String url, JsonObject params, String session, RestClient client){
        TestSubscriber<BaseResponse> subscriber = new TestSubscriber<>();
        client.getApiService().saveFloorPlan( url, session, params).subscribe(subscriber);
    }

}
