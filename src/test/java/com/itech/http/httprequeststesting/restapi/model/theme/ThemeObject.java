package com.itech.http.httprequeststesting.restapi.model.theme;

import com.itech.http.httprequeststesting.restapi.model.BaseResponse;

/**
 * Created by BARBIN on 05.10.2015.
 */
public class ThemeObject extends BaseResponse {

    protected String id;
    protected String title;
    protected String theme_status;
    protected String mall_id;
    protected String title_color;
    protected String logo_image;
    protected String background_image;
    protected String background_color;
    protected String child_color;
    protected String car_color;
    protected String friend_color;
    protected String categories_color;
    protected String welcome_text;
    protected String welcome_text_color;
    protected String created_at;
    protected String updated_at;
    protected String background_image_url;
    protected String logo_image_url;

    public ThemeObject() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return theme_status;
    }

    public void setStatus(String theme_status) {
        this.theme_status = theme_status;
    }

    public String getMall_id() {
        return mall_id;
    }

    public void setMall_id(String mall_id) {
        this.mall_id = mall_id;
    }

    public String getTitle_color() {
        return title_color;
    }

    public void setTitle_color(String title_color) {
        this.title_color = title_color;
    }

    public String getLogo_image() {
        return logo_image;
    }

    public void setLogo_image(String logo_image) {
        this.logo_image = logo_image;
    }

    public String getBackground_image() {
        return background_image;
    }

    public void setBackground_image(String background_image) {
        this.background_image = background_image;
    }

    public String getBackground_color() {
        return background_color;
    }

    public void setBackground_color(String background_color) {
        this.background_color = background_color;
    }

    public String getChild_color() {
        return child_color;
    }

    public void setChild_color(String child_color) {
        this.child_color = child_color;
    }

    public String getCar_color() {
        return car_color;
    }

    public void setCar_color(String car_color) {
        this.car_color = car_color;
    }

    public String getFriend_color() {
        return friend_color;
    }

    public void setFriend_color(String friend_color) {
        this.friend_color = friend_color;
    }

    public String getCategories_color() {
        return categories_color;
    }

    public void setCategories_color(String categories_color) {
        this.categories_color = categories_color;
    }

    public String getWelcome_text() {
        return welcome_text;
    }

    public void setWelcome_text(String welcome_text) {
        this.welcome_text = welcome_text;
    }

    public String getWelcome_text_color() {
        return welcome_text_color;
    }

    public void setWelcome_text_color(String welcome_text_color) {
        this.welcome_text_color = welcome_text_color;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getBackground_image_url() {
        return background_image_url;
    }

    public void setBackground_image_url(String background_image_url) {
        this.background_image_url = background_image_url;
    }

    public String getLogo_image_url() {
        return logo_image_url;
    }

    public void setLogo_image_url(String logo_image_url) {
        this.logo_image_url = logo_image_url;
    }
}
