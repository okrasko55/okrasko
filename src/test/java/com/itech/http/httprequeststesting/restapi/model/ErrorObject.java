package com.itech.http.httprequeststesting.restapi.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by NataliLapshina on 19.05.2015.
 */
public class ErrorObject {
    private List<String> password;
    private List<String> email;
    private List<String> user;
    private List<String> code;
    private List<String> phone;
    private List<String> first_name;
    private List<String> last_name;
    private List<String> photo;
    private List<String> route;
    @SerializedName("location_access")
    private List<String> locationAccess;
    @SerializedName("profile_visible")
    private List<String> profileVisible;


    public List<String> getPassword() {
        return password;
    }

    public void setPassword(List<String> password) {
        this.password = password;
    }

    public List<String> getEmail() {
        return email;
    }

    public void setEmail(List<String> email) {
        this.email = email;
    }

    public List<String> getUser() {
        return user;
    }

    public void setUser(List<String> user) {
        this.user = user;
    }

    public List<String> getPhone() {
        return phone;
    }

    public void setPhone(List<String> phone) {
        this.phone = phone;
    }

    public List<String> getFirst_name() {
        return first_name;
    }

    public void setFirst_name(List<String> first_name) {
        this.first_name = first_name;
    }

    public List<String> getLast_name() {
        return last_name;
    }

    public void setLast_name(List<String> last_name) {
        this.last_name = last_name;
    }

    public List<String> getPhoto() {
        return photo;
    }

    public void setPhoto(List<String> photo) {
        this.photo = photo;
    }

    public List<String> getCode() {
        return code;
    }

    public void setCode(List<String> code) {
        this.code = code;
    }

    public List<String> getLocationAccess() {
        return locationAccess;
    }

    public List<String> getProfileVisible() {
        return profileVisible;
    }

    public List<String> getRoute() {
        return route;
    }

    public void setRoute(List<String> route) {
        this.route = route;
    }

    public String getError() {
        if (password != null && password.size() > 0) {
            return password.get(0);
        }
        if (email != null && email.size() > 0) {
            return email.get(0);
        }
        if (user != null && user.size() > 0) {
            return user.get(0);
        }
        if (code != null && code.size() > 0) {
            return code.get(0);
        }
        if (phone != null && phone.size() > 0) {
            return phone.get(0);
        }
        if (first_name != null && first_name.size() > 0) {
            return first_name.get(0);
        }
        if (last_name != null && last_name.size() > 0) {
            return last_name.get(0);
        }
        if (photo != null && photo.size() > 0) {
            return photo.get(0);
        }
        if (locationAccess != null && locationAccess.size() > 0) {
            return locationAccess.get(0);
        }
        if (profileVisible != null && profileVisible.size() > 0) {
            return profileVisible.get(0);
        }

        if (route != null && !route.isEmpty()){
            return route.get(0);
        }

        return null;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ErrorObject{");
        sb.append("password size=").append(password != null ? password.size() : 0);
        sb.append(", email size=").append(email != null ? email.size() : 0);
        sb.append(", user size=").append(user != null ? user : 0);
        sb.append(", code size=").append(code != null ? code : 0);
        sb.append(", phone size=").append(phone != null ? phone.size() : 0);
        sb.append(", first_name size=").append(first_name != null ? first_name.size() : 0);
        sb.append(", last_name size=").append(last_name != null ? last_name.size() : 0);
        sb.append(", photo size=").append(photo != null ? photo.size() : 0);
        sb.append('}');
        return sb.toString();
    }
}
