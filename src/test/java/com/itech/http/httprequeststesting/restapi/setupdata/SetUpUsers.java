package com.itech.http.httprequeststesting.restapi.setupdata;

import com.google.gson.reflect.TypeToken;
import com.itech.http.httprequeststesting.DB.DataAccessObject;
import com.itech.http.httprequeststesting.DB.tables.UserDataSet;
import com.itech.http.httprequeststesting.restapi.utils.JsonToJava;
import com.itech.http.httprequeststesting.restapi.utils.TimeUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by inevzorov on 3/23/17.
 */

public class SetUpUsers {
    private static UserDataSet user;
    private static List <UserDataSet> gotUsers;
    private static List <UserDataSet> addedUsers;

    public static List<UserDataSet> addUsers() throws Exception {
        gotUsers = JsonToJava.getData("user.json", new TypeToken<List<UserDataSet>>() {
        }.getType());
        addedUsers = new ArrayList<>();
        for (UserDataSet item : gotUsers) {
            user = new UserDataSet(item.getFirstName(), item.getLastName(), item.getAuth_key(),
                    item.getPhone(), item.getRole(), item.getPasswordHash(), item.getPasswordResetToken(),
                    item.getEmail(), item.getStatus(), TimeUtils.getBeginningTimeStamp(), TimeUtils.getCurrentTimeStamp(),
                    item.getPhoto(), item.getX(), item.getY(), item.getZ(), item.getLatitude(), item.getLongitude(),
                    item.getInMall(), item.getInMallTime(), item.getLocationAccess(), item.getProfileVisible(),
                    null, item.getVoiceNav(), item.getPhotoLandscape(), item.getSendPush(), item.getDeviceToken(),
                    item.getDeviceService(), item.getAcceptAds(), item.getBackgroundActive());
            DataAccessObject.addObject(user);
            addedUsers.add(user);
        }
        return addedUsers;
    }
}