package com.itech.http.httprequeststesting.restapi.model.venues.products;

/**
 * Created by barbinkh on 04.08.2015.
 */
public class ProductsForAdapterModel {

    private String typeObject;
    private CategoryModel categoryObject;
    private FacilityModel facilityObject;
    private ProductModel productObject;

    public ProductsForAdapterModel() {
        typeObject = "";
        categoryObject = new CategoryModel();
        facilityObject = new FacilityModel();
        productObject = new ProductModel();
    }

    public ProductsForAdapterModel(String typeObject, CategoryModel categoryObject, FacilityModel facilityObject, ProductModel productObject) {
        this.typeObject = typeObject;
        this.facilityObject = facilityObject;
        this.categoryObject = categoryObject;
        this.productObject = productObject;
    }

    public String getTypeObject() {
        return typeObject;
    }

    public void setTypeObject(String typeObject) {
        this.typeObject = typeObject;
    }

    public CategoryModel getCategoryObject() {
        return categoryObject;
    }

    public void setCategoryObject(CategoryModel categoryObject) {
        this.categoryObject = categoryObject;
    }

    public FacilityModel getFacilityObject() {
        return facilityObject;
    }

    public void setFacilityObject(FacilityModel facilityObject) {
        this.facilityObject = facilityObject;
    }

    public ProductModel getProductObject() {
        return productObject;
    }

    public void setProductObject(ProductModel productObject) {
        this.productObject = productObject;
    }

}
