package com.itech.http.httprequeststesting.restapi.model.venues.floor;


/**
 * Created by pavel on 11/19/15.
 */
public class NodeModel {
    public float x;
    public float y;
    public float z;


    public NodeModel(NodeModel nodeModel) {
        this.x = nodeModel.x;
        this.y = nodeModel.y;
    }

    public NodeModel() {
    }

    public NodeModel(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public NodeModel(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }


    public void setZ(float z) {
        this.z = z;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float[] getPointMas(float z) {
        return new float[]{x, y, z};
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NodeModel nodeModel = (NodeModel) o;

        if (Float.compare(nodeModel.x, x) != 0) return false;
        if (Float.compare(nodeModel.y, y) != 0) return false;
        return Float.compare(nodeModel.z, z) == 0;

    }

    public boolean equalsDelta(Object o, float delta) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NodeModel that = (NodeModel) o;

        if (Math.abs(that.x - x) > delta) return false;
        return (Math.abs(that.y - y) <= delta);

    }


    @Override
    public int hashCode() {
        int result = (x != +0.0f ? Float.floatToIntBits(x) : 0);
        result = 31 * result + (y != +0.0f ? Float.floatToIntBits(y) : 0);
        result = 31 * result + (z != +0.0f ? Float.floatToIntBits(z) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "NodeModel{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
}
