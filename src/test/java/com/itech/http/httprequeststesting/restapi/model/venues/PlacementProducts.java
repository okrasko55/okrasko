package com.itech.http.httprequeststesting.restapi.model.venues;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Администратор on 11.03.2017.
 */

public class PlacementProducts {

    @SerializedName("placement_id")
    private Long placementId;

    @SerializedName("product_ids")
    private List<Long> productsIds;

    public List<Long> getProductsIds() {
        return productsIds;
    }

    public void setProductsIds(List<Long> productsIds) {
        this.productsIds = productsIds;
    }

    public Long getPlacementId() {
        return placementId;
    }

    public void setPlacementId(Long placementId) {
        this.placementId = placementId;
    }

}