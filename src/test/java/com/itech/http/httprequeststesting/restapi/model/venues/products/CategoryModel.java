package com.itech.http.httprequeststesting.restapi.model.venues.products;


import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by barbinkh on 03.08.2015.
 */
public class CategoryModel implements Comparator<CategoryModel> {
    public static final String NAME_FIELD = "name";
    public static final String DENY_STATUS = "deny";

    private String id;
    private String name;
    private String image;
    private String status;
    private String image_url;
    private String description;
    private String created_at;
    private String updated_at;
    private ArrayList<FacilityModel> facility = new ArrayList<>();


    public ArrayList<FacilityModel> getFacilityList() {
        return facility;
    }

    public void setFacilityList(ArrayList<FacilityModel> facility) {
        this.facility = facility;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public int compare(CategoryModel lhs, CategoryModel rhs) {
        return lhs.getName().compareTo(rhs.getName());
    }


    @Override
    public boolean equals(Object aThat) {
        if (!(aThat instanceof CategoryModel)) {
            return false;
        }

        CategoryModel that = (CategoryModel) aThat;

        return
                false;//EqualsUtil.areEqual(this.id, that.id);
    }


    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

}
