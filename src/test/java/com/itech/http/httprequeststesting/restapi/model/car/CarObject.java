package com.itech.http.httprequeststesting.restapi.model.car;


import com.itech.http.httprequeststesting.restapi.model.BaseResponse;

/**
 * Created by BARBIN on 07.09.2015.
 */
public class CarObject extends BaseResponse {
    private Integer id;
    private String name;
    private String number;
    private String photo_url;
    private String photo_landscape_url;
    private String description;
    private Integer in_mall;
    private Integer x;
    private Integer y;
    private Integer z;
    private Double lat;
    private Double lng;
    private double leave_here;
    private String userID;

    public CarObject() {
    }



    public int getId() {
        if (id == null)
            id = 0;
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public void setGps_longitude(double gps_longitude) {
        this.lng = gps_longitude;
    }

    public double getLeave_here() {
        return leave_here;
    }

    public void setLeave_here(int leave_here) {
        this.leave_here = leave_here;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getPhoto_landscape_url() {
        return photo_landscape_url;
    }

    public void setPhoto_landscape_url(String photo_landscape_url) {
        this.photo_landscape_url = photo_landscape_url;
    }

   /* public SearchItem convertInstance() {
        SearchItem item = new SearchItem();
        item.id = id;
        item.name = name;
        item.latitude = lng;
        item.longitude = lat;
        item.imageUrl = photo_url;
        item.inMallId = in_mall;

        return item;
    }*/
}
