package com.itech.http.httprequeststesting.restapi.utils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Igor on 27.02.2017.
 */

public class TimeUtils {
    private final static Long beginningTimeStamp = getCurrentTimeStamp();

    public static String getCurrentTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        Calendar calendar = new GregorianCalendar();
        //calendar.add(Calendar.MONTH, -5);
        return sdf.format(calendar.getTime());
    }

    public static String getPastTime(int unit, int value) {
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        Calendar calendar = new GregorianCalendar(1970, 1, 1, 0,0,0);
        calendar.add(unit, value);
        return sdf.format(calendar.getTime());
    }
    public static String getBeginTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        Calendar calendar = new GregorianCalendar(1970, 1, 1, 0,0,0);
        return sdf.format(calendar.getTime());
    }

    public static Long getCurrentTimeStamp() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        return Long.valueOf(timestamp.getTime()) / 1000;
    }

    public static Long getBeginningTimeStamp() {
        return beginningTimeStamp;
    }
}
