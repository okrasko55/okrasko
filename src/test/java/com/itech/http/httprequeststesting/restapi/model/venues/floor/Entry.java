package com.itech.http.httprequeststesting.restapi.model.venues.floor;

/**
 * Created by Pavel on 28.12.2016.
 */
public class Entry {
    private int id;
    private int shop_id;
    private double px;
    private double py;

    public Entry(double px, double py, int shop_id, int id) {
        this.px = px;
        this.py = py;
        this.shop_id = shop_id;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getShopId() {
        return shop_id;
    }

    public void setShopId(int shopId) {
        this.shop_id = shopId;
    }

    public double getPx() {
        return px;
    }

    public void setPx(double px) {
        this.px = px;
    }

    public double getPy() {
        return py;
    }

    public void setPy(double py) {
        this.py = py;
    }
}
