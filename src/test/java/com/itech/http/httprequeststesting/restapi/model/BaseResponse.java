package com.itech.http.httprequeststesting.restapi.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by NataliLapshina on 18.05.2015.
 */
public class BaseResponse {

    private static final String SUCCESS = "success";
    private static final String ERROR = "errors";


    @SerializedName("status")
    private String status;

    private ErrorObject errors;

    private String date;


    public BaseResponse() {

    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Return all errors for different instances
     * @return
     */
    public ErrorObject getErrors() {
        return errors;
    }

    public void setErrors(ErrorObject errors) {
        this.errors = errors;
    }

    public boolean isSuccess() {
        return SUCCESS.equals(status) || status == null;
    }

    public String getSyncDate() {
        return date;
    }

    public void setSyncDate(String syncDate) {
        this.date = syncDate;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BaseResponse{");
        sb.append("status='").append(status).append('\'');
        sb.append(", errors=").append(errors);
        sb.append('}');
        return sb.toString();
    }


}
