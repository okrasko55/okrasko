package com.itech.http.httprequeststesting.restapi.model.friends;

/**
 * Created by Pavel on 30.08.2016.
 */
public class FriendCountResponse {
    int count;

    public int getCount() {
        return count;
    }
}
