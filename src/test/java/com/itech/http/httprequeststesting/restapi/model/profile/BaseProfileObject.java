package com.itech.http.httprequeststesting.restapi.model.profile;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Pavel on 28.10.2015.
 */
public class BaseProfileObject<T> {

    @SerializedName("update")
    private List<T> updatedList;
    @SerializedName("delete")
    private List<Long> deletedIdList;

    public List<Long> getDeletedIdList() {
        return deletedIdList;
    }

    public List<T> getUpdatedList() {
        return updatedList;
    }
}
