package com.itech.http.httprequeststesting.network.models.user;

import com.itech.http.httprequeststesting.network.models.BaseResponse;

import java.io.Serializable;

/**
 * Created by Igor on 04.11.2016.
 */

public class UserResponse extends BaseResponse implements Serializable {

    private String name;
    private String message;
    private String code;
    private String type;
    private int profile_visible;
    private int location_access;
    private int voice_nav;
    private int send_push;
    private String id;
    private String email;
    private String token;
    private String first_name;
    private String last_name;
    private String photo;
    private String photo_url;
    private String photo_landscape_url;
    private String phone_code;
    private String phone_value;
    private String created_at;
    private String updated_at;

    private String x;
    private String y;
    private String z;
    private String gps_latitude;
    private String gps_longitude;
    private String in_mall;
    private String in_mall_time;

    public UserResponse() {
    }

    public UserResponse(String id, String email, String first_name, String last_name, String photo_url, String photo_landscape_url, String phone_code, String phone_value) {
        this.id = id;
        this.email = email;
        this.first_name = first_name;
        this.last_name = last_name;
        this.photo_url = photo_url;
        this.phone_code = phone_code;
        this.phone_value = phone_value;
        this.photo_landscape_url = photo_landscape_url;
    }

    public int getSend_push() {
        return send_push;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public String getY() {
        return y;
    }

    public void setY(String y) {
        this.y = y;
    }

    public String getZ() {
        return z;
    }

    public void setZ(String z) {
        this.z = z;
    }

    public String getGps_latitude() {
        return gps_latitude;
    }

    public void setGps_latitude(String gps_latitude) {
        this.gps_latitude = gps_latitude;
    }

    public String getGps_longitude() {
        return gps_longitude;
    }

    public void setGps_longitude(String gps_longitude) {
        this.gps_longitude = gps_longitude;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    public String getPhone_code() {
        return phone_code;
    }

    public void setPhone_code(String phone_code) {
        this.phone_code = phone_code;
    }

    public String getPhone_value() {
        return phone_value;
    }

    public void setPhone_value(String phone_value) {
        this.phone_value = phone_value;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getIn_mall() {
        return this.in_mall;
    }

    public void setIn_mall(String in_mall) {
        this.in_mall = in_mall;
    }

    public String getIn_mall_time() {
        return in_mall_time;
    }

    public void setIn_mall_time(String in_mall_time) {
        this.in_mall_time = in_mall_time;
    }

    public int getProfile_visible() {
        return profile_visible;
    }

    public void setProfile_visible(int profile_visible) {
        this.profile_visible = profile_visible;
    }

    public int getLocation_access() {
        return location_access;
    }

    public void setLocation_access(int location_access) {
        this.location_access = location_access;
    }

    public int getVoice_nav() {
        return voice_nav;
    }

    public void setVoice_nav(int voice_nav) {
        this.voice_nav = voice_nav;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserResponse{");
        sb.append("name='").append(name).append('\'');
        sb.append(", message='").append(message).append('\'');
        sb.append(", code='").append(code).append('\'');
        sb.append(", type='").append(type).append('\'');
        sb.append(", id='").append(id).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", token='").append(token).append('\'');
        sb.append(", first_name='").append(first_name).append('\'');
        sb.append(", last_name='").append(last_name).append('\'');
        sb.append(", photo='").append(photo).append('\'');
        sb.append(", photo_url='").append(photo_url).append('\'');
        sb.append(", phone_code='").append(phone_code).append('\'');
        sb.append(", phone_value='").append(phone_value).append('\'');
        sb.append(", created_at='").append(created_at).append('\'');
        sb.append(", updated_at='").append(updated_at).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public String getPhoto_landscape_url() {
        return photo_landscape_url;
    }

    public void setPhoto_landscape_url(String photo_landscape_url) {
        this.photo_landscape_url = photo_landscape_url;
    }

}