package com.itech.http.httprequeststesting.network.retrofit;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.itech.http.httprequeststesting.network.constants.APIConstants;
import com.itech.http.httprequeststesting.network.models.APIChildSyncModel;
import com.itech.http.httprequeststesting.network.models.APILoginModel;
import com.itech.http.httprequeststesting.network.models.APIPassRecoverModel;
import com.itech.http.httprequeststesting.network.models.APIRegistrationModel;
import com.itech.http.httprequeststesting.network.models.APITokenModel;
<<<<<<< HEAD:app/src/test/java/com/itech/http/httprequeststesting/network/retrofit/APIHttp.java
<<<<<<< HEAD
import com.itech.http.httprequeststesting.network.models.user.UserResponse;
=======
>>>>>>> 78264cb40b59466210a6f4b8e63ff2ed6dd7aa0c:src/test/java/com/itech/http/httprequeststesting/network/retrofit/APIHttp.java
import com.itech.http.httprequeststesting.network.models.venues.VenueSyncObjectList;
=======
import com.itech.http.httprequeststesting.network.models.ApiCarsResponsModel;
>>>>>>> eef3a42c4380aa361df946b62578b6539bd11767

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by barbinkh on 05.10.16.
 */

public class APIHttp {

    private static APIHttp APIHttp;
    private IAPIHttp iAPIHttp;

    public APIHttp() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(30, TimeUnit.SECONDS)
                .build();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(APIConstants.URLs.MAIN_URL)
                .client(client)
                .build();
        iAPIHttp = retrofit.create(IAPIHttp.class);
    }

    public static APIHttp getInstance() {
        if (APIHttp == null) {
            APIHttp = new APIHttp();
        }
        return APIHttp;
    }


    public Observable<Response<APILoginModel>> login(String login, String code, String phone,  String password) {
        return iAPIHttp.login(login, code, phone, password);
    }

    public Observable<Response<APIRegistrationModel>> registration( String firstName,  String lastName, String email,
                                                                   String phone_code, String phone_value,  String password,
                                                                   String photo , String photo_landscape) {
        return iAPIHttp.registration(firstName, lastName, email, phone_code,phone_value,  password, photo, photo_landscape);
    }

    public Observable<Response<APITokenModel>> token( String token) {
        return iAPIHttp.token(token);
    }

    public Observable<Response<APILoginModel>> invite( String token,  String inviteToken) {
        return iAPIHttp.invite(token, inviteToken);
    }

<<<<<<< HEAD:app/src/test/java/com/itech/http/httprequeststesting/network/retrofit/APIHttp.java
<<<<<<< HEAD
    public Observable<Response<ArrayList<APIChildSyncModel>>> childSync(@NonNull String token, @NonNull String since){
=======
    public Observable<Response<ArrayList<APIChildSyncModel>>> childSync( String token,  String since){
>>>>>>> 78264cb40b59466210a6f4b8e63ff2ed6dd7aa0c:src/test/java/com/itech/http/httprequeststesting/network/retrofit/APIHttp.java
        return iAPIHttp.childSync(token, since);
    }

    public  Observable<Response<VenueSyncObjectList>> getVenuesSyncList(@Query(APIConstants.URLs.TOKEN_PARAM) String token, @Field(APIConstants.URLs.SINCE_PARAM) String since) {
        return iAPIHttp.getVenuesSyncList(token, since);
    }
    /*Oleg's code*/
    public Observable<Response<APIPassRecoverModel>> recover (@NonNull String email){
        return iAPIHttp.recover(email);
    }
    /*end*/
=======
    public Observable<Response<ApiCarsResponsModel>> cars(@NonNull String token, @NonNull String name, @NonNull String number) {
        return iAPIHttp.cars(token, name, number);
    }
>>>>>>> eef3a42c4380aa361df946b62578b6539bd11767
}
