package com.itech.http.httprequeststesting.network.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alyona on 10.10.2016.
 */

public class APITokenModel {
    @SerializedName("status")
    private String status;

    @SerializedName("token")
    private String token;

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("photo_url")
    private String photo_url;

    @SerializedName("photo_landscape_url")
    private String photo_landscape_url;

    @SerializedName("id")
    private String id;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhotoUrl() {
        return photo_url;
    }

    public void setPhotoUrl(String photo_url) {
        this.photo_url = photo_url;
    }

    public String getPhotoLandscapeUrl() {
        return photo_landscape_url;
    }

    public void setPhotoLandscapeUrl(String photo_url) {
        this.photo_landscape_url = photo_landscape_url;
    }
}
