package com.itech.http.httprequeststesting.network.models;

import com.google.gson.annotations.SerializedName;
import com.itech.http.httprequeststesting.RegistrationTest;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Igor on 10.10.2016.
 */

public class APIErrorsModel {
    @SerializedName("status")
    private String status;

    @SerializedName("errors")
    private HashMap<String, List<String>> errors;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public HashMap<String, List<String>> getErrors() {
        return errors;
    }

    public void setErrors(HashMap<String, List<String>> errors) {
        this.errors = errors;
    }

    public void test() {

    }
}
