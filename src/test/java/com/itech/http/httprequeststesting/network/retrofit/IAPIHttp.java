package com.itech.http.httprequeststesting.network.retrofit;

import com.itech.http.httprequeststesting.network.constants.APIConstants;
import com.itech.http.httprequeststesting.network.models.APIChildSyncModel;
import com.itech.http.httprequeststesting.network.models.APILoginModel;
import com.itech.http.httprequeststesting.network.models.APIPassRecoverModel;
import com.itech.http.httprequeststesting.network.models.APIRegistrationModel;
import com.itech.http.httprequeststesting.network.models.APITokenModel;
<<<<<<< HEAD
import com.itech.http.httprequeststesting.network.models.venues.VenuesSyncResponseList;
import com.itech.http.httprequeststesting.network.models.user.UserResponse;
import com.itech.http.httprequeststesting.network.models.venues.VenueSyncObjectList;

import java.util.ArrayList;
=======
import com.itech.http.httprequeststesting.network.models.ApiCarsResponsModel;
>>>>>>> eef3a42c4380aa361df946b62578b6539bd11767

import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by barbinkh on 05.10.16.
 */

public interface IAPIHttp {
    @POST(APIConstants.URLs.LOGIN_URL)
    @FormUrlEncoded
    Observable<Response<APILoginModel>> login(@Field("email") String email,
                                             @Field("phone_code") String code,
                                             @Field("phone_value") String phone,
                                             @Field("password") String password);


    @FormUrlEncoded
    Observable<Response<APIRegistrationModel>> registration(@Field("first_name") String firstName,
                                                            @Field("last_name") String lastName,
                                                            @Field("email") String email,
                                                            @Field("phone_code") String phone_code,
                                                            @Field("phone_value") String phone_value,
                                                            @Field("password") String password,
                                                            @Field("photo") String photo,
                                                            @Field("photo_landscape") String photo_landscape);

    @POST(APIConstants.URLs.TOKEN_URL)
    @FormUrlEncoded
    Observable<Response<APITokenModel>>  token(@Field("token") String token);

    @POST(APIConstants.URLs.INVITE_URL)
    @FormUrlEncoded
    Observable<Response<APILoginModel>> invite(@Field("token") String token,
                                               @Field("inviteToken") String inviteToken);
    /**
     * Added by Eugene
     * */
    @POST(APIConstants.URLs.CARS_URL)
    @FormUrlEncoded
    Observable<Response<ApiCarsResponsModel>> cars(@Field("token") String token,
                                                   @Field("name") String name,
                                                   @Field("number") String number);

<<<<<<< HEAD
    @FormUrlEncoded
    @POST(APIConstants.URLs.VENUES_SYNC)
    Observable<Response<VenueSyncObjectList>> getVenuesSyncList(@Query(APIConstants.URLs.TOKEN_PARAM) String token, @Field(APIConstants.URLs.SINCE_PARAM) String since);

=======
    /***/
>>>>>>> eef3a42c4380aa361df946b62578b6539bd11767


    @POST(APIConstants.URLs.CHILD_URL)
    @FormUrlEncoded
    Observable<Response<ArrayList<APIChildSyncModel>>> childSync(@Field("token") String token,
                                                                  @Field("since") String since);

    /*Oleg's code*/
    @POST(APIConstants.URLs.AUTH_PASSWORD_RECOVER)
    @FormUrlEncoded
    Observable<Response<APIPassRecoverModel>> recover (@Field("email") String email);
    /*end*/
}
