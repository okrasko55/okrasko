package com.itech.http.httprequeststesting.network.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Oleg K on 18.10.2016.
 */

public class APIModelRegister {

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("status")
    private String status;

    @SerializedName("token")
    private String token;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("id")
    private String id;

    public String getStatus() { return status; }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
