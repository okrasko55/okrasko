package com.itech.http.httprequeststesting.network.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alyona on 17.10.2016.
 */

public class APIRegistrationModel {

    @SerializedName("status")
    private String status;

    @SerializedName("token")
    private String token;

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("photo_url")
    private String photoUrl;

    @SerializedName("photo_landscape_url")
    private String photoLandscapeUrl;

    @SerializedName("id")
    private int id;

    @SerializedName("photo")
    private String photo;

    @SerializedName("phone")
    private String phone;

    @SerializedName("phone_code")
    private String  phone_code;

    @SerializedName("phone_value")
    private String phone_value;

    @SerializedName("email")
    private String email;

    @SerializedName("location_access")
    private int location_access;

    @SerializedName("profile_visible")
    private int profile_visible;

    @SerializedName("voice_nav")
    private int voice_nav;

    @SerializedName("send_push")
    private int send_push;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getPhotoLandscapeUrl() {
        return photoLandscapeUrl;
    }

    public void setPhotoLandscapeUrl(String photoLandscapeUrl) {
        this.photoLandscapeUrl = photoLandscapeUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhone_code() {
        return phone_code;
    }

    public void setPhone_code(String phone_code) {
        this.phone_code = phone_code;
    }

    public String getPhone_value() {
        return phone_value;
    }

    public void setPhone_value(String phone_value) {
        this.phone_value = phone_value;
    }

    public int getLocation_access() {
        return location_access;
    }

    public void setLocation_access(int location_access) {
        this.location_access = location_access;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getProfile_visible() {
        return profile_visible;
    }

    public void setProfile_visible(int profile_visible) {
        this.profile_visible = profile_visible;
    }

    public int getVoice_nav() {
        return voice_nav;
    }

    public void setVoice_nav(int voice_nav) {
        this.voice_nav = voice_nav;
    }

    public int getSend_push() {
        return send_push;
    }

    public void setSend_push(int send_push) {
        this.send_push = send_push;
    }

}
