package com.itech.http.httprequeststesting.network.models.venues;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Natalia on 16.09.2015.
 */
public class VenueSyncObject {
    public static final String NAME_FIELD = "name";

    public static final String TYPE_INDOORS = "indoors";
    public static final String TYPE_INDOORATLAS = "indooratlas";

    private String id;

    private String name;

    private String description;

    private double lng;

    private double lat;

    private String image;

    @SerializedName("indoors_api_key")
    private String indoorsApiKey;

    @SerializedName("indoors_building_id")
    private String indoorsBuildingId;

    @SerializedName("indooratlas_api_key")
    private String indoorAtlasApiKey;

    @SerializedName("indooratlas_secret")
    private String indoorAtlasSecret;

    @SerializedName("indooratlas_venue_id")
    private String indoorAtlasVenueId;

    @SerializedName("image_url")
    private String imageUrl;

    @SerializedName("gps_corners")
    public List<ServerLatLong> gpsCorners;

    private boolean navigation;

    private int access;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getAccess() {
        return access;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getLongitude() {
        return lng;
    }

    public void setLongitude(double longitude) {
        this.lng = longitude;
    }

    public double getLatitude() {
        return lat;
    }

    public void setLatitude(double latitude) {
        this.lat = latitude;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIndoorsBuildingId() {
        return indoorsBuildingId;
    }




    public void setIndoorsBuildingId(String indoorsBuildingId) {
        this.indoorsBuildingId = indoorsBuildingId;
    }

    public String getIndoorsApiKey() {
        return indoorsApiKey;
    }

    public void setIndoorsApiKey(String indoorsApiKey) {
        this.indoorsApiKey = indoorsApiKey;
    }

    public String getIndoorAtlasApiKey() {
        return indoorAtlasApiKey;
    }

    public void setIndoorAtlasApiKey(String indoorAtlasApiKey) {
        this.indoorAtlasApiKey = indoorAtlasApiKey;
    }

    public String getIndoorAtlasSecret() {
        return indoorAtlasSecret;
    }

    public void setIndoorAtlasSecret(String indoorAtlasSecret) {
        this.indoorAtlasSecret = indoorAtlasSecret;
    }

    public String getIndoorAtlasVenueId() {
        return indoorAtlasVenueId;
    }

    public void setIndoorAtlasVenueId(String indoorAtlasVenueId) {
        this.indoorAtlasVenueId = indoorAtlasVenueId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.image = imageUrl;
    }

    public boolean isNavigation() {
        return navigation;
    }

    public void setNavigation(boolean navigation) {
        this.navigation = navigation;
    }

    public static class GpsCornerObject {

        private double lt;
        private double lg;

        public double getLt() {
            return lt;
        }

        public double getLg() {
            return lg;
        }
    }

    public static class ServerLatLong {
        public float lt;
        public float lg;


    }
}
