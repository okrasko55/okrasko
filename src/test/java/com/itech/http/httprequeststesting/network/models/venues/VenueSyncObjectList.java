package com.itech.http.httprequeststesting.network.models.venues;

import java.util.ArrayList;

/**
 * Created by Igor on 09.11.2016.
 */

public class VenueSyncObjectList {
    private ArrayList<VenueSyncObject> update;
    private ArrayList<Integer> lost;
    private ArrayList<VenueSyncObject> delete;

    public ArrayList<VenueSyncObject> getUpdate() {
        return update;
    }

    public void setUpdate(ArrayList<VenueSyncObject> update) {
        this.update = update;
    }

    public ArrayList<Integer> getLost() {
        return lost;
    }

    public void setLost(ArrayList<Integer> lost) {
        this.lost = lost;
    }

    public ArrayList<VenueSyncObject> getDelete() {
        return delete;
    }

    public void setDelete(ArrayList<VenueSyncObject> delete) {
        this.delete = delete;
    }


}
