package com.itech.http.httprequeststesting.network.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alyona on 14.11.2016.
 */

public class APIChildSyncModel {

    @SerializedName("id")
    private int id;

    @SerializedName("parent_id")
    private int parentId;

    @SerializedName("name")
    private String name;

    @SerializedName("code")
    private String code;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("photo")
    private String photo;

    @SerializedName("deleted_at")
    private String deletedAt;

    @SerializedName("x")
    private int x;

    @SerializedName("y")
    private int y;

    @SerializedName("z")
    private int z;

    @SerializedName("gps_latitude")
    private String gpsLatitude;

    @SerializedName("gps_longitude")
    private String gpsLongitude;

    @SerializedName("in_mall")
    private int inMall;

    @SerializedName("in_mall_time")
    private int inMallTime;

    @SerializedName("photo_landscape")
    private String photoLandscape;

    @SerializedName("token")
    private String token;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public String getGpsLatitude() {
        return gpsLatitude;
    }

    public void setGpsLatitude(String gpsLatitude) {
        this.gpsLatitude = gpsLatitude;
    }

    public String getGpsLongitude() {
        return gpsLongitude;
    }

    public void setGpsLongitude(String gpsLongitude) {
        this.gpsLongitude = gpsLongitude;
    }

    public int getInMall() {
        return inMall;
    }

    public void setInMall(int inMall) {
        this.inMall = inMall;
    }

    public int getInMallTime() {
        return inMallTime;
    }

    public void setInMallTime(int inMallTime) {
        this.inMallTime = inMallTime;
    }

    public String getPhotoLandscape() {
        return photoLandscape;
    }

    public void setPhotoLandscape(String photoLandscape) {
        this.photoLandscape = photoLandscape;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
