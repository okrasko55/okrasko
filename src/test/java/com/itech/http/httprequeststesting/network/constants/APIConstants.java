package com.itech.http.httprequeststesting.network.constants;


import java.util.HashMap;
import java.util.Map;

/**
 * Created by barbinkh on 05.10.16.
 */

public class APIConstants {

    public static class URLs {
        public static final String MAIN_URL = "http://dev.sns.ae";
        //   public static final String MAIN_URL = "http://bus.itcraftlab.com";
        public static final String LOGIN_URL = "/api/v2/auth/login";
        public static final String REGISTER_URL = "/api/v2/auth/register";
        // public static final String LOGIN_URL = "/api/v1/authenticate";
<<<<<<< HEAD
        public static final String TOKEN_URL = "/api/v2/auth/token";
        public static final String INVITE_URL = "/api/v2/invite/approve";

        public static final String CHILD_URL = "/api/v2/child/sync";//?token=2178b280387212312509e4c2f8d824b33de39a7d";

        //V2
        public static final String HEADER_PAGES_COUNT = "X-Pagination-Page-Count";

        public static final String DATE_FORMAT = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'";
        public static final String MULTIPART_FORM_DATA = "multipart/form-data";
        //Path constants
        public static final String PATH_ID = "id";
        //Login/registration
        public static final String AUTH_LOGIN = "/auth/login";
        public static final String AUTH_LOGOUT = "/auth/logout";

        public static final String AUTH_REGISTER = "/auth/register";
        public static final String AUTH_TOKEN = "/auth/token";
        public static final String AUTH_PASSWORD_RECOVER = "/auth/password-recover";
        public static final String AUTH_RECOVER_CODE = "/auth/recover-code";
        //Venues data
        public static final String VENUES_LIST = "/malls";
        public static final String VENUES_SEARCH_LIST = "/malls/search";
        public static final String VENUES_NEAREST_LIST = "/malls/nearest";
        public static final String VENUES_LOCATION_LIST = "/malls/gps";
        public static final String VENUE_DATA = "/malls/{id}";
        public static final String VENUES_SYNC = "api/v2/malls/sync";
        //User data
        public static final String USER_GET_DATA = "/users/{id}";
        public static final String USER_PROFILE_SYNC = "/users/profile";
        public static final String USER_PHOTO = "/users/{id}/photo";
        public static final String USER_DEVICE = "/users/{id}/device";
        public static final String USER_PUT_DATA = "/users/{id}";
        public static final String USER_SEARCH = "/users/user-search";
        //Types, Categories, Shops data
        public static final String TYPES_LIST = "/types";
        public static final String TYPES_SEARCH_LIST = "/types/search";
        public static final String CATEGORIES_LIST = "/categories";
        public static final String CATEGORIES_SEARCH_LIST = "/categories/search";
        public static final String SHOPS_LIST = "/shops";
        public static final String SHOPS_SEARCH_LIST = "/shops/search";
        public static final String PRODUCTS_SEARCH_LIST = "/products/search";
        //Routes
        public static final String ROUTES_SEARCH = "/path/search-pixels";
        public static final String ROUTES_TO_ENTRY = "/path/to-entry";
        public static final String ROUTES_TO_NEAREST_EXIT = "/path/nearest-exit";
        //Friends
        public static final String FRIEND_REQUEST = "/friend-requests";
        public static final String FRIENDS_SEARCH = "/friends/search";
        public static final String FRIENDS_DELETE = "/friends/{id}";
        public static final String FRIENDS_DELETE_USER_DELETE = "/friends/user-delete";
        public static final String FRIEND_REQUEST_APPROVE = "/friend-requests/approve";
        public static final String FRIEND_REQUEST_DELETE = "/friend-requests/{id}";
        public static final String FRIEND_REQUEST_TO_ME = "/friend-requests/to-me";
        public static final String FRIEND_REQUEST_TO_ME_COUNT = "/friend-requests/count";

        //Child
        public static final String CHILD_SEARCH = "/child/search";
        public static final String CHILD_SYNC = "/child/sync";
        public static final String CHILD_CREATE = "/child/create-child";
        public static final String CHILD_DELETE = "/children/{id}";
        public static final String CHILD_UPDATE = "/child/update-child";
        //Car
        public static final String CAR_SEARCH = "/cars/search";
        //    public static final String CAR_SYNC = "/cars/sync";
        public static final String CAR_CREATE = "/cars";
        public static final String CAR_DELETE = "/cars/{id}";
        public static final String CAR_UPDATE = "/cars/{id}/update";
        public static final String CAR_PHOTO = "/cars/{id}/photo";
        //Theme
        public static final String THEMES_SEARCH = "/themes/search";
        public static final String THEMES_SYNC = "/themes/sync";
        //Beacons
        public static final String BEACONS_SYNC = "/beacons/sync";
        public static final String BEACONS_SEARCH = "/beacons/search";
        //Floors
        public static final String FLOOR_SEARCh = "/floors/search";
        //Invite
        public static final String INVITE_APPROVE = "/invite/approve";
        public static final String SEARCH_SHOP_ACCESS = "/shops/search-access";
        public static final String SEARCH_PRODUCT_ACCESS = "/products/search-access";
        public static final String PHONE_UNIQUE = "PHONE_UNIQUE";
        public static final String EMAIL_UNIQUE = "EMAIL_UNIQUE";


        public static final String ALREADY_FRIENDS = "ALREADY_FRIENDS";
        public static final String INVITATION_NOT_FOUND = "INVITATION_NOT_FOUND";
        // TODO: 22.06.16 switch to proxy
        //Google maps destinations API
        public static final String GET_ROUTE = "/maps/api/directions/json";
        public static final String GET_GEOCODE = "/maps/api/geocode/json";
        public static final String DRIVING_MODE = "driving";
        public static final String WALKING_MODE = "walking";
        //Params names
        public static final String NAME_PARAM = "name";
        public static final String TOKEN_PARAM = "token";

        public static final String DEVICE_TOKEN = "device_token";
        public static final String DEVICE_PUSH_SEVICE = "device_service";
        public static final String PAGE_PARAM = "page";
        public static final String SORT_PARAM = "sort";
        public static final String PHOTO_PARAM = "photo";
        public static final String PHOTO_PARAM_LANDSCAPE = "photo_landscape";
        public static final String SEARCH_PARAM = "search";
        public static final String LATITUDE_PARAM = "latitude";
        public static final String LONGITUDE_PARAM = "longitude";

        public static final String EXPAND_PARAM = "expand";
        public static final String EXCLUDE_PARAM = "exclude";
        public static final String FIELDS_PARAM = "fields";
        public static final String EMAIL_PARAM = "email";
        public static final String LAST_NAME_PARAM = "last_name";
        public static final String FIRST_NAME_PARAM = "first_name";
        public static final String PROFILE_VISIBLE_PARAM = "profile_visible";
        public static final String LOCATION_ACCESS_PARAM = "location_access";
        public static final String PHONE_CODE_PARAM = "phone_code";
        public static final String VOICE_NAVIGATION = "voice_nav";
        public static final String SEND_PUSH= "send_push";

        public static final String PHONE_VALUE_PARAM = "phone_value";
        public static final String PASSWORD_PARAM = "password";
        public static final String CODE_PARAM = "code";
        public static final String FRIENDS_PARAM = "friends";
        public static final String CURRENT_PASSWORD_PARAM = "current_password";
        public static final String MALL_ID_PARAM = "mall_id";
        public static final String CREATED_AT_PARAM = "created_at";
        public static final String UPDATED_AT_PARAM = "updated_at";
        public static final String TYPE_PARAM = "type";
        public static final String TYPE_ID_PARAM = "type_id";
        public static final String CATEGORY_ID_PARAM = "category_id";
        public static final String FLOORS_PARAM = "floors";
        public static final String SHOPS_PARAM = "shops";
        public static final String ROOT_FROM_X_PARAM = "from_x";
        public static final String ROOT_FROM_Y_PARAM = "from_y";
        public static final String ROOT_FROM_Z_PARAM = "from_z";
        public static final String ROOT_TO_X_PARAM = "to_x";
        public static final String ROOT_TO_Y_PARAM = "to_y";
        public static final String ROOT_TO_Z_PARAM = "to_z";
        public static final String ROOT_ENTRY_ID = "entry_id";
        public static final String TO_USER_ID_PARAM = "to_user_id";
        public static final String SINCE_PARAM = "since";
        public static final String AREA_PARAM = "area";
        public static final String NUMBER = "number";
        public static final String DESCRIPTION = "description";
        public static final String X = "x";
        public static final String Y = "y";
        public static final String Z = "z";
        public static final String LAT = "gps_latitude";
        public static final String LONG = "gps_longitude";
        public static final String LEAVE_HERE = "leave_here";
        public static final String IN_MALL = "in_mall";
        public static final String INVITE_TOKEN = "inviteToken";
        public static final String ADMINS_PARAM = "admins";
        public static final String NEW_PARAM = "new";
        public static final String UPDATED_PARAM = "updated";
        public static final String DELETED_PARAM = "deleted";
        public static final Map<String, Integer> errorMessagesMap = new HashMap<>();
        public static final String ERROR_STATUS = "error";
        public static final String SUCCESS_STATUS = "success";
        //Base constants
        private static final String START_PARAMS = "?";
        private static final String ADD_PARAM = "&";
        private static final String LEFT_BRACKET = "{";
        private static final String RIGHT_BRACKET = "}";
        private static final String EQUALS = "=";
        //Error messages
        public static final String AUTH_FAILED = "AUTH_FAILED";
        public static final String AUTH_FAILED_PHONE = "AUTH_FAILED_PHONE";
        private static final String MAIL_UNIQUE = "MAIL_UNIQUE";
        private static final String NOT_FOUND = "NOT_FOUND";
        private static final String WRONG_CODE = "WRONG_CODE";
        private static final String FIRST_NAME_REQUIRED = "FIRST_NAME REQUIRED";
        private static final String LAST_NAME_REQUIRED = "LAST_NAME REQUIRED";
        private static final String PHONE_INVALID = "PHONE_INVALID";
        public static final String EMAIL_FORMAT = "EMAIL_FORMAT";
        private static final String EMAIL_REQUIRED = "EMAIL_REQUIRED";
        private static final String WRONG_TYPE = "WRONG_TYPE";
        private static final String STATUS_FAILED = "STATUS_FAILED";
        private static final String NO_DATA = "NO_DATA";
        private static final String WRONG_CURRENT_PASSWORD = "WRONG_CURRENT_PASSWORD";




/*        public static String buildUrl(String requestPart) {
            return BuildConfig.HOST + requestPart;
        }

        public static String buildUrlDefault(   String apiVersion ) {
            //String apiVersion = BuildConfig.API_VERSION;
            return String.format("%s/api/%s/", BuildConfig.HOST, apiVersion);
        }

        public static String buildSocketAddress() {
            return BuildConfig.SOCKET_ADDRESS;
        }

        public static String buildPathWithToken(String path) {
            return new StringBuilder(path).append(path.contains(START_PARAMS) ? ADD_PARAM : START_PARAMS)
                    .append(TOKEN_PARAM).append(EQUALS).append(LEFT_BRACKET).append(TOKEN_PARAM).append(RIGHT_BRACKET).toString();
        }

        public static String buildPathWithPage(String path) {
            return new StringBuilder(path).append(path.contains(START_PARAMS) ? ADD_PARAM : START_PARAMS)
                    .append(PAGE_PARAM).append(EQUALS).append(LEFT_BRACKET).append(PAGE_PARAM).append(RIGHT_BRACKET).toString();
        }
=======
        public static final String TOKEN_URL = "/api/v1/auth/token";
        public static final String INVITE_URL = "/api/v1/invite/approve";
        public static final String CARS_URL = "/api/v1/cars";
>>>>>>> eef3a42c4380aa361df946b62578b6539bd11767

        public static String buildGoogleApiUrl() {
            return BuildConfig.GOOLE_API_ENDPOINT;
        }*/
    }
}
