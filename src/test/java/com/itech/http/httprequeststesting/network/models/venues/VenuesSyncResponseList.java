package com.itech.http.httprequeststesting.network.models.venues;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Natalia on 16.09.2015.
 */
public class VenuesSyncResponseList extends ArrayList<VenueSyncObject> {

    public static <T> Collection<T> filter(Collection<T> target, IPredicate<T> predicate) {
        Collection<T> result = new ArrayList<>();
        for (T element : target) {
            if (predicate.apply(element)) {
                result.add(element);
            }
        }
        return result;
    }

    public interface IPredicate<T> {
        boolean apply(T type);
    }

}
