package com.itech.http.httprequeststesting.network.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Igor on 17.10.2016.
 */

public class APIInviteModel {

    @SerializedName("status")
    public List<String> errors;
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getErrors() {
        return errors;
    }
}
