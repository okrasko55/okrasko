package com.itech.http.httprequeststesting;

import com.itech.http.httprequeststesting.DB.DbSessionFactory;
import com.itech.http.httprequeststesting.DB.tables.InvitationsDataSet;
import com.itech.http.httprequeststesting.Logger.Log4jConfigure;
import com.itech.http.httprequeststesting.UserCredentials.UserData;
import com.itech.http.httprequeststesting.network.models.APILoginModel;
import com.itech.http.httprequeststesting.network.models.venues.VenueSyncObjectList;
import com.itech.http.httprequeststesting.network.retrofit.APIHttp;
import com.itech.http.httprequeststesting.requestcontrol.SyncController;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

import retrofit2.Response;
import rx.observers.TestSubscriber;

/**
 * Created by Igor on 17.10.2016.
 */

public class InviteTests {
    private static TestSubscriber<Response<APILoginModel>> testSubscriber;
    private static Response<APILoginModel> response;
    private static InvitationsDataSet invitations;
    private static TokenTest tokenTest;
    private static String inviteToken;
    private static Integer mallId = 14;
    private static String token;
    private static Transaction tx;
    private static SyncController syncController;
    private static Integer inviteId;

    @Before
    public  void dbSetUp() {
        Log4jConfigure.configureSimpleConsole();
        syncController = new SyncController();
        Session session = DbSessionFactory.getSessionFactory().openSession();
        long createdAt = System.currentTimeMillis() / 1000;
        Random r = new Random();
        int random = r.nextInt(1231237);
        inviteToken = "LlVUjutKP4qt5" + random;
        invitations = new InvitationsDataSet(1, 0, "testinnav@gmail.com", mallId, 1, createdAt, createdAt, null, inviteToken);
        tx = session.beginTransaction();
        session.save(invitations);
        tx.commit();
        session.close();
        inviteId = invitations.getId();
        tokenTest = new TokenTest();
        UserData userData = new UserData();
        token = userData.getUserLoginModelData("n1@n.c", "a").getToken();
        testSubscriber = new TestSubscriber<>();
        APIHttp.getInstance().invite(token, inviteToken).subscribe(testSubscriber);
        response = testSubscriber.getOnNextEvents().get(0);
        Assert.assertEquals(200, response.code());
    }

    @Test
    public void testSyncAferInvite() {
        VenueSyncObjectList venueSyncObjectList;
        SyncController syncController;
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        Calendar calendar = new GregorianCalendar();
        calendar.add(Calendar.MINUTE, -5);
        calendar.add(Calendar.HOUR, -2);
        syncController = new SyncController();
        venueSyncObjectList = syncController.GetSyncObjectBySinceDate(token, sdf.format(calendar.getTime())).getOnNextEvents().get(0).body();
        Assert.assertEquals(mallId.toString(), String.valueOf(venueSyncObjectList.getUpdate().get(0).getId()));
    }

    @Test
    public void testDeleteInvite() {
        VenueSyncObjectList venueSyncObjectList;
        Calendar calendar = new GregorianCalendar();
        Date currentTime = calendar.getTime();
        Long deletedAt = Long.valueOf((int) (currentTime.getTime() / 1000));
        Session session = DbSessionFactory.getSessionFactory().openSession();
        session.load(invitations, inviteId);
        invitations.setDeletedAt(deletedAt);
        //invitations.setToInviteId(9);
        tx = session.beginTransaction();
        session.update(invitations);
        tx.commit();
        session.close();
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
        calendar.add(Calendar.MINUTE, -5);
        calendar.add(Calendar.HOUR, -2);
        syncController = new SyncController();
        venueSyncObjectList = syncController.GetSyncObjectBySinceDate(token, sdf.format(calendar.getTime())).getOnNextEvents().get(0).body();
        Assert.assertEquals(mallId.toString(), String.valueOf(venueSyncObjectList.getLost().get(0)));
    }


    @After
    public void dbClear() {
        Session session = DbSessionFactory.getSessionFactory().openSession();
        tx = session.beginTransaction();
        session.delete(invitations);
        tx.commit();
        session.close();
    }

    @AfterClass
    public static void dbFactoryClose() {
        DbSessionFactory.close();
    }

}
