package com.itech.http.appiumandroidtesting;

import com.itech.http.appiumandroidtesting.core.MyLogger;
import com.itech.http.appiumandroidtesting.core.managers.DriverManager;
import com.itech.http.appiumandroidtesting.tests.TestPrimer;

import org.apache.log4j.Level;
import org.junit.runner.JUnitCore;

import java.net.MalformedURLException;

/**
 * Created by Igor on 07.12.2016.
 */

public class Runner {
    public static void main(String[] args) throws MalformedURLException {

        MyLogger.log.setLevel(Level.INFO);
        try {
            DriverManager.createDriver();
            JUnitCore.runClasses(TestPrimer.class);
        } finally {
            DriverManager.killDriver();
        }
    }
}