package com.itech.http.appiumandroidtesting;

import com.itech.http.httprequeststesting.Logger.Log4jConfigure;

import org.junit.Assert;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;

import java.net.MalformedURLException;
import java.util.List;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

/**
 * Created by Igor on 16.11.2016.
 */

public class NavigationTests {
    //Locators
    private static final String PATHACT = "com.smartnavigationsystems.mallapp.mallapplication:id/";
    public static final String ACTION_MENU_BUTTON = PATHACT + "action_menu";
    public static final String SEARCH_TEXT_FIELD_ID = PATHACT + "search_src_text";
    public static final String SEARCH_BUTTON_ID = PATHACT + "search_button";
    public static final String NAVIGATE_FROM_BUTTON_ID = PATHACT + "navigate_from_button";
    public static final String NAVIGATE_TO_BUTTON_ID = PATHACT + "navigate_to_button";
    public static final String NAVIGATE_FROM_DESC_ID = PATHACT + "navigate_from_desc";
    public static final String NAVIGATE_TO_DESC_ID = PATHACT + "navigate_to_desc";
    public static final String GO_TO_OUTDOOR = PATHACT + "venue_map_outdoor_action";
    public static final String GO_TO_INDOOR = PATHACT + "start_indoor_navigation";
    public static final String SEARCH_OBJECT_RESULT_LIST = PATHACT + "search_object_result_list";
    public static final String ITEM_LIST_TITLE_LAYOUT = PATHACT + "item_list_title_layout";

    //Test data

    //Facilities
    public static final String FACILITY_1 = "fac_1";
    public static final String FACILITY_2 = "Sales room";
    public static final String FACILITY_3 = "Testfacility1";

    //Cars
    public static final String CAR1 = "carr";
    public static final String CAR2 = "bus";

    //Messages
    public static final String LIKE_TO_PASS_TO_THE_INDOOR = "Would you like to pass to the indoor map to create a route to the exit?";
    public static final String WANT_EXIT_FROM_VENUE = "You want exit from venue to outdoor map to continue navigation?";


    private static AndroidDriver wd;

    private List<AndroidElement> list;

    @BeforeClass
    public static void appiumInit() {
        Log4jConfigure.configureSimpleConsole();
        try {
            wd = AppiumInitialize.setUp();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        /*wd.findElement(By.id(PATHACT + "welcome_button_sign_in")).click();
        wd.findElement(By.id(PATHACT + "sing_select_type_button_email")).click();
        wd.findElement(By.id(PATHACT + "login_email_layout")).sendKeys("n1@n.c");
        wd.findElement(By.id(PATHACT + "login_password_layout")).click();
        wd.findElement(By.id(PATHACT + "login_password_layout")).sendKeys("a");
        wd.findElement(By.id(PATHACT + "login_log_in")).click();*/
    }

    //To check that selected object has been outputed correctly in the field
    @Test
    public void fromToFieldsTextCheckFacility() {
        String text;

        wd.findElement(By.id(PATHACT + "action_menu")).click();
        list = wd.findElements(By.className("android.support.v7.widget.LinearLayoutCompat"));
        list.get(1).click();

        wd.findElement(By.id(NAVIGATE_FROM_BUTTON_ID)).click();
        list = wd.findElements(By.className("android.support.v7.app.ActionBar$Tab"));
        list.get(1).click();
        wd.findElement(By.id(SEARCH_BUTTON_ID)).click();
        wd.findElement(By.id(SEARCH_TEXT_FIELD_ID)).sendKeys(FACILITY_1);
        list = wd.findElements(By.id("item_facility_text"));
        list.get(0).click();
        text = wd.findElement(By.id(NAVIGATE_FROM_DESC_ID)).getText();

        Assert.assertEquals("Facility " + FACILITY_1, text);

        wd.findElement(By.id(NAVIGATE_TO_BUTTON_ID)).click();
        list = wd.findElements(By.className("android.support.v7.app.ActionBar$Tab"));
        list.get(1).click();
        wd.findElement(By.id(SEARCH_BUTTON_ID)).click();
        wd.findElement(By.id(SEARCH_TEXT_FIELD_ID)).sendKeys(FACILITY_2);
        list = wd.findElements(By.id("item_facility_text"));
        list.get(0).click();
        text = wd.findElement(By.id(NAVIGATE_TO_DESC_ID)).getText();

        Assert.assertEquals("Facility " + FACILITY_2, text);
        wd.findElement(By.id(PATHACT + "navigate_on_selected_route_button")).click();
        wd.findElement(By.id(PATHACT + "action_done")).click();
    }

    //Facilities
    @Test
    public void FacilitiesDifferentVenues() {
        //Menu button
        wd.findElement(By.id(ACTION_MENU_BUTTON)).click();
        //Select menu items
        list = wd.findElements(By.className("android.support.v7.widget.LinearLayoutCompat"));
        //Select  Navigate item
        list.get(1).click();
        //Select Navigate from field
        wd.findElement(By.id(NAVIGATE_FROM_BUTTON_ID)).click();
        //Select horizontal menu items (Venues, Facilities, Products, Friends, Children, Cars)
        list = wd.findElements(By.className("android.support.v7.app.ActionBar$Tab"));
        //Select Facility item
        list.get(1).click();
        //Tap on Search field
        wd.findElement(By.id(SEARCH_BUTTON_ID)).click();
        //Enter facility name to the Search field
        wd.findElement(By.id(SEARCH_TEXT_FIELD_ID)).sendKeys(FACILITY_1);
        //Select facilities from the search result
        list = wd.findElements(By.id("item_facility_text"));
        list.get(0).click();
        //Select Navigate to field
        wd.findElement(By.id(NAVIGATE_TO_BUTTON_ID)).click();
        //Select horizontal menu items (Venues, Facilities, Products, Friends, Children, Cars)
        list = wd.findElements(By.className("android.support.v7.app.ActionBar$Tab"));
        //Select Facility item
        list.get(1).click();
        //Tap on Search field
        wd.findElement(By.id(SEARCH_BUTTON_ID)).click();
        //Enter facility name to the Search field
        wd.findElement(By.id(SEARCH_TEXT_FIELD_ID)).sendKeys(FACILITY_3);
        list = wd.findElements(By.id("item_facility_text"));
        list.get(0).click();
        //Tap to Build route button
        wd.findElement(By.id(PATHACT + "navigate_on_selected_route_button")).click();

        //Checking "Pass to indoor" pop-up
        Assert.assertEquals(LIKE_TO_PASS_TO_THE_INDOOR, wd.findElement(By.className("android.widget.TextView")).getText());

        //Select Yes option
        wd.findElement(By.id("android:id/button1")).click();
        //Tap to "Go to outdoor" button
        wd.findElement(By.id(GO_TO_OUTDOOR)).click();

        //Checking "Pass to outdoor" pop-up
        Assert.assertEquals(WANT_EXIT_FROM_VENUE, wd.findElement(By.className("android.widget.TextView")).getText());

        //Select Yes option
        wd.findElement(By.id("android:id/button1")).click();
        wd.findElement(By.id(GO_TO_INDOOR)).click();

        //Close navigation activity
        wd.findElement(By.id(PATHACT + "action_done")).click();
    }

    @Test
    public void FacilityToCarAnotherVenue() {
        //Menu button
        wd.findElement(By.id(ACTION_MENU_BUTTON)).click();
        //Select menu items
        list = wd.findElements(By.className("android.support.v7.widget.LinearLayoutCompat"));
        //Select  Navigate item
        list.get(1).click();
        //Select Navigate from field
        wd.findElement(By.id(NAVIGATE_FROM_BUTTON_ID)).click();
        //Select horizontal menu items (Venues, Facilities, Products, Friends, Children, Cars)
        list = wd.findElements(By.className("android.support.v7.app.ActionBar$Tab"));
        //Select Facility item
        list.get(1).click();
        //Tap on Search field
        wd.findElement(By.id(SEARCH_BUTTON_ID)).click();
        //Enter facility name to the Search field
        wd.findElement(By.id(SEARCH_TEXT_FIELD_ID)).sendKeys(FACILITY_1);
        //Select facilities from the search result
        list = wd.findElements(By.id("item_facility_text"));
        list.get(0).click();
        //Select Navigate to field
        wd.findElement(By.id(NAVIGATE_TO_BUTTON_ID)).click();
        //Select horizontal menu items (Venues, Facilities, Products, Friends, Children, Cars)
        list = wd.findElements(By.className("android.support.v7.app.ActionBar$Tab"));
        //Select Cars item
        list.get(5).click();
        //Tap on Search field
        wd.findElement(By.id(SEARCH_BUTTON_ID)).click();
        //Enter facility name to the Search field
        wd.findElement(By.id(SEARCH_TEXT_FIELD_ID)).sendKeys(CAR1);
        wd.findElement(By.id(ITEM_LIST_TITLE_LAYOUT)).click();
        //Tap to Build route button
        wd.findElement(By.id(PATHACT + "navigate_on_selected_route_button")).click();

        //Checking "Pass to indoor" pop-up
        Assert.assertEquals(LIKE_TO_PASS_TO_THE_INDOOR, wd.findElement(By.className("android.widget.TextView")).getText());

        //Select Yes option
        wd.findElement(By.id("android:id/button1")).click();
        //Tap to "Go to outdoor" button
        wd.findElement(By.id(GO_TO_OUTDOOR)).click();

        //Checking "Pass to outdoor" pop-up
        Assert.assertEquals(WANT_EXIT_FROM_VENUE, wd.findElement(By.className("android.widget.TextView")).getText());

        //Select Yes option
        wd.findElement(By.id("android:id/button1")).click();
        wd.findElement(By.id(GO_TO_INDOOR)).click();

        //Close navigation activity
        wd.findElement(By.id(PATHACT + "action_done")).click();
    }

    @Test
    public void FacilityToCarSameVenue() {
        //Menu button
        wd.findElement(By.id(ACTION_MENU_BUTTON)).click();
        //Select menu items
        list = wd.findElements(By.className("android.support.v7.widget.LinearLayoutCompat"));
        //Select  Navigate item
        list.get(1).click();
        //Select Navigate from field
        wd.findElement(By.id(NAVIGATE_FROM_BUTTON_ID)).click();
        //Select horizontal menu items (Venues, Facilities, Products, Friends, Children, Cars)
        list = wd.findElements(By.className("android.support.v7.app.ActionBar$Tab"));
        //Select Facility item
        list.get(1).click();
        //Tap on Search field
        wd.findElement(By.id(SEARCH_BUTTON_ID)).click();
        //Enter facility name to the Search field
        wd.findElement(By.id(SEARCH_TEXT_FIELD_ID)).sendKeys(FACILITY_1);
        //Select facilities from the search result
        list = wd.findElements(By.id("item_facility_text"));
        list.get(0).click();
        //Select Navigate to field
        wd.findElement(By.id(NAVIGATE_TO_BUTTON_ID)).click();
        //Select horizontal menu items (Venues, Facilities, Products, Friends, Children, Cars)
        list = wd.findElements(By.className("android.support.v7.app.ActionBar$Tab"));
        //Select Cars item
        list.get(5).click();
        //Tap on Search field
        wd.findElement(By.id(SEARCH_BUTTON_ID)).click();
        //Enter facility name to the Search field
        wd.findElement(By.id(SEARCH_TEXT_FIELD_ID)).sendKeys(CAR2);
        wd.findElement(By.id(ITEM_LIST_TITLE_LAYOUT)).click();

        //Tap to Build route button
        wd.findElement(By.id(PATHACT + "navigate_on_selected_route_button")).click();

        //Close navigation activity
        wd.findElement(By.id(PATHACT + "action_done")).click();
    }

    @Test
    public void FacilitiesSameVenue() throws InterruptedException {
        wd.findElement(By.id(ACTION_MENU_BUTTON)).click();
        list = wd.findElements(By.className("android.support.v7.widget.LinearLayoutCompat"));
        list.get(1).click();
        wd.findElement(By.id(NAVIGATE_FROM_BUTTON_ID)).click();
        list = wd.findElements(By.className("android.support.v7.app.ActionBar$Tab"));
        list.get(1).click();
        wd.findElement(By.id(SEARCH_BUTTON_ID)).click();
        wd.findElement(By.id(SEARCH_TEXT_FIELD_ID)).sendKeys(FACILITY_1);
        list = wd.findElements(By.id("item_facility_text"));
        list.get(0).click();
        wd.findElement(By.id(NAVIGATE_TO_BUTTON_ID)).click();
        list = wd.findElements(By.className("android.support.v7.app.ActionBar$Tab"));
        list.get(1).click();
        wd.findElement(By.id(SEARCH_BUTTON_ID)).click();
        wd.findElement(By.id(SEARCH_TEXT_FIELD_ID)).sendKeys(FACILITY_2);
        list = wd.findElements(By.id("item_facility_text"));
        list.get(0).click();
        wd.findElement(By.id(PATHACT + "navigate_on_selected_route_button")).click();
        wd.findElement(By.id(PATHACT + "action_done")).click();
    }


    @AfterClass
    public static void signOut() {
        //AppiumInitializeIOS.tearDown();

    }
}