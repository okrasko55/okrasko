package com.itech.http.appiumandroidtesting;

import com.itech.http.httprequeststesting.Logger.Log4jConfigure;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import java.net.MalformedURLException;

import io.appium.java_client.ios.IOSDriver;

/**
 * Created by QA on 12/14/16.
 */

public class iOSTest {
    private static IOSDriver iosDriver;
    @BeforeClass
    public static void appiumInit() {
        Log4jConfigure.configureSimpleConsole();
        try {
            iosDriver = AppiumInitializeIOS.setUp();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testLogin() throws InterruptedException {
        iosDriver.findElementByName("SIGN IN").click();
        iosDriver.findElementByName("WITH EMAIL").click();
        WebElement email = iosDriver.findElementByAccessibilityId("E-mail");
        email.click();
        email.clear();
        email.sendKeys("n1@n.c");

        iosDriver.hideKeyboard();

        WebElement pass = iosDriver.findElementByAccessibilityId("Password");
        pass.click();
        pass.sendKeys("a");
        iosDriver.hideKeyboard();
        iosDriver.findElementByName("LOGIN").click();
        iosDriver.findElementByAccessibilityId("ic menu"); //unable to find
        Thread.sleep(10000);

        if (iosDriver != null) {
            iosDriver.quit();
        }

    }

}
