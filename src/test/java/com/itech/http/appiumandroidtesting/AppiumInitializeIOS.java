package com.itech.http.appiumandroidtesting;

/**
 * Created by Igor on 19.10.2016.
 */

import org.apache.commons.io.filefilter.FalseFileFilter;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;

public class AppiumInitializeIOS {

    private static IOSDriver iosDriver;


    public static IOSDriver setUp() throws MalformedURLException {

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.APPIUM_VERSION, "1.6.3");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "10.1");
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 5s (Model 14B72)");
        capabilities.setCapability(MobileCapabilityType.UDID, "88FFBAB0-F049-467D-BCA4-2A91471E700B");
        capabilities.setCapability(MobileCapabilityType.FULL_RESET, false);
        capabilities.setCapability("keychainPath","/Users/QA/Library/Keychains/login.keychain");
        capabilities.setCapability("keychainPassword","qa123");
        capabilities.setCapability("noReset","true");
        capabilities.setCapability("xcodeConfigFile", "/usr/local/lib/node_modules/appium/node_modules/appium-xcuitest-driver/WebDriverAgent/Configurations/ProjectSettings.xcconfig");
       // capabilities.setCapability(MobileCapabilityType.APP,"/Users/QA/Documents/mall-app-ios-git/In-Nav/In-Nav.ipa");
        capabilities.setCapability("bundleId", "com.smart.navigation.MallApp");
        capabilities.setCapability(IOSMobileCapabilityType.LAUNCH_TIMEOUT, 500000);
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.IOS_XCUI_TEST);


        //iOS
        iosDriver = new IOSDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
        iosDriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);



        return iosDriver;
    }

    public static void tearDown() {
        if (iosDriver != null) {
            iosDriver.quit();
        }
    }
}
