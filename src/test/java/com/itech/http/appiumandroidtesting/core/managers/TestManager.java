package com.itech.http.appiumandroidtesting.core.managers;

import com.itech.http.appiumandroidtesting.core.MyLogger;
import com.itech.http.appiumandroidtesting.core.Retry;
import com.itech.http.appiumandroidtesting.core.TestInfo;

import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * Created by Igor on 06.12.2016.
 */

public class TestManager {
    public static TestInfo testInfo = new TestInfo();

    @Rule
    public Retry retry = new Retry(3);

    @Before
    public void before() {
        testInfo.reset();
    }

    @Rule
    public TestRule listen = new TestWatcher() {
        @Override
        public void failed(Throwable t, Description description) {
            MyLogger.log.info("Test Failed:");
            TestInfo.printResults();
        }

        @Override
        public void succeeded(Description description) {
            MyLogger.log.info("Test Passed:");
            TestInfo.printResults();
        }
    };
}
