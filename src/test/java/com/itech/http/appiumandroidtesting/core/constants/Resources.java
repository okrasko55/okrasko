package com.itech.http.appiumandroidtesting.core.constants;

import com.itech.http.appiumandroidtesting.core.managers.ServerManager;

import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;

/**
 * Created by Igor on 06.12.2016.
 */

public class Resources {

    public static final String QUEUE = ServerManager.getWorkingDir() + "/src/main/resources/queue.json";

    public static org.json.simple.JSONObject getQueue() throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        Object obj = null;
        try {
            obj = parser.parse(new FileReader(QUEUE));
        } catch (org.json.simple.parser.ParseException e) {
            e.printStackTrace();
        }
        return (org.json.simple.JSONObject) obj;

    }
}