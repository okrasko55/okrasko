package com.itech.http.appiumandroidtesting;

/**
 * Created by Igor on 19.10.2016.
 */

import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;

public class AppiumInitialize {

    private static AndroidDriver wd;
    private static IOSDriver iosDriver;
    private static final String TESTDROID_SERVER = "http://127.0.0.1";
    public static String app = "/home/inevzorov/Apps/innav/prod/in_nav_client-1.0.1.7-20161125-debug-prod-08112016.apk";
    public static String appPackage = "com.smartnavigationsystems.mallapp.mallapplication";
    public static String activity = "activity.SplashActivity_";
    public static String udid = "0316038a4a723001" +
            "";
    public static String device = "Android";
    public static String deviceName = "S6";
    public static String platformVersion = "6.0.1";
    public static String platformName = "Android";

    public static AndroidDriver setUp() throws MalformedURLException {
        DesiredCapabilities dc = new DesiredCapabilities();
        dc.setCapability("device", device);
        dc.setCapability("deviceName", deviceName);
        dc.setCapability("platformVersion", platformVersion);
        dc.setCapability("platformName", platformName);
        dc.setCapability("app", app);
        dc.setCapability("app-package", appPackage);
        dc.setCapability("app-activity", activity);
        dc.setCapability("udid", udid);
        dc.setCapability("noReset", true);


        wd = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), dc);

        wd.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

        return wd;
    }

    public static void tearDown() {
        if (wd != null) {
            wd.quit();
        }
    }
}

