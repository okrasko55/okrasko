package com.itech.http.appiumandroidtesting.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by Igor on 07.12.2016.
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({
        Navigation.class,
        Functionality.class
})
public class TestPrimer {
}