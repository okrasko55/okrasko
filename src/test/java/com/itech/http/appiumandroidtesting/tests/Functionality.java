package com.itech.http.appiumandroidtesting.tests;

import com.itech.http.appiumandroidtesting.api.android.Android;
import com.itech.http.appiumandroidtesting.api.apps.innav.InNav;
import com.itech.http.appiumandroidtesting.core.managers.TestManager;

import org.junit.Test;

/**
 * Created by Igor on 07.12.2016.
 */

public class Functionality extends TestManager {
    private static InNav inNav = Android.app.innav;

    @Test
    public void testFirst() {
        testInfo.id("testFirst").suite("Functionality").name("Some description of verification");
    }
}
