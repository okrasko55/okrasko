package com.itech.http.appiumandroidtesting.api.interfaces;

/**
 * Created by Igor on 06.12.2016.
 */

public interface Application {

    void forceStop();

    void clearData();

    Object open();

    String packageID();

    String activityID();
}
