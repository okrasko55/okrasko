package com.itech.http.appiumandroidtesting.api.interfaces;

/**
 * Created by Igor on 06.12.2016.
 */

public interface Activity {

    Object waitToLoad();
}