package com.itech.http.appiumandroidtesting.api.android;

import com.itech.http.appiumandroidtesting.api.apps.Apps;
import com.itech.http.appiumandroidtesting.core.ADB;

import io.appium.java_client.android.AndroidDriver;

/**
 * Created by Igor on 06.12.2016.
 */


public class Android {

    public static AndroidDriver driver;
    public static ADB adb;
    public static Apps app = new Apps();
}
